﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormOutraOrigem : Form
    {
        public FormOutraOrigem()
        {
            InitializeComponent();
        }

        private void FormOutraOrigem_Load(object sender, EventArgs e)
        {

        }

        private void groupBoxLigacao_Enter(object sender, EventArgs e)
        {

        }

        private void labelInstrucaoSQL_Click(object sender, EventArgs e)
        {

        }

        private void textBoxInstrucaoSQL_TextChanged(object sender, EventArgs e)
        {
            if (this.textBoxInstrucaoSQL.Text.Trim() != string.Empty)
            {
                this.buttonOK.Enabled = true;
            }
            else
            {
                this.buttonOK.Enabled = false;
            }

        }

        private void comboBoxOutraOrigem_DropDown(object sender, EventArgs e)
        {
            if (this.Tag.ToString() != string.Empty)
            {
                this.Cursor = Cursors.WaitCursor;

                comboBoxOutraOrigem.Items.Clear();

                this.comboBoxCampoValorObter.Items.Clear();

                this.listBoxAtributosOutraOrigem.Items.Clear();

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT " +
                                         "cod_tabela_orig \"Cód. Tabela Origem\" " +
                                  "FROM gsf_tabela_orig " +
                                  "WHERE id_empresa = '" + this.Tag.ToString() + "' " +
                                  "ORDER BY 1";
                cmd.CommandType = CommandType.Text;

                try
                { 
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxOutraOrigem.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();

                this.Cursor = Cursors.Default;
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void buttonCatalogarTabela_Click(object sender, EventArgs e)
        {
            FormTabelasOrigem frmO = new FormTabelasOrigem();
            frmO.Tag = "NOVO";

            frmO.ShowDialog();
        }

        private void comboBoxCampoValorObter_DropDown(object sender, EventArgs e)
        {
            if (this.textBoxTabelaOrigem.Text != string.Empty)
            {
                this.Cursor = Cursors.WaitCursor;

                this.comboBoxCampoValorObter.Items.Clear();

                this.listBoxAtributosOutraOrigem.Items.Clear();

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandType = CommandType.Text;

                if (this.textBoxTabelaOrigem.Text.Contains('@'))
                {

                    cmd.CommandText = "SELECT " +
                                             "column_name  \"Nome da Coluna\" " +
                                      "FROM sys.user_tab_columns@" + this.textBoxTabelaOrigem.Text.Split('@')[1].ToString() + " " +
                                      "WHERE table_name = '" + this.comboBoxOutraOrigem.Text + "' " +
                                      "ORDER BY 1";
                }
                else
                {
                    cmd.CommandText = "SELECT " +
                                             "column_name  \"Nome da Coluna\" " +
                                      "FROM sys.user_tab_columns " +
                                      "WHERE table_name = '" + this.comboBoxOutraOrigem.Text + "' " +
                                      "ORDER BY 1";

                }

                try
                { 
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        this.comboBoxCampoValorObter.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                    }

                    dr.Close();

                    this.Cursor = Cursors.Default;
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void textBoxTabelaOrigem_TextChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxCampoValorObter.Items.Clear();

            this.listBoxAtributosTabelaOrigem.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandType = CommandType.Text;

            if (this.textBoxTabelaOrigem.Text.Contains('@'))
            {

                cmd.CommandText = "SELECT " +
                                         "column_name  \"Nome da Coluna\" " +
                                  "FROM sys.user_tab_columns@" + this.textBoxTabelaOrigem.Text.Split('@')[1].ToString() + " " +
                                  "WHERE table_name = '" + this.textBoxTabelaOrigem.Text.Split('@')[0].ToString() + "' " +
                                  "ORDER BY 1";

            }
            else
            {
                cmd.CommandText = "SELECT " +
                                         "column_name  \"Nome da Coluna\" " +
                                  "FROM sys.user_tab_columns " +
                                  "WHERE table_name = '" + this.textBoxTabelaOrigem.Text + "' " +
                                  "ORDER BY 1";

            }

            try
            { 
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.listBoxAtributosTabelaOrigem.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();

                this.textBoxInstrucaoSQL.Text = this.textBoxTabelaOrigem.Text;

                this.Cursor = Cursors.Default;
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void comboBoxOutraOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.textBoxInstrucaoSQL.Text = this.textBoxTabelaOrigem.Text + " INNER JOIN " + this.comboBoxOutraOrigem.Text;
        }

        private void comboBoxCampoValorObter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCampoValorObter.SelectedItem != null)
            {
                this.Cursor = Cursors.WaitCursor;

                this.listBoxAtributosOutraOrigem.Items.Clear();

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandType = CommandType.Text;

                if (this.textBoxTabelaOrigem.Text.Contains('@'))
                {
                    cmd.CommandText = "SELECT " +
                                             "column_name  \"Nome da Coluna\" " +
                                      "FROM sys.user_tab_columns@" + this.textBoxTabelaOrigem.Text.Split('@')[1].ToString() + " " +
                                      "WHERE table_name = '" + this.comboBoxOutraOrigem.Text + "' " +
                                      "ORDER BY 1";

                }
                else
                {
                    cmd.CommandText = "SELECT " +
                                             "column_name  \"Nome da Coluna\" " +
                                      "FROM sys.user_tab_columns " +
                                      "WHERE table_name = '" + this.comboBoxOutraOrigem.Text + "' " +
                                      "ORDER BY 1";

                }

                try
                { 
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        if (dr[0].ToString() != comboBoxCampoValorObter.SelectedItem.ToString())
                        {
                            this.listBoxAtributosOutraOrigem.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                        }
                    }

                    dr.Close();

                    this.textBoxInstrucaoSQL.Text = "SELECT " + this.comboBoxCampoValorObter.Text + " FROM " + this.textBoxTabelaOrigem.Text + " INNER JOIN " + this.comboBoxOutraOrigem.Text;

                    this.Cursor = Cursors.Default;
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void listBoxAtributosOutraOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBoxCampoValorObter.Text != string.Empty && this.textBoxTabelaOrigem.Text != string.Empty 
                && this.comboBoxOutraOrigem.Text != string.Empty && this.listBoxAtributosTabelaOrigem.SelectedItem != null)
            {
                this.textBoxInstrucaoSQL.Text = "SELECT " + this.comboBoxCampoValorObter.Text + " FROM " + this.textBoxTabelaOrigem.Text + 
                    " INNER JOIN " + this.comboBoxOutraOrigem.SelectedItem + " " +
                    "WHERE " + this.listBoxAtributosTabelaOrigem.SelectedItem + " = " + this.listBoxAtributosOutraOrigem.SelectedItem;
            }

        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.comboBoxCampoValorObter.Text != string.Empty && this.textBoxTabelaOrigem.Text != string.Empty
                && this.comboBoxOutraOrigem.Text != string.Empty && this.listBoxAtributosTabelaOrigem.SelectedItem != null
                && this.listBoxAtributosOutraOrigem.SelectedItem != null)
            {
                FormMapeamento pai = (FormMapeamento)this.Owner;
                pai.listBoxAtributosOrigem.Items.Add("(" + this.textBoxInstrucaoSQL.Text + ")");
                pai.listBoxAtributosOrigem.SelectedIndex = pai.listBoxAtributosOrigem.Items.Count - 1;
            }

            this.Close();

        }

        private void FormOutraOrigem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }
    }
}
