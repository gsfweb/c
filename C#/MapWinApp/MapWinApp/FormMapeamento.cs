﻿using System;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormMapeamento : Form
    {
        public FormMapeamento()
        {
            InitializeComponent();
        }

        public void comboBoxDBLink_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxDBLink.Items.Clear();
            this.comboBoxTabelaOrigem.Items.Clear();
            this.comboBoxTabelasDestino.Items.Clear();
            this.listBoxAtributosOrigem.Items.Clear();
            this.listBoxAtributosDestino.Items.Clear();
            this.listBoxAtributosMapeados.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT t.db_link FROM sys.user_db_links t";
            cmd.CommandType = CommandType.Text;

            try
            { 
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxDBLink.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            this.Cursor = Cursors.Default;
        }

        public void comboBoxEmpresa_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.comboBoxEmpresa.Items.Clear();
            this.comboBoxTabelaOrigem.Items.Clear();
            this.comboBoxTabelasDestino.Items.Clear();
            this.listBoxAtributosOrigem.Items.Clear();
            this.listBoxAtributosDestino.Items.Clear();
            this.listBoxAtributosMapeados.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT id \"Id. Empresa\", nome_empresa \"Nome da Empresa\" " +
                              "FROM gsf_empresa";
            cmd.CommandType = CommandType.Text;

            try
            {
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.comboBoxEmpresa.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length) + " - " + dr[1].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;
        }

        public void comboBoxTabelaOrigem_DropDown(object sender, EventArgs e)
        {
            this.buttonOutraOrigem.Enabled = false;
            this.buttonIncluirMapeamento.Enabled = false;
            this.buttonRemoverMapeamento.Enabled = false;
            this.buttonOutraOrigem.Enabled = false;
            this.buttonNovoCriterioSelecaoOrigem.Enabled = false;
            this.buttonRemoverCriterioSelecaoOrigem.Enabled = false;

            if (this.comboBoxEmpresa.Text != null)
            {
                this.Cursor = Cursors.WaitCursor;

                comboBoxTabelaOrigem.Items.Clear();

                this.listBoxAtributosOrigem.Items.Clear();

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT " +
                                         "cod_tabela_orig \"Cód. Tabela Origem\" " +
                                  "FROM gsf_tabela_orig " +
                                  "WHERE id_empresa = '" + this.comboBoxEmpresa.Text.Split('-')[0].Trim() + "' " +
                                  "ORDER BY 1";
                cmd.CommandType = CommandType.Text;

                try
                {
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        this.comboBoxTabelaOrigem.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                    }

                    dr.Close();
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.Cursor = Cursors.Default;
            }
        }

        public void comboBoxTabelasDestino_DropDown(object sender, EventArgs e)
        {

            if (this.comboBoxDBLink.Text != null && this.comboBoxEmpresa.Text != null)
            {
                this.Cursor = Cursors.WaitCursor;

                comboBoxTabelasDestino.Items.Clear();

                this.listBoxAtributosDestino.Items.Clear();

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT " +
                                         "cod_tabela_dest \"Cód. Tabela Destino\" " +
                                  "FROM gsf_tabela_dest " +
                                  "ORDER BY 1";
                cmd.CommandType = CommandType.Text;

                try
                {
                    OracleDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        this.comboBoxTabelasDestino.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                    }

                    dr.Close();
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.Cursor = Cursors.Default;
            }
        }

        public void comboBoxTabelaOrigem_SelectedValueChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if (this.comboBoxTabelaOrigem.SelectedItem != null)
            {
                this.buttonOutraOrigem.Enabled = true;
                this.buttonNovoCriterioSelecaoOrigem.Enabled = true;
            }
            else
            {
                this.buttonOutraOrigem.Enabled = false;
                this.buttonNovoCriterioSelecaoOrigem.Enabled = false;
            }

            this.listBoxAtributosOrigem.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandType = CommandType.Text;

            if (this.comboBoxDBLink.Text == string.Empty)
            {
                cmd.CommandText = "SELECT " +
                                            "column_name  \"Nome da Coluna\" " +
                                    "FROM sys.user_tab_columns " +
                                    "WHERE table_name = '" + this.comboBoxTabelaOrigem.Text + "'" +
                                    "ORDER BY 1";
            }
            else
            {
                cmd.CommandText = "SELECT " +
                                            "column_name  \"Nome da Coluna\" " +
                                    "FROM sys.user_tab_columns@" + this.comboBoxDBLink.Text + " " +
                                    "WHERE table_name = '" + this.comboBoxTabelaOrigem.Text + "'" +
                                    "ORDER BY 1";

            }

            try
            {
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.listBoxAtributosOrigem.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            this.Cursor = Cursors.Default;
        }   

        public void comboBoxTabelasDestino_SelectedValueChanged(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            OracleDataReader dr;

            this.listBoxAtributosDestino.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT " +
                                     "column_name  \"Nome da Coluna\" " +
                              "FROM sys.user_tab_columns " +
                              "WHERE table_name = '" + this.comboBoxTabelasDestino.Text + "' " +
                              "ORDER BY 1";
            cmd.CommandType = CommandType.Text;

            try
            {
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    this.listBoxAtributosDestino.Items.Add(dr[0].ToString().PadRight(dr[0].ToString().Length));
                }

                dr.Close();
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (this.comboBoxTabelaOrigem.Text != null && this.comboBoxTabelasDestino.Text != null)
            {
                if (this.textBoxIdMapeamento.Text != string.Empty)
                {
                    this.listBoxAtributosMapeados.Items.Clear();

                    cmd.CommandText = "SELECT id_ordem, " +
                                             "atr_sql_orig || ' => ' || cod_atr_dest \"Atributo Mapeado\" " +
                                      "FROM gsf_atr_map " +
                                      "WHERE id_map = " + this.textBoxIdMapeamento.Text + " " +
                                      "ORDER BY 2";

                    try
                    {
                        dr = cmd.ExecuteReader();

                        while (dr.Read())
                        {
                            this.listBoxAtributosMapeados.Items.Add(dr[1].ToString());
                        }

                        this.listBoxAtributosMapeados.Sorted = true;

                        dr.Close();
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            this.Cursor = Cursors.Default;
        }

        private void buttonIncluirMapeamento_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            if(this.comboBoxTabelaOrigem.Text != string.Empty && this.comboBoxTabelasDestino.Text != string.Empty)
            {
                if (this.Tag.ToString() == "NOVO")
                {
                    OracleCommand cmd1 = new OracleCommand();

                    cmd1.Connection = Database.connection;
                    cmd1.CommandText = "Gsf_Map_Pkg.Inserir";
                    cmd1.CommandType = CommandType.StoredProcedure;

                    cmd1.Parameters.Add("p_id_empresa", this.comboBoxEmpresa.Text.Split('-')[0].Trim());
                    if (this.comboBoxDBLink.Text == string.Empty)
                    {
                        cmd1.Parameters.Add("p_id_db_link");
                    }
                    else
                    {
                        cmd1.Parameters.Add("p_id_db_link", this.comboBoxDBLink.Text);
                    }

                    OracleParameter outputPar = new OracleParameter("p_id_map", OracleDbType.Int32);
                    outputPar.Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add(outputPar);

                    cmd1.Parameters.Add("p_cod_tabela_orig", this.comboBoxTabelaOrigem.Text);
                    cmd1.Parameters.Add("p_cod_tabela_dest", this.comboBoxTabelasDestino.Text);
                    cmd1.Parameters.Add("p_cod_modulo", null);

                    try
                    {
                        cmd1.ExecuteNonQuery();

                        try
                        {
                            this.textBoxIdMapeamento.Text = cmd1.Parameters["p_id_map"].Value.ToString();

                            this.comboBoxDBLink.Enabled = false;
                            this.comboBoxEmpresa.Enabled = false;
                            this.comboBoxTabelaOrigem.Enabled = false;
                            this.comboBoxTabelasDestino.Enabled = false;

                            this.Tag = "INCLUIR ATRIBUTOS";
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (OracleException ex)
                    {
                        this.Cursor = Cursors.Default;
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }

                if (this.listBoxAtributosOrigem.SelectedItem != null && this.listBoxAtributosDestino.SelectedItem != null)
                {
                    if (this.textBoxIdMapeamento.Text != string.Empty)
                    {
                        OracleCommand cmd2 = new OracleCommand();

                        cmd2.Connection = Database.connection;
                        cmd2.CommandText = "Gsf_Atr_Map_Pkg.Inserir";
                        cmd2.CommandType = CommandType.StoredProcedure;

                        OracleParameter p1 = new OracleParameter("p_id_map", OracleDbType.Int32);
                        p1.Direction = ParameterDirection.Input;
                        p1.Value = this.textBoxIdMapeamento.Text;
                        cmd2.Parameters.Add(p1);

                        OracleParameter p2 = new OracleParameter("p_id_ordem", OracleDbType.Int32);
                        p2.Direction = ParameterDirection.Output;
                        cmd2.Parameters.Add(p2);

                        OracleParameter p3 = new OracleParameter("p_atr_sql_orig", OracleDbType.Varchar2);
                        p3.Direction = ParameterDirection.Input;
                        p3.Value = this.listBoxAtributosOrigem.SelectedItem;
                        cmd2.Parameters.Add(p3);

                        OracleParameter p4 = new OracleParameter("p_cod_atr_dest", OracleDbType.Varchar2);
                        p4.Direction = ParameterDirection.Input;
                        p4.Value = this.listBoxAtributosDestino.SelectedItem;
                        cmd2.Parameters.Add(p4);

                        try
                        {
                            cmd2.ExecuteNonQuery();

                            try
                            {
                                this.listBoxAtributosMapeados.Items.Add(this.listBoxAtributosOrigem.SelectedItem + " => " + this.listBoxAtributosDestino.SelectedItem);
                                this.ActiveControl = this.listBoxAtributosOrigem;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }

                        }
                        catch (OracleException ex)
                        {
                            MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                        }
                    }
                }
            }

            this.Cursor = Cursors.Default;
        }

        private void panelMapeamento_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBoxDBLink_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonRemoverMapeamento_Click(object sender, EventArgs e)
        {
            if (this.listBoxAtributosMapeados.SelectedItem != null)
            {
                this.Cursor = Cursors.WaitCursor;

                OracleParameter p1 = new OracleParameter("p_id_map", OracleDbType.Int32);
                p1.Direction = ParameterDirection.Input;

                OracleParameter p2 = new OracleParameter("p_atr_sql_orig", OracleDbType.Varchar2);
                p2.Direction = ParameterDirection.Input;

                OracleParameter p3 = new OracleParameter("p_cod_atr_dest", OracleDbType.Varchar2);
                p3.Direction = ParameterDirection.Input;

                OracleCommand cmd1 = new OracleCommand();

                cmd1.Connection = Database.connection;
                cmd1.CommandText = "Gsf_Atr_Map_Pkg.Remover";
                cmd1.CommandType = CommandType.StoredProcedure;

                cmd1.Parameters.Add(p1);
                cmd1.Parameters.Add(p2);
                cmd1.Parameters.Add(p3);

                cmd1.Parameters["p_id_map"].Value = this.textBoxIdMapeamento.Text;
                cmd1.Parameters["p_atr_sql_orig"].Value = this.listBoxAtributosMapeados.Text.Split('=')[0].Trim();
                cmd1.Parameters["p_cod_atr_dest"].Value = this.listBoxAtributosMapeados.Text.Split('>')[1].Trim();

                try
                {
                    cmd1.ExecuteNonQuery();
                    this.listBoxAtributosMapeados.Items.RemoveAt(this.listBoxAtributosMapeados.SelectedIndex);
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine("Exception: {0}", ex.ToString());
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

                
            }
        }

        public void Limpar_Controles(object sender, EventArgs e)
        {
            this.textBoxIdMapeamento.Text = string.Empty;

            this.comboBoxDBLink.Items.Clear();
            this.comboBoxDBLink.Text = string.Empty;
            this.comboBoxDBLink.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxDBLink.Enabled = true;

            this.comboBoxEmpresa.Items.Clear();
            this.comboBoxEmpresa.Text = string.Empty;
            this.comboBoxEmpresa.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxEmpresa.Enabled = true;

            this.comboBoxTabelaOrigem.Items.Clear();
            this.comboBoxTabelaOrigem.Text = string.Empty;
            this.comboBoxTabelaOrigem.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxTabelaOrigem.Enabled = true;

            this.comboBoxTabelasDestino.Items.Clear();
            this.comboBoxTabelasDestino.Text = string.Empty;
            this.comboBoxTabelasDestino.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBoxTabelasDestino.Enabled = true;

            this.listBoxAtributosDestino.Items.Clear();
            this.listBoxAtributosDestino.Enabled = true;

            this.listBoxAtributosMapeados.Items.Clear();
            this.listBoxAtributosMapeados.Enabled = true;

            this.listBoxAtributosOrigem.Items.Clear();
            this.listBoxAtributosOrigem.Enabled = true;

            this.listViewCriteriosSelecaoOrigem.Items.Clear();
            //this.comboBoxEmpresa_DropDown(null, null);
        }

        private void buttonOutraOrigem_Click(object sender, EventArgs e)
        {
            if (this.comboBoxTabelaOrigem.Text != string.Empty)
            {
                FormOutraOrigem frm = new FormOutraOrigem();
                if (this.comboBoxDBLink.Text == string.Empty)
                {
                    frm.textBoxTabelaOrigem.Text = this.comboBoxTabelaOrigem.Text;
                }
                else
                {
                    frm.textBoxTabelaOrigem.Text = this.comboBoxTabelaOrigem.Text + "@" + this.comboBoxDBLink.Text;
                }
                
                frm.Tag = this.comboBoxEmpresa.Text.Split(' ')[0].ToString().Trim();

                frm.ShowDialog(this);
            }
        }

        private void listViewCriteriosSelecao_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listViewCriteriosSelecaoOrigem.SelectedIndices.Count != 0 )
            {
                this.buttonRemoverCriterioSelecaoOrigem.Enabled = true;
            }
            else
            {
                this.buttonRemoverCriterioSelecaoOrigem.Enabled = false;
            }

        }

        private void buttonNovoCriterioSelecaoOrigem_Click(object sender, EventArgs e)
        {
            FormCriteriosSelecao frm = new FormCriteriosSelecao();

            for (int nIndex = 0; nIndex < this.listBoxAtributosOrigem.Items.Count; nIndex++)
            {
                frm.comboBoxAtributosOrigem.Items.Add(this.listBoxAtributosOrigem.Items[nIndex].ToString());
            }

            frm.comboBoxAtributosOrigem.DropDownStyle = ComboBoxStyle.DropDownList;
            frm.comboBoxAtributosOrigem.Enabled = true;

            frm.comboBoxOperadorRelacional.DropDownStyle = ComboBoxStyle.DropDownList;
            frm.comboBoxOperadorRelacional.Enabled = true;

            frm.textBoxValorPesquisa.Enabled = true;

            frm.comboBoxOperadorLogico.DropDownStyle = ComboBoxStyle.DropDownList;
            frm.comboBoxOperadorLogico.Enabled = true;

            frm.buttonIncluirRemover.Text = "Incluir";

            frm.ShowDialog(this);
        }

        private void buttonRemoverCriterioSelecaoOrigem_Click(object sender, EventArgs e)
        {
            if (this.listViewCriteriosSelecaoOrigem.SelectedItems.Count > 0)
            {
                FormCriteriosSelecao frm = new FormCriteriosSelecao();

                frm.comboBoxAtributosOrigem.DropDownStyle = ComboBoxStyle.Simple;
                frm.comboBoxAtributosOrigem.Text = this.listViewCriteriosSelecaoOrigem.SelectedItems[0].SubItems[0].Text;
                frm.comboBoxAtributosOrigem.Enabled = false;

                frm.comboBoxOperadorRelacional.DropDownStyle = ComboBoxStyle.Simple;
                frm.comboBoxOperadorRelacional.Text = this.listViewCriteriosSelecaoOrigem.SelectedItems[0].SubItems[1].Text;
                frm.comboBoxOperadorRelacional.Enabled = false;

                frm.textBoxValorPesquisa.Text = this.listViewCriteriosSelecaoOrigem.SelectedItems[0].SubItems[2].Text;
                frm.textBoxValorPesquisa.Enabled = false;

                frm.comboBoxOperadorLogico.DropDownStyle = ComboBoxStyle.Simple;
                frm.comboBoxOperadorLogico.Text = this.listViewCriteriosSelecaoOrigem.SelectedItems[0].SubItems[3].Text;
                frm.comboBoxOperadorLogico.Enabled = false;

                frm.buttonIncluirRemover.Text = "Remover";

                frm.ShowDialog(this);
            }
        }

        private void textBoxIdMapeamento_TextChanged(object sender, EventArgs e)
        {
            if (textBoxIdMapeamento.Text != string.Empty)
            {
                this.Cursor = Cursors.WaitCursor;

                this.buttonNovoCriterioSelecaoOrigem.Enabled = true;
                this.buttonRemoverMapeamento.Enabled = false;

                this.listViewCriteriosSelecaoOrigem.Items.Clear();

                OracleDataReader dr;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "SELECT " +
                                        "id_map \"Id. Mapa\", " + 
                                        "id_ordem \" Id. Ordem\", " +
                                        "cod_atr_orig \"Atributo\", " +
                                        "oper_relacional \"Operador Relacional\", " +
                                        "condicao \"Condição\", " +
                                        "oper_logico \"Operador Lógico\" " +
                                  "FROM gsf_crit_selecao_map " +
                                  "WHERE id_map = " + this.textBoxIdMapeamento.Text + " " +
                                  "ORDER BY 2";
                cmd.CommandType = CommandType.Text;

                try
                {
                    dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        this.listViewCriteriosSelecaoOrigem.Items.Add(dr[2].ToString());
                        this.listViewCriteriosSelecaoOrigem.Items[this.listViewCriteriosSelecaoOrigem.Items.Count - 1].Tag = dr[1].ToString();
                        this.listViewCriteriosSelecaoOrigem.Items[this.listViewCriteriosSelecaoOrigem.Items.Count - 1].SubItems.Add(dr[3].ToString());
                        this.listViewCriteriosSelecaoOrigem.Items[this.listViewCriteriosSelecaoOrigem.Items.Count - 1].SubItems.Add(dr[4].ToString());
                        this.listViewCriteriosSelecaoOrigem.Items[this.listViewCriteriosSelecaoOrigem.Items.Count - 1].SubItems.Add(dr[5].ToString());
                    }

                    dr.Close();
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.Cursor = Cursors.Default;
            }
            else
            {
                this.buttonNovoCriterioSelecaoOrigem.Enabled = false;
                //this.buttonRemoverCriterioSelecaoOrigem.Enabled = false;
            }
        }

        private void listBoxAtributosDestino_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void VerificaItensListBox(object sender, EventArgs e)
        {
            if (this.listBoxAtributosOrigem.SelectedIndex != -1 && this.listBoxAtributosDestino.SelectedIndex != -1)
            {
                this.buttonIncluirMapeamento.Enabled = true;
            }
            else
            {
                this.buttonIncluirMapeamento.Enabled = false;
            }
        }

        private void FormMapeamento_Load(object sender, EventArgs e)
        {
            this.buttonRemoverCriterioSelecaoOrigem.Enabled = false;
            this.ActiveControl = this.comboBoxDBLink;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.Tag.ToString() == "EXCLUSÃO")
            {

                this.Cursor = Cursors.WaitCursor;

                OracleParameter p1 = new OracleParameter("p_id_map", this.textBoxIdMapeamento.Text);
                p1.Direction = ParameterDirection.Input;

                OracleParameter p2 = new OracleParameter("p_id_log", null);
                p2.Direction = ParameterDirection.Output;

                OracleCommand cmd = new OracleCommand();

                cmd.Connection = Database.connection;
                cmd.CommandText = "Gsf_Map_Pkg.Remover_Cascata";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(p1);
                cmd.Parameters.Add(p2);

                try
                {
                    cmd.ExecuteNonQuery();

                    try
                    {
                        if (cmd.Parameters["p_id_log"].Value.ToString() != string.Empty)
                        {
                            Util.Trata_Retorno(cmd.Parameters["p_id_log"].Value.ToString());
                        }

                        this.Cursor = Cursors.Default;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OracleException ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show(ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void FormMapeamento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void comboBoxTabelasDestino_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxTabelaOrigem_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelEmpresa_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBoxAtributosOrigem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxTabelasDestino_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FormMapeamento_Activated(object sender, EventArgs e)
        {
            this.buttonIncluirMapeamento.Enabled = false;
            this.buttonRemoverMapeamento.Enabled = false;
            this.comboBoxDBLink.Focus();
        }

        private void listBoxAtributosMapeados_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBoxAtributosMapeados.SelectedIndex != -1)
            {
                this.buttonRemoverMapeamento.Enabled = true;
            }
            else
            {
                this.buttonRemoverMapeamento.Enabled = false;
            }
        }
    }
}
