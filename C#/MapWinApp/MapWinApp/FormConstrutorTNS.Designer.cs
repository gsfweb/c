﻿namespace MapWinApp
{
    partial class FormConstrutorTNS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelServiceName = new System.Windows.Forms.Label();
            this.textBoxServiceName = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBoxProtocolParameters = new System.Windows.Forms.GroupBox();
            this.comboBoxProtocolo = new System.Windows.Forms.ComboBox();
            this.labelProtocol = new System.Windows.Forms.Label();
            this.labelKeyPipePort = new System.Windows.Forms.Label();
            this.textBoxKeyPipePort = new System.Windows.Forms.TextBox();
            this.labelHostServer = new System.Windows.Forms.Label();
            this.textBoxHostServer = new System.Windows.Forms.TextBox();
            this.groupBoxProtocolParameters.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelServiceName
            // 
            this.labelServiceName.AutoSize = true;
            this.labelServiceName.Location = new System.Drawing.Point(9, 185);
            this.labelServiceName.Name = "labelServiceName";
            this.labelServiceName.Size = new System.Drawing.Size(77, 13);
            this.labelServiceName.TabIndex = 6;
            this.labelServiceName.Text = "Service Name:";
            // 
            // textBoxServiceName
            // 
            this.textBoxServiceName.Location = new System.Drawing.Point(12, 201);
            this.textBoxServiceName.Name = "textBoxServiceName";
            this.textBoxServiceName.Size = new System.Drawing.Size(123, 20);
            this.textBoxServiceName.TabIndex = 7;
            this.textBoxServiceName.TextChanged += new System.EventHandler(this.textBoxServidor_TextChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(271, 218);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // groupBoxProtocolParameters
            // 
            this.groupBoxProtocolParameters.Controls.Add(this.comboBoxProtocolo);
            this.groupBoxProtocolParameters.Controls.Add(this.labelProtocol);
            this.groupBoxProtocolParameters.Controls.Add(this.labelKeyPipePort);
            this.groupBoxProtocolParameters.Controls.Add(this.textBoxKeyPipePort);
            this.groupBoxProtocolParameters.Controls.Add(this.labelHostServer);
            this.groupBoxProtocolParameters.Controls.Add(this.textBoxHostServer);
            this.groupBoxProtocolParameters.Location = new System.Drawing.Point(12, 12);
            this.groupBoxProtocolParameters.Name = "groupBoxProtocolParameters";
            this.groupBoxProtocolParameters.Size = new System.Drawing.Size(334, 163);
            this.groupBoxProtocolParameters.TabIndex = 10;
            this.groupBoxProtocolParameters.TabStop = false;
            this.groupBoxProtocolParameters.Text = "Protocol Parameters";
            // 
            // comboBoxProtocolo
            // 
            this.comboBoxProtocolo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProtocolo.FormattingEnabled = true;
            this.comboBoxProtocolo.Items.AddRange(new object[] {
            "IPC",
            "Named Pipes",
            "SDP",
            "TCP/IP",
            "TCP/IP with SSL"});
            this.comboBoxProtocolo.Location = new System.Drawing.Point(6, 37);
            this.comboBoxProtocolo.Name = "comboBoxProtocolo";
            this.comboBoxProtocolo.Size = new System.Drawing.Size(121, 21);
            this.comboBoxProtocolo.TabIndex = 1;
            this.comboBoxProtocolo.SelectedIndexChanged += new System.EventHandler(this.comboBoxProtocolo_SelectedIndexChanged);
            // 
            // labelProtocol
            // 
            this.labelProtocol.AutoSize = true;
            this.labelProtocol.Location = new System.Drawing.Point(4, 21);
            this.labelProtocol.Name = "labelProtocol";
            this.labelProtocol.Size = new System.Drawing.Size(49, 13);
            this.labelProtocol.TabIndex = 0;
            this.labelProtocol.Text = "Protocol:";
            // 
            // labelKeyPipePort
            // 
            this.labelKeyPipePort.AutoSize = true;
            this.labelKeyPipePort.Location = new System.Drawing.Point(4, 118);
            this.labelKeyPipePort.Name = "labelKeyPipePort";
            this.labelKeyPipePort.Size = new System.Drawing.Size(78, 13);
            this.labelKeyPipePort.TabIndex = 4;
            this.labelKeyPipePort.Text = "Key/Pipe/Port:";
            // 
            // textBoxKeyPipePort
            // 
            this.textBoxKeyPipePort.Location = new System.Drawing.Point(7, 134);
            this.textBoxKeyPipePort.Name = "textBoxKeyPipePort";
            this.textBoxKeyPipePort.Size = new System.Drawing.Size(57, 20);
            this.textBoxKeyPipePort.TabIndex = 5;
            // 
            // labelHostServer
            // 
            this.labelHostServer.AutoSize = true;
            this.labelHostServer.Location = new System.Drawing.Point(4, 70);
            this.labelHostServer.Name = "labelHostServer";
            this.labelHostServer.Size = new System.Drawing.Size(68, 13);
            this.labelHostServer.TabIndex = 2;
            this.labelHostServer.Text = "Host/Server:";
            // 
            // textBoxHostServer
            // 
            this.textBoxHostServer.Location = new System.Drawing.Point(7, 86);
            this.textBoxHostServer.Name = "textBoxHostServer";
            this.textBoxHostServer.Size = new System.Drawing.Size(310, 20);
            this.textBoxHostServer.TabIndex = 3;
            // 
            // FormConstrutorTNS
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 253);
            this.Controls.Add(this.groupBoxProtocolParameters);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelServiceName);
            this.Controls.Add(this.textBoxServiceName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConstrutorTNS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Construtor TNS";
            this.Load += new System.EventHandler(this.FormConstrutorTNS_Load);
            this.groupBoxProtocolParameters.ResumeLayout(false);
            this.groupBoxProtocolParameters.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelServiceName;
        public System.Windows.Forms.TextBox textBoxServiceName;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.GroupBox groupBoxProtocolParameters;
        public System.Windows.Forms.ComboBox comboBoxProtocolo;
        private System.Windows.Forms.Label labelProtocol;
        private System.Windows.Forms.Label labelKeyPipePort;
        public System.Windows.Forms.TextBox textBoxKeyPipePort;
        private System.Windows.Forms.Label labelHostServer;
        public System.Windows.Forms.TextBox textBoxHostServer;
    }
}