﻿namespace MapWinApp
{
    partial class FormTarefas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTarefas));
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelIdTarefa = new System.Windows.Forms.Label();
            this.textBoxIdTarefa = new System.Windows.Forms.TextBox();
            this.labelIdMap = new System.Windows.Forms.Label();
            this.textBoxIdMapeamento = new System.Windows.Forms.TextBox();
            this.buttonPesquisaIdMap = new System.Windows.Forms.Button();
            this.labelDescricao = new System.Windows.Forms.Label();
            this.textBoxDescricao = new System.Windows.Forms.TextBox();
            this.checkBoxExecutar = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(284, 218);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 9;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelIdTarefa
            // 
            this.labelIdTarefa.AutoSize = true;
            this.labelIdTarefa.Location = new System.Drawing.Point(12, 9);
            this.labelIdTarefa.Name = "labelIdTarefa";
            this.labelIdTarefa.Size = new System.Drawing.Size(56, 13);
            this.labelIdTarefa.TabIndex = 10;
            this.labelIdTarefa.Text = "Id. Tarefa:";
            // 
            // textBoxIdTarefa
            // 
            this.textBoxIdTarefa.Enabled = false;
            this.textBoxIdTarefa.Location = new System.Drawing.Point(15, 25);
            this.textBoxIdTarefa.Name = "textBoxIdTarefa";
            this.textBoxIdTarefa.Size = new System.Drawing.Size(84, 20);
            this.textBoxIdTarefa.TabIndex = 11;
            // 
            // labelIdMap
            // 
            this.labelIdMap.AutoSize = true;
            this.labelIdMap.Location = new System.Drawing.Point(12, 52);
            this.labelIdMap.Name = "labelIdMap";
            this.labelIdMap.Size = new System.Drawing.Size(87, 13);
            this.labelIdMap.TabIndex = 12;
            this.labelIdMap.Text = "Id. Mapeamento:";
            // 
            // textBoxIdMapeamento
            // 
            this.textBoxIdMapeamento.Location = new System.Drawing.Point(15, 68);
            this.textBoxIdMapeamento.Name = "textBoxIdMapeamento";
            this.textBoxIdMapeamento.Size = new System.Drawing.Size(84, 20);
            this.textBoxIdMapeamento.TabIndex = 13;
            this.textBoxIdMapeamento.TextChanged += new System.EventHandler(this.textBoxIdMapeamento_TextChanged);
            // 
            // buttonPesquisaIdMap
            // 
            this.buttonPesquisaIdMap.Image = ((System.Drawing.Image)(resources.GetObject("buttonPesquisaIdMap.Image")));
            this.buttonPesquisaIdMap.Location = new System.Drawing.Point(105, 66);
            this.buttonPesquisaIdMap.Name = "buttonPesquisaIdMap";
            this.buttonPesquisaIdMap.Size = new System.Drawing.Size(26, 23);
            this.buttonPesquisaIdMap.TabIndex = 14;
            this.buttonPesquisaIdMap.UseVisualStyleBackColor = true;
            this.buttonPesquisaIdMap.Click += new System.EventHandler(this.buttonPesquisaIdMap_Click);
            // 
            // labelDescricao
            // 
            this.labelDescricao.AutoSize = true;
            this.labelDescricao.Location = new System.Drawing.Point(12, 96);
            this.labelDescricao.Name = "labelDescricao";
            this.labelDescricao.Size = new System.Drawing.Size(58, 13);
            this.labelDescricao.TabIndex = 15;
            this.labelDescricao.Text = "Descrição:";
            // 
            // textBoxDescricao
            // 
            this.textBoxDescricao.Location = new System.Drawing.Point(15, 112);
            this.textBoxDescricao.Multiline = true;
            this.textBoxDescricao.Name = "textBoxDescricao";
            this.textBoxDescricao.Size = new System.Drawing.Size(344, 53);
            this.textBoxDescricao.TabIndex = 16;
            // 
            // checkBoxExecutar
            // 
            this.checkBoxExecutar.AutoSize = true;
            this.checkBoxExecutar.Location = new System.Drawing.Point(16, 176);
            this.checkBoxExecutar.Name = "checkBoxExecutar";
            this.checkBoxExecutar.Size = new System.Drawing.Size(145, 17);
            this.checkBoxExecutar.TabIndex = 17;
            this.checkBoxExecutar.Text = "Executar carga de dados";
            this.checkBoxExecutar.UseVisualStyleBackColor = true;
            this.checkBoxExecutar.CheckedChanged += new System.EventHandler(this.checkBoxExecutar_CheckedChanged);
            // 
            // FormTarefas
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 254);
            this.Controls.Add(this.checkBoxExecutar);
            this.Controls.Add(this.textBoxDescricao);
            this.Controls.Add(this.labelDescricao);
            this.Controls.Add(this.buttonPesquisaIdMap);
            this.Controls.Add(this.textBoxIdMapeamento);
            this.Controls.Add(this.labelIdMap);
            this.Controls.Add(this.textBoxIdTarefa);
            this.Controls.Add(this.labelIdTarefa);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTarefas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tarefas";
            this.Load += new System.EventHandler(this.FormTarefas_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormTarefas_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelIdTarefa;
        public System.Windows.Forms.TextBox textBoxIdTarefa;
        private System.Windows.Forms.Label labelIdMap;
        public System.Windows.Forms.TextBox textBoxIdMapeamento;
        public System.Windows.Forms.Button buttonPesquisaIdMap;
        private System.Windows.Forms.Label labelDescricao;
        public System.Windows.Forms.TextBox textBoxDescricao;
        public System.Windows.Forms.CheckBox checkBoxExecutar;
    }
}