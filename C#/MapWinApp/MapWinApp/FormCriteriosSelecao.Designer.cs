﻿namespace MapWinApp
{
    partial class FormCriteriosSelecao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCriteriosSelecao));
            this.buttonIncluirRemover = new System.Windows.Forms.Button();
            this.comboBoxAtributosOrigem = new System.Windows.Forms.ComboBox();
            this.labelAtributosOrigem = new System.Windows.Forms.Label();
            this.labelOperadorRelacional = new System.Windows.Forms.Label();
            this.comboBoxOperadorLogico = new System.Windows.Forms.ComboBox();
            this.comboBoxOperadorRelacional = new System.Windows.Forms.ComboBox();
            this.labelValorPesquisa = new System.Windows.Forms.Label();
            this.textBoxValorPesquisa = new System.Windows.Forms.TextBox();
            this.labelOperadorLogico = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonIncluirRemover
            // 
            this.buttonIncluirRemover.Enabled = false;
            this.buttonIncluirRemover.Location = new System.Drawing.Point(266, 213);
            this.buttonIncluirRemover.Name = "buttonIncluirRemover";
            this.buttonIncluirRemover.Size = new System.Drawing.Size(75, 23);
            this.buttonIncluirRemover.TabIndex = 9;
            this.buttonIncluirRemover.Text = "&ND";
            this.buttonIncluirRemover.Click += new System.EventHandler(this.buttonIncluirRemover_Click);
            // 
            // comboBoxAtributosOrigem
            // 
            this.comboBoxAtributosOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAtributosOrigem.FormattingEnabled = true;
            this.comboBoxAtributosOrigem.Location = new System.Drawing.Point(15, 29);
            this.comboBoxAtributosOrigem.Name = "comboBoxAtributosOrigem";
            this.comboBoxAtributosOrigem.Size = new System.Drawing.Size(180, 21);
            this.comboBoxAtributosOrigem.TabIndex = 10;
            this.comboBoxAtributosOrigem.SelectedValueChanged += new System.EventHandler(this.VerificaDados);
            // 
            // labelAtributosOrigem
            // 
            this.labelAtributosOrigem.AutoSize = true;
            this.labelAtributosOrigem.Location = new System.Drawing.Point(12, 13);
            this.labelAtributosOrigem.Name = "labelAtributosOrigem";
            this.labelAtributosOrigem.Size = new System.Drawing.Size(46, 13);
            this.labelAtributosOrigem.TabIndex = 13;
            this.labelAtributosOrigem.Text = "Atributo:";
            // 
            // labelOperadorRelacional
            // 
            this.labelOperadorRelacional.AutoSize = true;
            this.labelOperadorRelacional.Location = new System.Drawing.Point(12, 63);
            this.labelOperadorRelacional.Name = "labelOperadorRelacional";
            this.labelOperadorRelacional.Size = new System.Drawing.Size(107, 13);
            this.labelOperadorRelacional.TabIndex = 14;
            this.labelOperadorRelacional.Text = "Operador Relacional:";
            // 
            // comboBoxOperadorLogico
            // 
            this.comboBoxOperadorLogico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOperadorLogico.FormattingEnabled = true;
            this.comboBoxOperadorLogico.Items.AddRange(new object[] {
            "E",
            "Ou"});
            this.comboBoxOperadorLogico.Location = new System.Drawing.Point(15, 174);
            this.comboBoxOperadorLogico.Name = "comboBoxOperadorLogico";
            this.comboBoxOperadorLogico.Size = new System.Drawing.Size(46, 21);
            this.comboBoxOperadorLogico.TabIndex = 15;
            this.comboBoxOperadorLogico.SelectedIndexChanged += new System.EventHandler(this.comboBoxOperadorLogico_SelectedIndexChanged);
            this.comboBoxOperadorLogico.SelectedValueChanged += new System.EventHandler(this.VerificaDados);
            // 
            // comboBoxOperadorRelacional
            // 
            this.comboBoxOperadorRelacional.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOperadorRelacional.FormattingEnabled = true;
            this.comboBoxOperadorRelacional.Items.AddRange(new object[] {
            "=",
            ">",
            ">=",
            "<",
            "<="});
            this.comboBoxOperadorRelacional.Location = new System.Drawing.Point(15, 80);
            this.comboBoxOperadorRelacional.Name = "comboBoxOperadorRelacional";
            this.comboBoxOperadorRelacional.Size = new System.Drawing.Size(43, 21);
            this.comboBoxOperadorRelacional.TabIndex = 16;
            this.comboBoxOperadorRelacional.SelectedValueChanged += new System.EventHandler(this.VerificaDados);
            // 
            // labelValorPesquisa
            // 
            this.labelValorPesquisa.AutoSize = true;
            this.labelValorPesquisa.Location = new System.Drawing.Point(12, 113);
            this.labelValorPesquisa.Name = "labelValorPesquisa";
            this.labelValorPesquisa.Size = new System.Drawing.Size(95, 13);
            this.labelValorPesquisa.TabIndex = 17;
            this.labelValorPesquisa.Text = "Valor de Pesquisa:";
            // 
            // textBoxValorPesquisa
            // 
            this.textBoxValorPesquisa.Location = new System.Drawing.Point(15, 130);
            this.textBoxValorPesquisa.Name = "textBoxValorPesquisa";
            this.textBoxValorPesquisa.Size = new System.Drawing.Size(180, 20);
            this.textBoxValorPesquisa.TabIndex = 18;
            this.textBoxValorPesquisa.TextChanged += new System.EventHandler(this.VerificaDados);
            // 
            // labelOperadorLogico
            // 
            this.labelOperadorLogico.AutoSize = true;
            this.labelOperadorLogico.Location = new System.Drawing.Point(12, 158);
            this.labelOperadorLogico.Name = "labelOperadorLogico";
            this.labelOperadorLogico.Size = new System.Drawing.Size(89, 13);
            this.labelOperadorLogico.TabIndex = 19;
            this.labelOperadorLogico.Text = "Operador Lógico:";
            // 
            // FormCriteriosSelecao
            // 
            this.AcceptButton = this.buttonIncluirRemover;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 244);
            this.Controls.Add(this.labelOperadorLogico);
            this.Controls.Add(this.textBoxValorPesquisa);
            this.Controls.Add(this.labelValorPesquisa);
            this.Controls.Add(this.comboBoxOperadorRelacional);
            this.Controls.Add(this.comboBoxOperadorLogico);
            this.Controls.Add(this.labelOperadorRelacional);
            this.Controls.Add(this.labelAtributosOrigem);
            this.Controls.Add(this.comboBoxAtributosOrigem);
            this.Controls.Add(this.buttonIncluirRemover);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCriteriosSelecao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Critérios de Seleção";
            this.Load += new System.EventHandler(this.FormCriteriosSelecao_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormCriteriosSelecao_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button buttonIncluirRemover;
        public System.Windows.Forms.ComboBox comboBoxAtributosOrigem;
        private System.Windows.Forms.Label labelAtributosOrigem;
        private System.Windows.Forms.Label labelOperadorRelacional;
        public System.Windows.Forms.ComboBox comboBoxOperadorLogico;
        public System.Windows.Forms.ComboBox comboBoxOperadorRelacional;
        private System.Windows.Forms.Label labelValorPesquisa;
        public System.Windows.Forms.TextBox textBoxValorPesquisa;
        private System.Windows.Forms.Label labelOperadorLogico;
    }
}