﻿namespace MapWinApp
{
    partial class FormOutraOrigem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormOutraOrigem));
            this.labelTabelaOrigem = new System.Windows.Forms.Label();
            this.textBoxTabelaOrigem = new System.Windows.Forms.TextBox();
            this.labelOutraOrigem = new System.Windows.Forms.Label();
            this.comboBoxOutraOrigem = new System.Windows.Forms.ComboBox();
            this.labelCampoValorObter = new System.Windows.Forms.Label();
            this.comboBoxCampoValorObter = new System.Windows.Forms.ComboBox();
            this.groupBoxLigacao = new System.Windows.Forms.GroupBox();
            this.labelAtributosOutraOrigem = new System.Windows.Forms.Label();
            this.labelAtributosTabelaOrigem = new System.Windows.Forms.Label();
            this.listBoxAtributosOutraOrigem = new System.Windows.Forms.ListBox();
            this.listBoxAtributosTabelaOrigem = new System.Windows.Forms.ListBox();
            this.labelInstrucaoSQL = new System.Windows.Forms.Label();
            this.textBoxInstrucaoSQL = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCatalogarTabela = new System.Windows.Forms.Button();
            this.groupBoxLigacao.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelTabelaOrigem
            // 
            this.labelTabelaOrigem.AutoSize = true;
            this.labelTabelaOrigem.Location = new System.Drawing.Point(8, 9);
            this.labelTabelaOrigem.Name = "labelTabelaOrigem";
            this.labelTabelaOrigem.Size = new System.Drawing.Size(79, 13);
            this.labelTabelaOrigem.TabIndex = 0;
            this.labelTabelaOrigem.Text = "Tabela Origem:";
            // 
            // textBoxTabelaOrigem
            // 
            this.textBoxTabelaOrigem.Location = new System.Drawing.Point(11, 25);
            this.textBoxTabelaOrigem.Name = "textBoxTabelaOrigem";
            this.textBoxTabelaOrigem.ReadOnly = true;
            this.textBoxTabelaOrigem.Size = new System.Drawing.Size(305, 20);
            this.textBoxTabelaOrigem.TabIndex = 1;
            this.textBoxTabelaOrigem.TextChanged += new System.EventHandler(this.textBoxTabelaOrigem_TextChanged);
            // 
            // labelOutraOrigem
            // 
            this.labelOutraOrigem.AutoSize = true;
            this.labelOutraOrigem.Location = new System.Drawing.Point(8, 52);
            this.labelOutraOrigem.Name = "labelOutraOrigem";
            this.labelOutraOrigem.Size = new System.Drawing.Size(123, 13);
            this.labelOutraOrigem.TabIndex = 2;
            this.labelOutraOrigem.Text = "Tabela de Outra Origem:";
            // 
            // comboBoxOutraOrigem
            // 
            this.comboBoxOutraOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOutraOrigem.FormattingEnabled = true;
            this.comboBoxOutraOrigem.Location = new System.Drawing.Point(11, 68);
            this.comboBoxOutraOrigem.Name = "comboBoxOutraOrigem";
            this.comboBoxOutraOrigem.Size = new System.Drawing.Size(174, 21);
            this.comboBoxOutraOrigem.TabIndex = 3;
            this.comboBoxOutraOrigem.DropDown += new System.EventHandler(this.comboBoxOutraOrigem_DropDown);
            this.comboBoxOutraOrigem.SelectedIndexChanged += new System.EventHandler(this.comboBoxOutraOrigem_SelectedIndexChanged);
            // 
            // labelCampoValorObter
            // 
            this.labelCampoValorObter.AutoSize = true;
            this.labelCampoValorObter.Location = new System.Drawing.Point(8, 98);
            this.labelCampoValorObter.Name = "labelCampoValorObter";
            this.labelCampoValorObter.Size = new System.Drawing.Size(120, 13);
            this.labelCampoValorObter.TabIndex = 4;
            this.labelCampoValorObter.Text = "Campo do valor à obter:";
            // 
            // comboBoxCampoValorObter
            // 
            this.comboBoxCampoValorObter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCampoValorObter.FormattingEnabled = true;
            this.comboBoxCampoValorObter.Location = new System.Drawing.Point(11, 114);
            this.comboBoxCampoValorObter.Name = "comboBoxCampoValorObter";
            this.comboBoxCampoValorObter.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCampoValorObter.TabIndex = 5;
            this.comboBoxCampoValorObter.DropDown += new System.EventHandler(this.comboBoxCampoValorObter_DropDown);
            this.comboBoxCampoValorObter.SelectedIndexChanged += new System.EventHandler(this.comboBoxCampoValorObter_SelectedIndexChanged);
            // 
            // groupBoxLigacao
            // 
            this.groupBoxLigacao.Controls.Add(this.labelAtributosOutraOrigem);
            this.groupBoxLigacao.Controls.Add(this.labelAtributosTabelaOrigem);
            this.groupBoxLigacao.Controls.Add(this.listBoxAtributosOutraOrigem);
            this.groupBoxLigacao.Controls.Add(this.listBoxAtributosTabelaOrigem);
            this.groupBoxLigacao.Location = new System.Drawing.Point(12, 147);
            this.groupBoxLigacao.Name = "groupBoxLigacao";
            this.groupBoxLigacao.Size = new System.Drawing.Size(432, 125);
            this.groupBoxLigacao.TabIndex = 6;
            this.groupBoxLigacao.TabStop = false;
            this.groupBoxLigacao.Text = "Ligação";
            this.groupBoxLigacao.Enter += new System.EventHandler(this.groupBoxLigacao_Enter);
            // 
            // labelAtributosOutraOrigem
            // 
            this.labelAtributosOutraOrigem.AutoSize = true;
            this.labelAtributosOutraOrigem.Location = new System.Drawing.Point(234, 25);
            this.labelAtributosOutraOrigem.Name = "labelAtributosOutraOrigem";
            this.labelAtributosOutraOrigem.Size = new System.Drawing.Size(182, 13);
            this.labelAtributosOutraOrigem.TabIndex = 17;
            this.labelAtributosOutraOrigem.Text = "Atributos da Tabela de Outra Origem:";
            // 
            // labelAtributosTabelaOrigem
            // 
            this.labelAtributosTabelaOrigem.AutoSize = true;
            this.labelAtributosTabelaOrigem.Location = new System.Drawing.Point(12, 25);
            this.labelAtributosTabelaOrigem.Name = "labelAtributosTabelaOrigem";
            this.labelAtributosTabelaOrigem.Size = new System.Drawing.Size(153, 13);
            this.labelAtributosTabelaOrigem.TabIndex = 15;
            this.labelAtributosTabelaOrigem.Text = "Atributos da Tabela de Origem:";
            // 
            // listBoxAtributosOutraOrigem
            // 
            this.listBoxAtributosOutraOrigem.FormattingEnabled = true;
            this.listBoxAtributosOutraOrigem.Location = new System.Drawing.Point(237, 41);
            this.listBoxAtributosOutraOrigem.Name = "listBoxAtributosOutraOrigem";
            this.listBoxAtributosOutraOrigem.Size = new System.Drawing.Size(178, 69);
            this.listBoxAtributosOutraOrigem.TabIndex = 13;
            this.listBoxAtributosOutraOrigem.SelectedIndexChanged += new System.EventHandler(this.listBoxAtributosOutraOrigem_SelectedIndexChanged);
            // 
            // listBoxAtributosTabelaOrigem
            // 
            this.listBoxAtributosTabelaOrigem.FormattingEnabled = true;
            this.listBoxAtributosTabelaOrigem.Location = new System.Drawing.Point(15, 41);
            this.listBoxAtributosTabelaOrigem.Name = "listBoxAtributosTabelaOrigem";
            this.listBoxAtributosTabelaOrigem.Size = new System.Drawing.Size(178, 69);
            this.listBoxAtributosTabelaOrigem.TabIndex = 12;
            this.listBoxAtributosTabelaOrigem.SelectedIndexChanged += new System.EventHandler(this.listBoxAtributosOutraOrigem_SelectedIndexChanged);
            // 
            // labelInstrucaoSQL
            // 
            this.labelInstrucaoSQL.AutoSize = true;
            this.labelInstrucaoSQL.Location = new System.Drawing.Point(8, 280);
            this.labelInstrucaoSQL.Name = "labelInstrucaoSQL";
            this.labelInstrucaoSQL.Size = new System.Drawing.Size(78, 13);
            this.labelInstrucaoSQL.TabIndex = 7;
            this.labelInstrucaoSQL.Text = "Instrução SQL:";
            // 
            // textBoxInstrucaoSQL
            // 
            this.textBoxInstrucaoSQL.Location = new System.Drawing.Point(12, 296);
            this.textBoxInstrucaoSQL.Multiline = true;
            this.textBoxInstrucaoSQL.Name = "textBoxInstrucaoSQL";
            this.textBoxInstrucaoSQL.ReadOnly = true;
            this.textBoxInstrucaoSQL.Size = new System.Drawing.Size(432, 34);
            this.textBoxInstrucaoSQL.TabIndex = 8;
            this.textBoxInstrucaoSQL.TextChanged += new System.EventHandler(this.textBoxInstrucaoSQL_TextChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(369, 336);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 9;
            this.buttonOK.Text = "&OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCatalogarTabela
            // 
            this.buttonCatalogarTabela.Location = new System.Drawing.Point(191, 68);
            this.buttonCatalogarTabela.Name = "buttonCatalogarTabela";
            this.buttonCatalogarTabela.Size = new System.Drawing.Size(25, 23);
            this.buttonCatalogarTabela.TabIndex = 10;
            this.buttonCatalogarTabela.Text = "...";
            this.buttonCatalogarTabela.UseVisualStyleBackColor = true;
            this.buttonCatalogarTabela.Click += new System.EventHandler(this.buttonCatalogarTabela_Click);
            // 
            // FormOutraOrigem
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 371);
            this.Controls.Add(this.buttonCatalogarTabela);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxInstrucaoSQL);
            this.Controls.Add(this.labelInstrucaoSQL);
            this.Controls.Add(this.groupBoxLigacao);
            this.Controls.Add(this.comboBoxCampoValorObter);
            this.Controls.Add(this.labelCampoValorObter);
            this.Controls.Add(this.comboBoxOutraOrigem);
            this.Controls.Add(this.labelOutraOrigem);
            this.Controls.Add(this.textBoxTabelaOrigem);
            this.Controls.Add(this.labelTabelaOrigem);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormOutraOrigem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Outra Origem";
            this.Load += new System.EventHandler(this.FormOutraOrigem_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormOutraOrigem_KeyDown);
            this.groupBoxLigacao.ResumeLayout(false);
            this.groupBoxLigacao.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTabelaOrigem;
        public System.Windows.Forms.TextBox textBoxTabelaOrigem;
        private System.Windows.Forms.Label labelOutraOrigem;
        private System.Windows.Forms.ComboBox comboBoxOutraOrigem;
        private System.Windows.Forms.Label labelCampoValorObter;
        private System.Windows.Forms.ComboBox comboBoxCampoValorObter;
        private System.Windows.Forms.GroupBox groupBoxLigacao;
        private System.Windows.Forms.Label labelInstrucaoSQL;
        private System.Windows.Forms.TextBox textBoxInstrucaoSQL;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCatalogarTabela;
        public System.Windows.Forms.ListBox listBoxAtributosTabelaOrigem;
        private System.Windows.Forms.ListBox listBoxAtributosOutraOrigem;
        private System.Windows.Forms.Label labelAtributosTabelaOrigem;
        private System.Windows.Forms.Label labelAtributosOutraOrigem;
    }
}