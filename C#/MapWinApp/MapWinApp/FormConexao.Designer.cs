﻿namespace MapWinApp
{
    partial class FormConexao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConexao));
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelPassword = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.labelTNSName = new System.Windows.Forms.Label();
            this.textBoxTNSName = new System.Windows.Forms.TextBox();
            this.buttonConstrutorTNSName = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(15, 75);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(167, 20);
            this.textBoxUserName.TabIndex = 4;
            this.textBoxUserName.TextChanged += new System.EventHandler(this.textBoxUserName_TextChanged);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(15, 117);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(167, 20);
            this.textBoxPassword.TabIndex = 6;
            this.textBoxPassword.TextChanged += new System.EventHandler(this.textBoxPassword_TextChanged);
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(16, 60);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(46, 13);
            this.labelUserName.TabIndex = 3;
            this.labelUserName.Text = "Usuário:";
            this.labelUserName.Click += new System.EventHandler(this.labelUserName_Click);
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(15, 101);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(41, 13);
            this.labelPassword.TabIndex = 6;
            this.labelPassword.Text = "Senha:";
            this.labelPassword.Click += new System.EventHandler(this.labelPassword_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(252, 165);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 7;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // labelTNSName
            // 
            this.labelTNSName.AutoSize = true;
            this.labelTNSName.Location = new System.Drawing.Point(12, 15);
            this.labelTNSName.Name = "labelTNSName";
            this.labelTNSName.Size = new System.Drawing.Size(63, 13);
            this.labelTNSName.TabIndex = 0;
            this.labelTNSName.Text = "TNS Name:";
            this.labelTNSName.Click += new System.EventHandler(this.labelTNSName_Click);
            // 
            // textBoxTNSName
            // 
            this.textBoxTNSName.Location = new System.Drawing.Point(15, 31);
            this.textBoxTNSName.Name = "textBoxTNSName";
            this.textBoxTNSName.Size = new System.Drawing.Size(280, 20);
            this.textBoxTNSName.TabIndex = 1;
            // 
            // buttonConstrutorTNSName
            // 
            this.buttonConstrutorTNSName.Image = ((System.Drawing.Image)(resources.GetObject("buttonConstrutorTNSName.Image")));
            this.buttonConstrutorTNSName.Location = new System.Drawing.Point(301, 29);
            this.buttonConstrutorTNSName.Name = "buttonConstrutorTNSName";
            this.buttonConstrutorTNSName.Size = new System.Drawing.Size(26, 23);
            this.buttonConstrutorTNSName.TabIndex = 2;
            this.buttonConstrutorTNSName.UseVisualStyleBackColor = true;
            this.buttonConstrutorTNSName.Click += new System.EventHandler(this.buttonConstrutorTNSName_Click);
            // 
            // FormConexao
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 200);
            this.Controls.Add(this.buttonConstrutorTNSName);
            this.Controls.Add(this.textBoxTNSName);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.textBoxPassword);
            this.Controls.Add(this.textBoxUserName);
            this.Controls.Add(this.labelTNSName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormConexao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MapWinApp - Conexão ao Banco de Dados";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.FormConexao_HelpButtonClicked);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormConexao_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormConexao_FormClosed);
            this.Load += new System.EventHandler(this.FormConexao_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormConexao_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelTNSName;
        public System.Windows.Forms.TextBox textBoxTNSName;
        private System.Windows.Forms.Button buttonConstrutorTNSName;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Button buttonOK;
    }
}