﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.OleDb;
using Oracle.DataAccess.Client;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormSelecionarMapeamento : Form
    {
        private int sortColumn = -1;

        public FormSelecionarMapeamento()
        {
            InitializeComponent();
        }

        private void FormSelecionarMapeamento_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            this.listViewMapeamentos.Items.Clear();

            OracleCommand cmd = new OracleCommand();

            cmd.Connection = Database.connection;
            cmd.CommandText = "SELECT " +
                                    "id_map \"Id. Mapeamento\", " +
                                    "TO_CHAR(data_map, 'DD/MM/YYYY HH24:MI:SS') \"Data Mapeamento\", " +
                                    "id_db_link \"Id. DBLink\", " +
                                    "id_empresa \"Id. Empresa\", " +
                                    "\"Nome da Empresa\", " +
                                    "cod_modulo \"Cód. Modulo\", " +
                                    "cod_tabela_orig \"Tabela Origem\", " +
                                    "\"Descrição da Tabela de Origem\", " +
                                    "cod_tabela_dest \"Tabela Destino\", " +
                                    "\"Descrição da Tabela de Destino\", " +
                                    "id_objeto \"Id. Objeto\" " +
                              "FROM   gsf_map t ORDER BY 1";
            cmd.CommandType = CommandType.Text;

            try
            { 
                OracleDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    if (this.listViewMapeamentos.Columns.Count == 0)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            string col = dr.GetName(i).ToString();
                            if (col != "Id. Objeto")
                            {
                                this.listViewMapeamentos.Columns.Add(col, col.Length * 20);
                            }
                        }
                    }

                    ListViewItem lv = new ListViewItem(dr[0].ToString());

                    for (int i = 1; i < dr.FieldCount; i++)
                    {
                        string col = dr[i].ToString();

                        if (col != "Id. Objeto")
                        {
                            lv.SubItems.Add(col);
                        }
                        else
                        {
                            lv.SubItems[i].Tag = col;
                        }
                    }

                    this.listViewMapeamentos.Items.Add(lv);
                }

                dr.Close();

                this.ActiveControl = textBoxTextoPesquisa;

                this.Cursor = Cursors.Default;
            }
            catch (OracleException ex)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show(Environment.NewLine + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBoxTextoPesquisa_TextChanged(object sender, EventArgs e)
        {
            // Call FindItemWithText with the contents of the textbox.
            ListViewItem foundItem =
                listViewMapeamentos.FindItemWithText(textBoxTextoPesquisa.Text, false, 0, true);
            if (foundItem != null)
            {
                listViewMapeamentos.TopItem = foundItem;

            }
        }

        private void listViewMapeamentos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listViewMapeamentos_DoubleClick(object sender, EventArgs e)
        {
            FormTarefas pai = (FormTarefas)this.Owner;

            pai.textBoxIdMapeamento.Text = listViewMapeamentos.FocusedItem.SubItems[0].Text;

            this.Close();
        }

        private void listViewMapeamentos_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine whether the column is the same as the last column clicked.
            if (e.Column != sortColumn)
            {
                // Set the sort column to the new column.
                sortColumn = e.Column;
                // Set the sort order to ascending by default.
                listViewMapeamentos.Sorting = SortOrder.Ascending;
            }
            else
            {
                // Determine what the last sort order was and change it.
                if (listViewMapeamentos.Sorting == SortOrder.Ascending)
                    listViewMapeamentos.Sorting = SortOrder.Descending;
                else
                    listViewMapeamentos.Sorting = SortOrder.Ascending;
            }

            // Call the sort method to manually sort.
            listViewMapeamentos.Sort();

            // Set the ListViewItemSorter property to a new ListViewItemComparer
            // object.
            //this.listViewMapeamentos.ListViewItemSorter = new ListViewItemComparer(e.Column,
            //                                                  listViewMapeamentos.Sorting);

            this.listViewMapeamentos.ListViewItemSorter = new ListViewAutoSorter(e.Column,
                                                                                 listViewMapeamentos.Sorting);
        }

        private void FormSelecionarMapeamento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

        }

        private void listViewMapeamentos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                listViewMapeamentos_DoubleClick(null, null);
            }
        }
    }
}
