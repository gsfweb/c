﻿namespace MapWinApp
{
    partial class FormSelecionarMapeamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelecionarMapeamento));
            this.listViewMapeamentos = new System.Windows.Forms.ListView();
            this.labelTextoPesquisa = new System.Windows.Forms.Label();
            this.textBoxTextoPesquisa = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // listViewMapeamentos
            // 
            this.listViewMapeamentos.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listViewMapeamentos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listViewMapeamentos.FullRowSelect = true;
            this.listViewMapeamentos.GridLines = true;
            this.listViewMapeamentos.HideSelection = false;
            this.listViewMapeamentos.Location = new System.Drawing.Point(0, 55);
            this.listViewMapeamentos.MultiSelect = false;
            this.listViewMapeamentos.Name = "listViewMapeamentos";
            this.listViewMapeamentos.Size = new System.Drawing.Size(472, 207);
            this.listViewMapeamentos.TabIndex = 2;
            this.listViewMapeamentos.UseCompatibleStateImageBehavior = false;
            this.listViewMapeamentos.View = System.Windows.Forms.View.Details;
            this.listViewMapeamentos.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewMapeamentos_ColumnClick);
            this.listViewMapeamentos.SelectedIndexChanged += new System.EventHandler(this.listViewMapeamentos_SelectedIndexChanged);
            this.listViewMapeamentos.DoubleClick += new System.EventHandler(this.listViewMapeamentos_DoubleClick);
            this.listViewMapeamentos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listViewMapeamentos_KeyDown);
            // 
            // labelTextoPesquisa
            // 
            this.labelTextoPesquisa.AutoSize = true;
            this.labelTextoPesquisa.Location = new System.Drawing.Point(5, 8);
            this.labelTextoPesquisa.Name = "labelTextoPesquisa";
            this.labelTextoPesquisa.Size = new System.Drawing.Size(107, 13);
            this.labelTextoPesquisa.TabIndex = 0;
            this.labelTextoPesquisa.Text = "Texto para Pesquisa:";
            // 
            // textBoxTextoPesquisa
            // 
            this.textBoxTextoPesquisa.Location = new System.Drawing.Point(8, 24);
            this.textBoxTextoPesquisa.Name = "textBoxTextoPesquisa";
            this.textBoxTextoPesquisa.Size = new System.Drawing.Size(100, 20);
            this.textBoxTextoPesquisa.TabIndex = 1;
            this.textBoxTextoPesquisa.TextChanged += new System.EventHandler(this.textBoxTextoPesquisa_TextChanged);
            // 
            // FormSelecionarMapeamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(472, 262);
            this.Controls.Add(this.textBoxTextoPesquisa);
            this.Controls.Add(this.labelTextoPesquisa);
            this.Controls.Add(this.listViewMapeamentos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSelecionarMapeamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Selecionar Mapeamento";
            this.Load += new System.EventHandler(this.FormSelecionarMapeamento_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormSelecionarMapeamento_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewMapeamentos;
        private System.Windows.Forms.Label labelTextoPesquisa;
        public System.Windows.Forms.TextBox textBoxTextoPesquisa;
    }
}