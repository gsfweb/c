CREATE OR REPLACE PACKAGE NFE_GERA_XML_NEW is


procedure gera_xml_new ( p_id_docfis in number );

function gera_nfe ( p_id_docfis in number )  return clob;

function gera_ide ( p_id_docfis in number ) return XMLType;

function gera_emi_dest( p_id_cod_local in number
                      , p_cd_tarefa    in varchar2
                      , p_cod_estado   in varchar2 ) return XMLType;

function gera_retirada_entrega( p_id_cod_local in number
                              , p_cd_tarefa    in varchar2 ) return XMLType;

function gera_item ( p_id_docfis in number ) return XMLType;

function gera_imposto ( p_id_docfis      in number
                      , p_id_item_docfis in number ) return XMLType;

function gera_icms ( p_id_docfis      in number
                   , p_id_item_docfis in number ) return XMLType;

function gera_ipi ( p_id_docfis      in number 
                  , p_id_item_docfis in number ) return XMLType;

function gera_ii ( p_id_docfis      in number
                 , p_id_item_docfis in number ) return XMLType;

function gera_pis( p_id_docfis      in number
                 , p_id_item_docfis in number ) return XMLType;

function gera_cofins ( p_id_docfis      in number
                     , p_id_item_docfis in number ) return XMLType;

function gera_total ( p_id_docfis in number ) return XMLType;

function gera_transportadora ( p_id_cod_local in number
                             , p_ind_frete    in varchar2 ) return XMLType;

function gera_volume ( p_id_docfis in number ) return XMLType;

function gera_cobranca ( p_id_docfis in number ) return XMLType;

function gera_duplicata ( p_id_docfis in number ) return XMLType;

function gera_inf_adicionais ( p_id_docfis in number ) return XMLType; 

END NFE_GERA_XML_NEW;
/
CREATE OR REPLACE PACKAGE BODY NFE_GERA_XML_NEW is

--vari�veis globais

PROCEDURE gera_xml_new( p_id_docfis in number ) is

  v_id_docfis   number;
  v_id_controle number;
  v_xml         clob;

BEGIN

  v_id_docfis := p_id_docfis;
  v_xml       := gera_nfe( v_id_docfis );
  
    --gravo emiss�o
  nfe_controle_emissao_pkg.insert_controle( 'NFE'       --p_tp_nota      varchar2
                                          , v_id_docfis --p_cod_origem   varchar2
                                          , 'G'         --p_status       varchar2
                                          , ''          --p_descricao    varchar2
                                          , 'GSF'       --p_origem       varchar2
                                          , ''          --p_chave_nfe    varchar2
                                          , 'ENV'       --p_tipo_envio   varchar2
                                          , v_id_docfis --p_id_docfis    varchar2
                                          , v_id_controle);
  
  dbms_output.put_line(v_id_controle);
  -- gravo xml
  nfe_xml_emissao_pkg.insert_nfe_xml( v_id_controle
                                    , v_xml);
  

END gera_xml_new;

--GERA XML NFE
function gera_nfe ( p_id_docfis in number ) return clob is

  v_nfe       clob;
  v_id_docfis number;

begin
  
  v_id_docfis := p_id_docfis;
  
  SELECT XMLPI(NAME "xml ",'version="1.0" encoding="UTF-8"') ||
         XMLElement("nfeProc",
                   XMLAttributes('http://www.portalfiscal.inf.br/nfe' AS "xmlns", '3.10' AS "versao"),
                                XMLElement("NFe",
                                          XMLElement("infNFe", XMLattributes('3.10' AS "versao", 'NFe'||DOC.Chave_Nfe AS "Id"),
                                                    gera_ide( DOC.ID_DOCFIS )
                                                    --informa remetente
                                                  , gera_emi_dest ( DOC.ID_CLIFORN_REMETENTE, 'E', DOC.cod_estado_orig ) 
                                                    --informa destinatario
                                                  , gera_emi_dest ( DOC.ID_CLIFORN_DESTINATARIO, 'D', DOC.cod_estado_dest ) 
                                                  , case
                                                     --informa retirada
                                                     when DOC.Id_Cliforn_Retirada <> DOC.Id_Cliforn_Entrega then
                                                      gera_retirada_entrega( DOC.Id_Cliforn_Retirada, 'R')
                                                     --informa entrega
                                                     when DOC.Id_Cliforn_Retirada <> DOC.Id_Cliforn_Entrega then
                                                      gera_retirada_entrega( DOC.Id_Cliforn_Entrega, 'E')
                                                    end retira_entrega 
                                                    --informa descri��o dos itens
                                                  , gera_item( DOC.ID_DOCFIS )
                                                    --informa total
                                                  , gera_total( DOC.ID_DOCFIS )
                                                    --informa transportadora
                                                  , gera_transportadora( DOC.Id_Cliforn_Transp, Doc.ind_frete )
                                                    --informa volume
                                                  , gera_volume( DOC.ind_frete )
                                                    --informa cobran�a
                                                  , gera_cobranca( DOC.ID_DOCFIS )
                                                    --informa informa��es adicionais
                                                  , gera_inf_adicionais( DOC.ID_DOCFIS )
                                                    )
                                          )
                   ).getCLOBVal() RESULT INTO v_nfe
    FROM efd_capa_docfis DOC
   WHERE DOC.ID_DOCFIS = v_id_docfis;
     
  return v_nfe;          

END gera_nfe;

--GERA GRUPO IDE
function gera_ide ( p_id_docfis in number ) return XMLType is
  
  v_ide       XMLType;
  v_id_docfis number;
  
begin
  
  v_id_docfis := p_id_docfis;
  
  SELECT XMLElement("ide",
                   XMLForest(cad_estado.retorna_codigo_uf(cod_estado_orig) "cUF"
                           , lpad(id_docfis,8,0) "cNF" 
                           , nfe_util.retorna_desc_operacao(id_nop) "natOp"
                           , ind_pagto "indPag"
                           , nfe_util.retorna_modelo_doc(id_modelo) "mod"
                           , serie_docfis "serie"
                           , num_docfis "nNF"
                           , to_char(TO_TIMESTAMP_TZ (to_char(data_emissao 
                                                    ,'YYYY-MM-DD HH:MI:SS')
                                                    , 'YYYY-MM-DD HH:MI:SSTZH:TZM')
                                                    ,'YYYY-MM-DD"T"HH:MI:SSTZH:TZM') "dhEmi" 
                           , to_char(TO_TIMESTAMP_TZ (to_char(data_saida_rec 
                                                    ,'YYYY-MM-DD HH:MI:SS')
                                                    , 'YYYY-MM-DD HH:MI:SSTZH:TZM')
                                                    ,'YYYY-MM-DD"T"HH:MI:SSTZH:TZM') "dhSaiEnt"
                           , decode(movto_e_s,'E','0','1') "tpNF" 
                           , decode(movto_e_s,'S',decode(substr(id_cfop,1,1), '5',1,'6',2,3)) "idDest"
                           , '3509205' "cMunFG" 
                           , 1         "tpImp" 
                           , 1         "tpEmis" 
                           , 0         "cDV"
                           , 2         "tpAmb" 
                           , ind_oper  "finNFe" 
                           , 0         "indFinal" 
                           , 2         "indPres"
                           , 0         "procEmi" 
                           , 1.0       "verProc"
                           )
                   ) IDE
                   INTO v_ide
    FROM efd_capa_docfis doc
   WHERE id_docfis = v_id_docfis;
          
  return v_ide;  
  
end gera_ide;

--GERA EMINETE E DESTINAT�RIO
function gera_emi_dest( p_id_cod_local in number
                      , p_cd_tarefa    in varchar2
                      , p_cod_estado   in varchar2) return XMLType is

  v_emi_des      XMLType;
  v_id_cod_local number;
  v_cd_tarefa    varchar2(20);
  v_cod_estado   varchar2(20);
  
  begin
    
  v_id_cod_local := p_id_cod_local;
  v_cd_tarefa    := p_cd_tarefa;
  v_cod_estado   := p_cod_estado;
     
  SELECT 
         decode(v_cd_tarefa, 'E',
                            XMLElement("emit",
                                      XMLType('<'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'||nfe_util.remove_padrao_cpf_cnpj(cpf_cnpj)||'</'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'),
                                      XMLForest(razao_social "xNome"
                                               ,nome_fantasia "xFant"),
                                               XMLElement("enderEmit",
                                                         XMLForest(logradouro                                            "xLgr"
                                                                  ,numero                                                "nro"
                                                                  ,complemento                                           "xCpl"
                                                                  ,bairro                                                "xBairro"
                                                                  ,codigo_municipio                                      "cMun"
                                                                  ,cad_municipio.retorna_descricao_mun(codigo_municipio) "xMun"
                                                                  ,v_cod_estado                                          "UF"
                                                                  ,nfe_util.remove_padrao_cep(cep)                       "CEP"
                                                                  ,1058                                                  "cPais"
                                                                  ,pais                                                  "xPais"
                                                                  ,nfe_util.remove_padrao_fone(telefone)                 "fone")
                                                         ),
                                                         XMLForest(inscricao_estadual "IE"
                                                                  ,3 "CRT")        
                                                         
                                      )  
                           ,XMLElement("dest",
                                      XMLType('<'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'||nfe_util.remove_padrao_cpf_cnpj(cpf_cnpj)||'</'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'),
                                      XMLForest(razao_social "xNome"
                                               ,case
                                                 when pais != 'BRASIL' then
                                                  pais  
                                                end "idEstrangeiro"),
                                               XMLElement("enderDest",
                                                         XMLForest(logradouro                                            "xLgr"
                                                                  ,numero                                                "nro"
                                                                  ,complemento                                           "xCpl"
                                                                  ,bairro                                                "xBairro"
                                                                  ,codigo_municipio                                      "cMun"
                                                                  ,cad_municipio.retorna_descricao_mun(codigo_municipio) "xMun"
                                                                  ,v_cod_estado                                          "UF"
                                                                  ,nfe_util.remove_padrao_cep(cep)                       "CEP"
                                                                  ,1058                                                  "cPais"
                                                                  ,pais                                                  "xPais"
                                                                  ,nfe_util.remove_padrao_fone(telefone)                 "fone")
                                                         ),
                                                         decode(inscricao_estadual, 'ISENTO',
                                                               XMLElement("ISENTO",2),
                                                               XMLForest(1 "indIEDest"
                                                                        ,inscricao_estadual "IE") 
                                                               ),
                                                         XMLForest(inscricao_municipal "IM"
                                                                  ,e_mail "email")
                                      )
               ) INTO v_emi_des   
    FROM cad_cliente_fornecedor CCF
       , cad_cliente_fornecedor_locais CCL
   WHERE CCF.ID       = CCL.ID_CAD_CFE
     AND CCL.ID_LOCAL = v_id_cod_local;
    
  return v_emi_des;    

end gera_emi_dest;

--GERA RETIRADA OU ENTEGA
function gera_retirada_entrega( p_id_cod_local in number
                              , p_cd_tarefa    in varchar2) return XMLType is

  v_ret_entrega  XMLType;
  v_id_cod_local number;
  v_cd_tarefa    varchar2(20);
  
  begin
    
  v_id_cod_local := p_id_cod_local;
  v_cd_tarefa    := p_cd_tarefa;
    
  SELECT 
         decode(v_cd_tarefa, 'R',
               XMLElement("retirada",
                         XMLType('<'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'||nfe_util.remove_padrao_cpf_cnpj(cpf_cnpj)||'</'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'),
                         XMLForest(logradouro                                            "xLgr"
                                  ,numero                                                "nro"
                                  ,complemento                                           "xCpl"
                                  ,bairro                                                "xBairro"
                                  ,codigo_municipio                                      "cMun"
                                  ,cad_municipio.retorna_descricao_mun(codigo_municipio) "xMun"
                                  ,unidade_federativa                                    "UF"
                                  --,'SP' UF
                                  ) 
                                    
                          ),
               XMLElement("entrega",
                         XMLType('<'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'||nfe_util.remove_padrao_cpf_cnpj(cpf_cnpj)||'</'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'),
                         XMLForest(logradouro                                            "xLgr"
                                  ,numero                                                "nro"
                                  ,complemento                                           "xCpl"
                                  ,bairro                                                "xBairro"
                                  ,codigo_municipio                                      "cMun"
                                  ,cad_municipio.retorna_descricao_mun(codigo_municipio) "xMun"
                                  ,unidade_federativa                                    "UF"
                                  --,'SP' UF
                                  ) 
                                    
                         )           
               ) INTO v_ret_entrega 
    FROM cad_cliente_fornecedor CCF
       , cad_cliente_fornecedor_locais CCL
   WHERE CCF.ID       = CCL.ID_CAD_CFE
     AND CCL.ID_LOCAL = v_id_cod_local;
    
  return v_ret_entrega;  

end gera_retirada_entrega;

--GERA ITENS 
function gera_item( p_id_docfis in number) return XMLType is
  
  v_itens     XMLType;
  v_id_docfis number;
  
  begin
    
  v_id_docfis := p_id_docfis;
    
  SELECT XMLAgg( 
               XMLElement("det",
                          XMLattributes(IDO.NUM_ITEM AS "nItem"), 
                                       XMLElement("prod", XMLForest( nfe_util.retorna_cod_item(id_prod_serv)   "cProd"
                                                                   , ''                                        "cEAN"
                                                                   , nfe_util.retorna_desc_item(id_prod_serv)  "xProd"
                                                                   , IDO.NCM                                   "NCM"
                                                                   , nfe_util.retorna_cod_cfop(id_cfop)        "CFOP"
                                                                   , unid                                      "uCom"
                                                                   , nfe_util.formata_4_casas(qtd)             "qCom" 
                                                                   , nfe_util.formata_4_casas(vl_item)         "vUnCom"
                                                                   , nfe_util.formata_2_casas((vl_item * qtd)) "vProd"
                                                                   , ''                                        "cEANTrib"
                                                                   , unid                                      "uTrib"
                                                                   , nfe_util.formata_4_casas(qtd)             "qTrib"
                                                                   , nfe_util.formata_2_casas(vl_item)         "vUnTrib"
                                                                   , case 
                                                                      when IDO.vl_frete > 0 then
                                                                       nfe_util.formata_2_casas(IDO.vl_frete)
                                                                     end                                       "vFrete"
                                                                   , case 
                                                                      when IDO.vl_seg > 0 then
                                                                       nfe_util.formata_2_casas(IDO.vl_seg)
                                                                     end                                       "vSeg"   
                                                                   , case 
                                                                      when IDO.vl_Desc > 0 then
                                                                       nfe_util.formata_2_casas(IDO.vl_Desc)
                                                                     end                                       "vDesc"
                                                                   , case 
                                                                      when IDO.vl_out_da > 0 then
                                                                       nfe_util.formata_2_casas(IDO.vl_out_da)
                                                                     end                                       "vDesc"     
                                                                   , 1 "indTot")),
                                       --chama rotina gera impostos - item a item                            
                                       gera_imposto( v_id_docfis
                                                   , IDO.ID_ITEM_DOCFIS  )
                          )
               ) ITEM 
               INTO v_itens         
    FROM (SELECT * FROM efd_item_docfis ORDER BY NUM_ITEM) IDO
   WHERE ido.id_docfis = v_id_docfis;

  return v_itens;     
  
end gera_item; 

--GERA IMPOSTOS
function gera_imposto( p_id_docfis      in number
                     , p_id_item_docfis in number ) return XMLType is
  
  v_imposto        XMLType;
  v_id_docfis      number;
  v_id_item_docfis number;
  
  begin
    
  v_id_docfis      := p_id_docfis;
  v_id_item_docfis := p_id_item_docfis;
    
  SELECT XMLElement("imposto",
                   --RETORNA ICMS
                   gera_icms( v_id_docfis
                            , v_id_item_docfis ),
                   --RETORNA IPI
                   gera_ipi( v_id_docfis
                           , v_id_item_docfis ),
                   --RETORNA II 
                   gera_ii( v_id_docfis
                          , v_id_item_docfis ),
                   --RETORNA PIS
                   gera_pis( v_id_docfis
                           , v_id_item_docfis ),
                   --RETORNA COFINS
                   gera_cofins( v_id_docfis
                              , v_id_item_docfis )
  
         ) IMPOSTO
         INTO v_imposto 
    FROM DUAL;    
      
  return v_imposto;
  
end gera_imposto;

--GERA IMPOSTO ICMS
function gera_icms ( p_id_docfis      in number
                   , p_id_item_docfis in number ) return XMLType is
  
  v_icms           XMLType;
  v_id_docfis      number;
  v_id_item_docfis number;
  
  begin
    
  v_id_docfis      := p_id_docfis;
  v_id_item_docfis := p_id_item_docfis;
    
  SELECT XMLElement("ICMS",
                     XMLtype('<ICMS'||IDO.cst_icms||'>'||
                             XMLForest( IDO.ind_mercadoria "orig"
                                      , IDO.cst_icms "CST"
                                      ,case 
                                        when IDO.cst_icms  not in ('40','41','50')  then
                                         0  
                                       end "modBC"
                                      ,case 
                                        when IDO.cst_icms = '20'  then
                                         nfe_util.formata_2_casas( round(((IDO.vl_total_item - IDO.vl_bc_icms) / IDO.vl_total_item) * 100,2)) 
                                       end "pRedBC" 
                                      , nfe_util.formata_2_casas(IDO.vl_bc_icms) "vBC"
                                      , nfe_util.formata_4_casas(IDO.aliq_icms)  "pICMS"
                                      , nfe_util.formata_2_casas(IDO.vl_icms)    "vICMS")                                     
                     ||'</ICMS'||IDO.cst_icms||'>')
                   ) ISCM INTO v_icms  
                     FROM efd_item_docfis IDO, (select 'ICMS'|| aux.cst_icms val from efd_item_docfis aux where aux.id_item_docfis = v_id_item_docfis )aux
                    WHERE IDO.ID_DOCFIS      = v_id_docfis
                      AND IDO.ID_ITEM_DOCFIS = v_id_item_docfis;
                      
                   
  return v_icms;
  
end gera_icms;

--GERA IMPOSTO IPI
function gera_ipi ( p_id_docfis      in number
                  , p_id_item_docfis in number ) return XMLType is
  
  v_ipi            XMLType;
  v_id_docfis      number;
  v_id_item_docfis number;
  
  begin
    
  v_id_docfis      := p_id_docfis;
  v_id_item_docfis := p_id_item_docfis;
    
  SELECT XMLElement("IPI",
                   XMLForest(999 "cEnq"
                             ,case
                               when IDO.cst_ipi in ('00','49','50','99') then       
                                          XMLForest( IDO.cst_ipi "CST"
                                                   , nfe_util.formata_2_casas(IDO.vl_bc_ipi) "vBC"
                                                   , nfe_util.formata_4_casas(IDO.aliq_ipi)  "pIPI"
                                                   , nfe_util.formata_2_casas(IDO.vl_ipi)    "vIPI"
                                                   )
                              end "IPITrib"
                             ,case
                               when IDO.cst_ipi in ('01','02','03','04','51','52','53','54','55') then
                                XMLForest(IDO.cst_ipi "CST") 
                              end "IPINT" 
                            )
                   )IPI INTO v_ipi
    FROM efd_item_docfis IDO
   WHERE IDO.ID_DOCFIS      = v_id_docfis
     AND IDO.ID_ITEM_DOCFIS = v_id_item_docfis;
     
  return v_ipi;                    
  
end gera_ipi;

--GERA IMPOSTO II
function gera_ii ( p_id_docfis      in number
                 , p_id_item_docfis in number ) return XMLType is
  
  v_ii             XMLType;
  v_id_docfis      number;
  v_id_item_docfis number;
  
  begin
    
  v_id_docfis      := p_id_docfis;
  v_id_item_docfis := p_id_item_docfis;
    
  SELECT case
          when IDO.vl_ii > 0 then 
           XMLElement("II",
                     XMLForest(nfe_util.formata_2_casas(IDO.vl_bc_ii)          "vBC"
                             , nfe_util.formata_2_casas(IDO.vl_desp_aduaneira) "vDespAdu"
                             , nfe_util.formata_2_casas(IDO.vl_ii)             "vII"
                             , nfe_util.formata_2_casas(IDO.vl_iof)            "vIOF")
                     ) 
         end II INTO v_ii   
        FROM efd_item_docfis IDO
       WHERE IDO.ID_DOCFIS      = v_id_docfis
         AND IDO.ID_ITEM_DOCFIS = v_id_item_docfis;  
  
  return v_ii;

end gera_ii;

--GERA IMPOSTO PIS
function gera_pis(  p_id_docfis      in number
                  , p_id_item_docfis in number ) return XMLtype is
  
  v_pis            XMLtype;
  v_id_docfis      number;
  v_id_item_docfis number;
  
  begin
    
  v_id_docfis      := p_id_docfis;
  v_id_item_docfis := p_id_item_docfis;
    
  SELECT XMLElement("PIS",
                    case
                      when IDO.Cst_Pis in ('01','02') then
                        XMLElement("PISAliq",
                                  XMLForest(cst_pis                             "CST"
                                          , nfe_util.formata_2_casas(vl_bc_pis) "vBC"
                                          , nfe_util.formata_4_casas(aliq_pis)  "pPIS"
                                          , nfe_util.formata_2_casas(vl_pis)    "vPIS")
                                  )
                    end PISAliq
                   ,case
                     when IDO.Cst_Pis = '03' then
                      XMLElement("PISQtde",
                                XMLForest(cst_pis                           "CST"
                                        , nfe_util.formata_4_casas(qtd)     "qBCProd"
                                        , nfe_util.formata_4_casas(vl_item) "vAliqProd"
                                        , nfe_util.formata_2_casas(vl_pis)  "vPIS")
                                )
                    end PISQtde
                   ,case 
                     when IDO.Cst_Pis in ('04','05','06','07','08','09') then
                      XMLElement("PISNT",   
                                XMLForest(cst_pis CST)
                                )
                    end PISNT
                   ,case 
                     when IDO.Cst_Pis in ('49','50','51','52','53','54','55','56','60',
                                         '61','62','63','64','65','66','67','70','71',
                                         '72','73','74','75','98','99') then
                      XMLElement("PISOutr",null)
                    end PISOutr
                   ,case 
                     when IDO.Cst_Pis in ('ST') then
                      XMLElement("PISST",null)
                    end PISST  
                   ) PIS INTO v_pis
    FROM efd_item_docfis IDO
   where IDO.ID_DOCFIS      = v_id_docfis
     AND IDO.ID_ITEM_DOCFIS = v_id_item_docfis;
  
  return v_pis;                      
  
end gera_pis;

--GERA IMPOSTO COFINS
function gera_cofins (  p_id_docfis      in number
                      , p_id_item_docfis in number ) return XMLType is

  v_cofins         XMLType;
  v_id_docfis      number;
  v_id_item_docfis number;
  
  begin
    
  v_id_docfis      := p_id_docfis;
  v_id_item_docfis := p_id_item_docfis;
    
  SELECT XMLElement("COFINS",
                   case
                    when IDO.cst_cofins in ('01','02') then
                     XMLElement("COFINSAliq",
                               XMLForest(IDO.cst_cofins                             "CST"
                                        ,nfe_util.formata_2_casas(IDO.vl_bc_cofins) "vBC"
                                        ,nfe_util.formata_4_casas(IDO.aliq_cofins)  "pCOFINS"
                                        ,nfe_util.formata_2_casas(IDO.vl_cofins)    "vCOFINS")         
                               )
                   end COFINS
                  ,case 
                    when IDO.cst_cofins = '03' then
                     XMLElement("COFINSQtde",
                               XMLForest(IDO.cst_cofins                          "CST"
                                        ,nfe_util.formata_4_casas(IDO.qtd)       "qBCProd"
                                        ,nfe_util.formata_4_casas(IDO.vl_item)   "vAliqProd"
                                        ,nfe_util.formata_2_casas(IDO.vl_cofins) "vCOFINS")
                               )
                   end COFINSQtde
                  ,case 
                    when IDO.cst_cofins in ('04','05','06','07','08','09') then
                     XMLElement("COFINSNT",
                               XMLElement("CST", IDO.cst_cofins)          
                               )
                   end COFINSNT
                  ,case
                    when IDO.cst_cofins in ('49','50','51','52','53','54','55','56','60',
                           '61','62','63','64','65','66','67','70','71',
                           '72','73','74','75','98','99') then
                     XMLElement("COFINSOutr",null)
                   end COFINSOutr
                  ,case
                    when IDO.cst_cofins = 'ST' then
                     XMLElement("COFINSST",null)    
                   end COFINSST                
                   ) INTO v_cofins
    FROM efd_item_docfis IDO
   WHERE IDO.ID_DOCFIS      = v_id_docfis
     AND IDO.ID_ITEM_DOCFIS = v_id_item_docfis; 
     
  return v_cofins;                   

end gera_cofins;

--GERA TOTAL  
function gera_total ( p_id_docfis in number ) return XMLType is
  
  v_total     XMLType;
  v_id_docfis number;
  
  begin
    
  v_id_docfis := p_id_docfis;
    
  SELECT XMLElement("total",
                   XMLElement("ICMSTot",
                             XMLForest(nfe_util.formata_2_casas(vl_bc_icms)    "vBC"
                                     , nfe_util.formata_2_casas(vl_icms)       "vICMS"
                                     , nfe_util.formata_2_casas(0)             "vICMSDeson"
                                     , nfe_util.formata_2_casas(vl_bc_icms_st) "vBCST"
                                     , nfe_util.formata_2_casas(vl_icms_st)    "vST"
                                     , nfe_util.formata_2_casas(vl_merc)       "vProd"
                                     , nfe_util.formata_2_casas(vl_frete)      "vFrete"
                                     , nfe_util.formata_2_casas(vl_seg)        "vSeg"
                                     , nfe_util.formata_2_casas(vl_desc)       "vDesc"
                                     , nfe_util.formata_2_casas(vl_ii)         "vII"
                                     , nfe_util.formata_2_casas(vl_ipi)        "vIPI"
                                     , nfe_util.formata_2_casas(vl_pis)        "vPIS"
                                     , nfe_util.formata_2_casas(vl_cofins)     "vCOFINS"
                                     , nfe_util.formata_2_casas(vl_out_da)     "vOutro"
                                     , nfe_util.formata_2_casas(vl_tot_nf)     "vNF")
                             )
                   ) INTO v_total 
    FROM efd_capa_docfis doc
   WHERE doc.id_docfis = v_id_docfis;
   
  return v_total;
     
end gera_total;

--GERA TRANSPORTE 
function gera_transportadora( p_id_cod_local in number
                            , p_ind_frete    in varchar2  ) return XMLType is

  v_transportadora XMLType;
  v_id_cod_local   number;
  v_ind_frete      varchar2(40);
  
  begin
    
  v_id_cod_local := p_id_cod_local;
  v_ind_frete    := p_ind_frete;
    
  SELECT XMLElement("transp",
                   XMLElement("modFrete", v_ind_frete),
                             XMLElement("transporta",
                                       XMLType('<'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'||nfe_util.remove_padrao_cpf_cnpj(cpf_cnpj)||'</'||nfe_util.retorna_desc_cpf_cnpj(cpf_cnpj)||'>'),
                                       XMLForest(razao_social                              "xNome"
                                               , inscricao_estadual                        "IE"
                                               , logradouro                                "xEnder"
                                               , codigo_municipio                          "xMun"
                                               , cad_estado.retorna_uf(unidade_federativa) "UF"
                                                )
                                       )
                   ) INTO v_transportadora
    FROM cad_cliente_fornecedor CCF
       , cad_cliente_fornecedor_locais CCL
   WHERE CCF.ID 	    = CCL.ID_CAD_CFE
     AND CCL.ID_LOCAL = v_id_cod_local;
     
  return v_transportadora;      

end gera_transportadora;

--GERA VOLUME
function gera_volume( p_id_docfis in number ) return XMLType is
  
  v_volume    XMLType;
  v_id_docfis number;
  
  begin
    
  v_id_docfis := p_id_docfis;
  
  SELECT XMLElement("vol",
                   XMLForest(qtd_volume "qVol"
                            ,marca      "marca"
                            ,nr_volume  "nVol"
                            ,peso_liq   "pesoL"
                            ,peso_bruto "pesoB"    
                            )   
                   ) INTO v_volume  
    FROM EFD_VOLUME_DOCFIS VDO
   WHERE VDO.ID_DOCFIS = v_id_docfis;
      
  return v_volume;
  
end gera_volume;

--GERA COBRAN�A
function gera_cobranca( p_id_docfis in number ) return XMLType is
  
  v_cobranca  XMLType;
  v_id_docfis number;
  
  begin
    
  v_id_docfis := p_id_docfis;
    
  SELECT XMLElement("cobr",
                   XMLElement("fat",
                             XMLForest(num_fatura                                "nFat"
                                      ,nfe_util.formata_2_casas(sum(vl_parcela)) "vOrig"   
                                      ,case
                                        when sum(vl_desconto) > 0 then
                                         nfe_util.formata_2_casas(sum(vl_desconto))
                                       end                                       "vDesc"
                                      ,nfe_util.formata_2_casas(sum(vl_liquido)) "vLiq"
                                      )
                             ),
                             --chama duplicata
                             gera_duplicata( v_id_docfis )
                   ) INTO v_cobranca
    FROM EFD_PARCELA_DOCFIS PDO 
   WHERE PDO.ID_DOCFIS = v_id_docfis
   GROUP BY NUM_FATURA;
 
  return v_cobranca;   
  
end gera_cobranca;

--GERA DUPLICATA
function gera_duplicata( p_id_docfis in number ) return XMLType is

  v_duplicata XMLType;
  v_id_docfis number;
  
  begin
    
  v_id_docfis := p_id_docfis;
  
  SELECT XMLAgg(XMLElement("dup",
                   XMLForest(num_parcela                           "nDup"
                            ,to_char(data_vencimento,'yyyy-mm-dd') "dVenc"
                            ,nfe_util.formata_2_casas(vl_parcela)  "vDup"
                            )
                          )
               ) INTO v_duplicata
    FROM efd_parcela_docfis 
   WHERE id_docfis = v_id_docfis;

  return v_duplicata;     

end gera_duplicata;

--GERA INF. ADICIONAIS
function gera_inf_adicionais ( p_id_docfis in number ) return XMLType is

  v_inf_adicionais XMlType;
  v_id_docfis      number;
  
  begin
    
  v_id_docfis := p_id_docfis;
    
  SELECT XMLElement("infAdic", 
                   XMLForest(inf_adicional    "infAdFisco"
                            ,inf_complementar "infCpl"
                            )
                   ) INTO v_inf_adicionais 
    FROM EFD_INFO_DOCFIS IDO
   WHERE id_docfis = v_id_docfis;  
   
  return v_inf_adicionais;    

end gera_inf_adicionais; 


END NFE_GERA_XML_NEW;
/
