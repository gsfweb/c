create or replace view gsf_tipo_log as
select COD_TIPO
     , DESC_TIPO
     , VERSAO_LINHA
     , ID_OBJETO
 from GSF_TIPO_LOG_TAB
WITH READ ONLY;
