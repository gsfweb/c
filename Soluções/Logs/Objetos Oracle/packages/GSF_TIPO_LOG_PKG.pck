CREATE OR REPLACE PACKAGE GSF_TIPO_LOG_PKG is

       PROCEDURE Inserir( p_cod_tipo  gsf_tipo_log_tab.cod_tipo%TYPE
                        , p_desc_tipo gsf_tipo_log_tab.desc_tipo%TYPE );

       PROCEDURE Alterar( p_id_objeto gsf_tipo_log_tab.id_objeto%TYPE
                        , p_desc_tipo gsf_tipo_log_tab.desc_tipo%TYPE );

       PROCEDURE Remover( p_id_objeto gsf_tipo_log_tab.id_objeto%TYPE );
       
       FUNCTION Obtem_Descricao( p_cod_tipo gsf_tipo_log_tab.cod_tipo%TYPE ) return varchar2; 

END GSF_TIPO_LOG_PKG;
/
CREATE OR REPLACE PACKAGE BODY GSF_TIPO_LOG_PKG is

--variaveis globais
v_sql_dynamic VARCHAR2(2000);

--gera novo tipo de log
PROCEDURE Inserir( p_cod_tipo  gsf_tipo_log_tab.cod_tipo%TYPE
                 , p_desc_tipo gsf_tipo_log_tab.desc_tipo%TYPE ) is

       --variáveis locais
       v_cod_tipo  gsf_tipo_log_tab.cod_tipo%TYPE;
       v_desc_tipo gsf_tipo_log_tab.desc_tipo%TYPE;
       v_id_objeto gsf_tipo_log_tab.id_objeto%TYPE;

       BEGIN
         
         v_cod_tipo  := p_cod_tipo;
         v_desc_tipo := p_desc_tipo;
         
         --INSERE REGISTRO 
         v_sql_dynamic := 'insert into GSF_TIPO_LOG_TAB(cod_tipo
                                                     , desc_tipo
                                                     , versao_linha ) 
                                                values(
                                                       :cod_tipo
                                                     , :desc_tipo
                                                     , sysdate)';
                                       
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_tipo
             , v_desc_tipo;
         
         --PEGA ROWID 
         SELECT ROWID 
           INTO v_id_objeto  
           FROM GSF_TIPO_LOG_TAB
          WHERE COD_TIPO = v_cod_tipo;
         
         --INSERE ROWID
         v_sql_dynamic := 'update GSF_TIPO_LOG_TAB 
                              set ID_OBJETO = :id_objeto
                            where COD_TIPO  = :cod_tipo'; 
         EXECUTE IMMEDIATE v_sql_dynamic                  
         USING v_id_objeto
             , v_cod_tipo;
         
         COMMIT;    


       EXCEPTION
         WHEN others THEN
           ROLLBACK; 
           GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TIPO_LOG_PKG.INSERIR');
           dbms_output.put_line(SQLERRM); 


END Inserir;

--ATUALIZA TIPO LOG TAB
PROCEDURE Alterar( p_id_objeto gsf_tipo_log_tab.id_objeto%TYPE 
                 , p_desc_tipo gsf_tipo_log_tab.desc_tipo%TYPE ) is
  
       --variaveis locais   
       v_id_objeto gsf_tipo_log_tab.cod_tipo%TYPE;
       v_desc_tipo gsf_tipo_log_tab.desc_tipo%TYPE;
       
       BEGIN
         
         v_id_objeto := p_id_objeto;
         v_desc_tipo := p_desc_tipo;
         
         --ATUALIZA DESC DE ERRO
         v_sql_dynamic := 'update GSF_TIPO_LOG_TAB
                              set DESC_TIPO    = :v_desc_tipo
                                , VERSAO_LINHA = sysdate
                            where ID_OBJETO    = :id_objeto';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_desc_tipo
             , v_id_objeto;
         
         COMMIT;    
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TIPO_LOG_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);   
  

END Alterar;

--REMOVE TIPO_LOG_TAB
PROCEDURE Remover( p_id_objeto gsf_tipo_log_tab.id_objeto%TYPE ) is

       --variaveis locais
       v_id_objeto gsf_tipo_log_tab.id_objeto%TYPE;
       
       BEGIN
         
         v_id_objeto := p_id_objeto; 
         
         v_sql_dynamic := 'delete 
                             from GSF_TIPO_LOG_TAB
                            where ID_OBJETO = :id_objeto'; 
       
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto;
       
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TIPO_LOG_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);   
  
END Remover;

function Obtem_descricao( p_cod_tipo gsf_tipo_log_tab.cod_tipo%TYPE ) return varchar2 is
         
         v_descricao gsf_tipo_log_tab.desc_tipo%TYPE;
        
         begin
           
           select desc_tipo 
             INTO v_descricao 
             from gsf_tipo_log_tab
            where cod_tipo = p_cod_tipo;
            
          return v_descricao;  

end Obtem_descricao;

END GSF_TIPO_LOG_PKG;
/
