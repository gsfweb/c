CREATE OR REPLACE PACKAGE GSF_LOG_PKG IS

       PROCEDURE Inserir( p_cod_tipo  gsf_log_tab.cod_tipo%TYPE
                        , p_texto_log gsf_log_tab.texto_log%TYPE
                        , P_ds_metodo gsf_log_tab.ds_metodo%TYPE );
       
       PROCEDURE Alterar( p_id_log    gsf_log_tab.id_log%TYPE
                        , p_cod_tipo  gsf_log_tab.cod_tipo%TYPE
                        , p_texto_log gsf_log_tab.texto_log%TYPE
                        , p_ds_metodo gsf_log_tab.ds_metodo%TYPE );
                                    
       PROCEDURE Remover( p_id_log gsf_log_tab.id_log%TYPE );         
       
       PROCEDURE Excluir_lote( p_data_inicio gsf_log_tab.versao_linha%TYPE
                             , p_data_final  gsf_log_tab.versao_linha%TYPE );                    

END GSF_LOG_PKG;
/
CREATE OR REPLACE PACKAGE BODY GSF_LOG_PKG IS

--variaveis globais
v_sql_dynamic VARCHAR2(2000);


--gera novo log
PROCEDURE Inserir( p_cod_tipo  gsf_log_tab.cod_tipo%TYPE
                 , p_texto_log gsf_log_tab.texto_log%TYPE
                 , P_ds_metodo gsf_log_tab.ds_metodo%TYPE ) is
                            
       --variaveis locais
       v_id_log    gsf_log_tab.id_log%TYPE;
       v_cod_tipo  gsf_log_tab.cod_tipo%TYPE;
       v_texto_log gsf_log_tab.texto_log%TYPE;
       v_id_objeto gsf_log_tab.id_objeto%TYPE;
       v_ds_metodo gsf_log_tab.ds_metodo%TYPE;
 
       BEGIN
         
         v_cod_tipo  := p_cod_tipo;
         v_texto_log := p_texto_log;
         v_ds_metodo := p_ds_metodo;
         
         --gera sequence
         SELECT GSF_LOG_SEQ.NEXTVAL INTO v_id_log FROM dual;
         
         --insere registro log
         v_sql_dynamic := 'insert into GSF_LOG_TAB( ID_LOG
                                                 , COD_TIPO
                                                 , TEXTO_LOG
                                                 , VERSAO_LINHA
                                                 , DS_METODO )
                                          values ( :id_log
                                                 , :cod_tipo
                                                 , :texto_log
                                                 , sysdate
                                                 , :ds_metodo )';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_log
             , v_cod_tipo
             , v_texto_log
             , v_ds_metodo;
         
         --pega ROWID
         SELECT ROWID
           INTO v_id_objeto
           FROM GSF_LOG_TAB
          WHERE ID_LOG = v_id_log;   
         
         --insere ROWID
         v_sql_dynamic := 'update GSF_LOG_TAB
                              set ID_OBJETO = :id_objeto
                            where ID_LOG    = :id_log';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto
             , v_id_log;
              
         COMMIT;    
         
         EXCEPTION
           WHEN others THEN
             dbms_output.put_line(SQLERRM);
             ROLLBACK;   
          
       
END Inserir;

--atualiza log
PROCEDURE Alterar( p_id_log    gsf_log_tab.id_log%TYPE
                 , p_cod_tipo  gsf_log_tab.cod_tipo%TYPE
                 , p_texto_log gsf_log_tab.texto_log%TYPE
                 , p_ds_metodo gsf_log_tab.ds_metodo%TYPE ) is
       
       --variaveis locais  
       v_id_log    gsf_log_tab.id_log%TYPE;                     
       v_cod_tipo  gsf_log_tab.cod_tipo%TYPE;
       v_texto_log gsf_log_tab.texto_log%TYPE;
       v_ds_metodo gsf_log_tab.ds_metodo%TYPE;
       
       BEGIN
         
         v_id_log    := p_id_log;
         v_cod_tipo  := p_cod_tipo;
         v_texto_log := p_texto_log;
         v_ds_metodo := p_ds_metodo;
         
         v_sql_dynamic := 'update GSF_LOG_TAB
                              set COD_TIPO     = :cod_tipo
                                , TEXTO_LOG    = :texto_log
                                , VERSAO_LINHA = sysdate
                                , DS_METODO    = :ds_metodo
                            where ID_LOG       = :id_log';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_tipo
             , v_texto_log
             , v_id_log
             , v_ds_metodo;
               
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             dbms_output.put_line(SQLERRM);
             ROLLBACK;
             	


END Alterar;   

--remove registro log
PROCEDURE Remover( p_id_log gsf_log_tab.id_log%TYPE ) is  

       --variaveis locais
       v_id_log gsf_log_tab.id_log%TYPE;
       
       BEGIN
         
         v_id_log := p_id_log;
         
         v_sql_dynamic := 'delete 
                             from GSF_LOG_TAB
                            where ID_LOG = :id_log';
       
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_log;
       
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             dbms_output.put_line(SQLERRM);
             ROLLBACK;                     

END Remover;

--execlus�o por lote - per�odo dd/mm/yyyy hh24:mi:ss ( inicio e final )
PROCEDURE Excluir_lote( p_data_inicio gsf_log_tab.versao_linha%TYPE
                      , p_data_final  gsf_log_tab.versao_linha%TYPE ) is
                       
       --variaveis locais                
       v_data_inicio gsf_log_tab.versao_linha%TYPE;
       v_data_final  gsf_log_tab.versao_linha%TYPE;
       
       --declara��o de cursores/types/tables
       CURSOR c_periodo_exclusao IS SELECT ID_LOG
                                      FROM GSF_LOG_TAB
                                     WHERE versao_linha BETWEEN v_data_inicio 
                                       AND v_data_final;
       
       TYPE t_periodo_exclusao IS TABLE OF c_periodo_exclusao%ROWTYPE 
                               INDEX BY PLS_INTEGER;
            
       l_periodo_exclusao t_periodo_exclusao;
               
       BEGIN       
         
         --range dd/mm/yyyy hh24:mi:ss 
         v_data_inicio := p_data_inicio;
         v_data_final  := p_data_final;      
         
         OPEN c_periodo_exclusao;

         FETCH c_periodo_exclusao

         BULK COLLECT INTO l_periodo_exclusao;

         CLOSE c_periodo_exclusao;
         
         FOR indx IN l_periodo_exclusao.first .. l_periodo_exclusao.last LOOP
           
           v_sql_dynamic := 'delete 
                               from GSF_LOG_TAB
                              where ID_LOG = :id_log';
           
           EXECUTE IMMEDIATE v_sql_dynamic
           USING l_periodo_exclusao(indx).ID_LOG;
           
         END LOOP;
         
         --exclus�o total 
         COMMIT;
         
         --cancela exclus�o
         EXCEPTION
           WHEN others THEN
             dbms_output.put_line(SQLERRM);
             ROLLBACK;   
                            
                       
END Excluir_lote;                       

END GSF_LOG_PKG;
/
