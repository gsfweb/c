create or replace view efd_cad_livro_fiscal as
select id
     , modelo
     , id_empresa
     , firma
     , cnpj_cpf
     , insc_estadual
     , mes_ano_fiscal
     , data_geracao
     , xml
 from efd_cad_livro_fiscal_tab
 WITH READ ONLY;
