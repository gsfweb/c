CREATE OR REPLACE VIEW GSF_REGRA_LIV_FISCAL_P1 AS
SELECT id_regra
     , id_empresa
     , tipo
     , uf, cfop
     , cod_valor_fiscal
     , cst
     , base_calculo
     , aliquota
     , imposto
  FROM GSF_REGRA_LIV_FISCAL_P1_TAB
  WITH READ ONLY;
