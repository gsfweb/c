CREATE OR REPLACE VIEW CAD_MOD_DOC_LIVROS_FIS AS
SELECT modelo,
       exige_cod_valor_fiscal,
       descricao
  FROM CAD_MOD_DOC_LIVROS_FIS_TAB
  WITH READ ONLY;
