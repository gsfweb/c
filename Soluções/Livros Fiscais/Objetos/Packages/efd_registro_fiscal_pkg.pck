create or replace package efd_registro_fiscal_pkg is

PROCEDURE INSERIR(reg_fis efd_registro_fiscal_tab%ROWTYPE);

PROCEDURE ALTERAR(p_id_nota    efd_registro_fiscal_tab.id_nota_fiscal%TYPE
                , p_id_empresa efd_registro_fiscal_tab.id_empresa%TYPE 
                , reg_fis      efd_registro_fiscal_tab%ROWTYPE
                , p_count      OUT number);

end efd_registro_fiscal_pkg;
/
create or replace package body efd_registro_fiscal_pkg is

PROCEDURE INSERIR(reg_fis efd_registro_fiscal_tab%ROWTYPE) IS
  
BEGIN
       INSERT INTO efd_registro_fiscal_tab
              (id_reg_fiscal,
               id_nota_fiscal,
               no_nota_fiscal,
               serie,
               sub_serie,
               uf_orig,
               endereco_dest,
               cidade_dest,
               pais_dest,
               bairro_dest,
               uf_dest,
               cep_dest,
               cnpj_cpf_dest,
               data_entrada,
               data_emissao,
               tipo_entrada_saida,
               especie_nf,
               no_documento,
               value_frete,
               valor_seguro,
               valor_despesas_acessorias,
               status_nota_fiscal,
               valor_mercadoria,
               valor_ipi,
               valor_desconto,
               valor_substituicao_tributaria,
               inscricao_substituicao_trib,
               quantidade_volume,
               especie_volume,
               peso_bruto,
               peso_liquido,
               placa_veiculo,
               observação,
               tipo_nota_fiscal,
               inscricao_suframa,
               uf_veiculo,
               cancelado,
               id_empresa
               )
      VALUES
             (reg_fis.id_reg_fiscal,
              reg_fis.id_nota_fiscal,
              reg_fis.no_nota_fiscal,
              reg_fis.serie,
              reg_fis.sub_serie,
              reg_fis.uf_orig,
              reg_fis.endereco_dest,
              reg_fis.cidade_dest,
              reg_fis.pais_dest,
              reg_fis.bairro_dest,
              reg_fis.uf_dest,
              reg_fis.cep_dest,
              reg_fis.cnpj_cpf_dest,
              reg_fis.data_entrada,
              reg_fis.data_emissao,
              reg_fis.tipo_entrada_saida,
              reg_fis.especie_nf,
              reg_fis.no_documento,
              reg_fis.value_frete,
              reg_fis.valor_seguro,
              reg_fis.valor_despesas_acessorias,
              reg_fis.status_nota_fiscal,
              reg_fis.valor_mercadoria,
              reg_fis.valor_ipi,
              reg_fis.valor_desconto,
              reg_fis.valor_substituicao_tributaria,
              reg_fis.inscricao_substituicao_trib,
              reg_fis.quantidade_volume,
              reg_fis.especie_volume,
              reg_fis.peso_bruto,
              reg_fis.peso_liquido,
              reg_fis.placa_veiculo,
              reg_fis.observação,
              reg_fis.tipo_nota_fiscal,
              reg_fis.inscricao_suframa,
              reg_fis.uf_veiculo,
              reg_fis.cancelado,
              reg_fis.id_empresa);
              
                
      COMMIT;
      
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK;
          GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_registro_fiscal_pkg.INSERIR');
          dbms_output.put_line(SQLERRM);



END INSERIR;



PROCEDURE ALTERAR(p_id_nota efd_registro_fiscal_tab.id_nota_fiscal%TYPE
                , p_id_empresa efd_registro_fiscal_tab.id_empresa%TYPE
                , reg_fis efd_registro_fiscal_tab%ROWTYPE
                , p_count OUT number) IS

       v_id_nota    efd_registro_fiscal_tab.id_nota_fiscal%TYPE;
       v_id_empresa efd_registro_fiscal_tab.id_empresa%TYPE;
       
       BEGIN
         
         v_id_nota    := p_id_nota;
         v_id_empresa := p_id_empresa;
         p_count      := 0;
         
         UPDATE efd_registro_fiscal_tab 
            SET no_nota_fiscal                = reg_fis.no_nota_fiscal,
                serie                         = reg_fis.serie,
                sub_serie                     = reg_fis.sub_serie,
                uf_orig                       = reg_fis.uf_orig,
                endereco_dest                 = reg_fis.endereco_dest,
                cidade_dest                   = reg_fis.cidade_dest,
                pais_dest                     = reg_fis.pais_dest,
                bairro_dest                   = reg_fis.bairro_dest,
                uf_dest                       = reg_fis.uf_dest,
                cep_dest                      = reg_fis.cep_dest,
                cnpj_cpf_dest                 = reg_fis.cnpj_cpf_dest,
                data_entrada                  = reg_fis.data_entrada,
                data_emissao                  = reg_fis.data_emissao,
                tipo_entrada_saida            = reg_fis.tipo_entrada_saida,
                especie_nf                    = reg_fis.especie_nf,
                no_documento                  = reg_fis.no_documento,
                value_frete                   = reg_fis.value_frete,
                valor_seguro                  = reg_fis.valor_seguro,
                valor_despesas_acessorias     = reg_fis.valor_despesas_acessorias,
                status_nota_fiscal            = reg_fis.status_nota_fiscal,
                valor_mercadoria              = reg_fis.valor_mercadoria,
                valor_ipi                     = reg_fis.valor_ipi,
                valor_desconto                = reg_fis.valor_desconto,
                valor_substituicao_tributaria = reg_fis.valor_substituicao_tributaria,
                inscricao_substituicao_trib   = reg_fis.inscricao_substituicao_trib,
                quantidade_volume             = reg_fis.quantidade_volume,
                especie_volume                = reg_fis.especie_volume,
                peso_bruto                    = reg_fis.peso_bruto,
                peso_liquido                  = reg_fis.peso_liquido,
                placa_veiculo                 = reg_fis.placa_veiculo,
                observação                    = reg_fis.observação,
                tipo_nota_fiscal              = reg_fis.tipo_nota_fiscal,
                inscricao_suframa             = reg_fis.inscricao_suframa,
                uf_veiculo                    = reg_fis.uf_veiculo,
                cancelado                     = reg_fis.cancelado,
                id_empresa                    = reg_fis.id_empresa    
                
          WHERE id_nota_fiscal = v_id_nota
            AND id_empresa     = v_id_empresa;
          
          p_count := SQL%ROWCOUNT;
          
          COMMIT;
          
          
          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK;
              GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_registro_fiscal_pkg.ALTERAR');
              dbms_output.put_line(SQLERRM);
          
END ALTERAR;


end efd_registro_fiscal_pkg;
/
