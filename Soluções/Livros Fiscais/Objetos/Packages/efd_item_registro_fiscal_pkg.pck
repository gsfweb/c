create or replace package efd_item_registro_fiscal_pkg is

PROCEDURE INSERIR(reg_item_fis efd_item_registro_fiscal_tab%ROWTYPE);

PROCEDURE ALTERAR(p_id_reg_fiscal efd_item_registro_fiscal_tab.id_reg_fiscal%TYPE
                , p_linha         efd_item_registro_fiscal_tab.no_linha%TYPE
                , reg_item_fis    efd_item_registro_fiscal_tab%ROWTYPE
                , p_count OUT     NUMBER  );

end efd_item_registro_fiscal_pkg;
/
create or replace package body efd_item_registro_fiscal_pkg is



PROCEDURE INSERIR(reg_item_fis efd_item_registro_fiscal_tab%ROWTYPE) IS
  
  BEGIN
    INSERT INTO EFD_ITEM_REGISTRO_FISCAL_TAB( id_reg_fiscal
                                            , no_linha
                                            , numero_item_nf
                                            , cod_produto
                                            , descricao_produto
                                            , cfop
                                            , valor_desconto
                                            , cst_pis
                                            , cst_cofins
                                            , cst_icms
                                            , perc_base_reducao_icms
                                            , classificacao_icms
                                            , base_insenta_icms
                                            , outras_bases_icms
                                            , perc_icms
                                            , base_calculo_icms
                                            , tipo_operacao_icms
                                            , cod_tributacao_icms
                                            , valor_icms
                                            , perc_base_reducao_ipi
                                            , classificacao_ipi
                                            , base_isenta_ipi
                                            , outras_bases_ipi
                                            , perc_ipi
                                            , base_calculo_ipi
                                            , tipo_operacao_ipi
                                            , valor_ipi
                                            , perc_recuperacao_ipi
                                            , perc_iss
                                            , base_isenta_iss
                                            , base_calculo_iss
                                            , valor_iss
                                            , perc_reducao_iss
                                            , cod_servico_iss
                                            , quantidade_item
                                            , valor_total_item
                                            , id_natureza_operacao
                                            , sub_item_natureza_operacao
                                            , substituicao_icms
                                            , valor_substituicao_icms
                                            , base_substituicao_icms
                                            , inscricao_substituicao_icms
                                            , unidade_medida
                                            , valor_despesa_acessoria
                                            , valor_contabil
                                            , valor_frete
                                            , classificacao_cofins
                                            , valor_cofins
                                            , perc_cofins
                                            , perc_desconto
                                            , valor_diferenca_icms
                                            , perc_icms_zone_franca
                                            , valor_icms_zona_franca
                                            , cod_retencao_inss
                                            , perc_inss
                                            , perc_reducao_inss
                                            , valor_inss
                                            , valor_seguro
                                            , valor_ipi_a_recuperar
                                            , cod_retencao_irpj
                                            , valor_base_ir
                                            , perc_ir
                                            , perc_reducao_ir
                                            , valor_ir
                                            , valor_material
                                            , valor_frete_nota
                                            , texto_nota
                                            , classificacao_pis
                                            , perc_pis
                                            , valor_pis
                                            , valor_servico
                                            , preco_bruto_unitario
                                            , preco_unitario
                                            , valor_reter_ir
                                            , valor_inss_a_reter
                                            , valor_iss_a_reter
                                            , valor_custo_frete
                                            , valor_ipi_descolado
                                            , valor_pis_a_reter
                                            , valor_cofins_a_reter
                                            , valor_csll_a_reter
                                            , perc_pis_zona_franca
                                            , valor_pis_zona_franca
                                            , perc_cofins_zona_franca
                                            , valor_cofins_zona_franca
                                            , valor_retencao_icms
                                            , desc_cod_servico_iss
                                            , perc_retencao_pis
                                            , perc_retencao_cofins
                                            , perc_retencao_csll
                                            , cod_retencao_pis
                                            , linha_retencao_pis
                                            , cod_retencao_cofins
                                            , linha_retencao_cofins
                                            , valor_parcial_antecip_icms
                                            , perc_base_reducao_pis
                                            , perc_base_reducao_cofins
                                            , base_calculo_pis
                                            , base_calculo_cofins
                                            , cod_contribuicao_social
                                            , cod_tipo_credito
                                            , cod_base_credito
                                            , base_calculo_inss
                                            , observacao
                                            , cod_enquadramento
                                            , cst_ipi )
                                     VALUES ( reg_item_fis.id_reg_fiscal
                                            , reg_item_fis.no_linha
                                            , reg_item_fis.numero_item_nf
                                            , reg_item_fis.cod_produto
                                            , reg_item_fis.descricao_produto
                                            , reg_item_fis.cfop
                                            , reg_item_fis.valor_desconto
                                            , reg_item_fis.cst_pis
                                            , reg_item_fis.cst_cofins
                                            , reg_item_fis.cst_icms
                                            , reg_item_fis.perc_base_reducao_icms
                                            , reg_item_fis.classificacao_icms
                                            , reg_item_fis.base_insenta_icms
                                            , reg_item_fis.outras_bases_icms
                                            , reg_item_fis.perc_icms
                                            , reg_item_fis.base_calculo_icms
                                            , reg_item_fis.tipo_operacao_icms
                                            , reg_item_fis.cod_tributacao_icms
                                            , reg_item_fis.valor_icms
                                            , reg_item_fis.perc_base_reducao_ipi
                                            , reg_item_fis.classificacao_ipi
                                            , reg_item_fis.base_isenta_ipi
                                            , reg_item_fis.outras_bases_ipi
                                            , reg_item_fis.perc_ipi
                                            , reg_item_fis.base_calculo_ipi
                                            , reg_item_fis.tipo_operacao_ipi
                                            , reg_item_fis.valor_ipi
                                            , reg_item_fis.perc_recuperacao_ipi
                                            , reg_item_fis.perc_iss
                                            , reg_item_fis.base_isenta_iss
                                            , reg_item_fis.base_calculo_iss
                                            , reg_item_fis.valor_iss
                                            , reg_item_fis.perc_reducao_iss
                                            , reg_item_fis.cod_servico_iss
                                            , reg_item_fis.quantidade_item
                                            , reg_item_fis.valor_total_item
                                            , reg_item_fis.id_natureza_operacao
                                            , reg_item_fis.sub_item_natureza_operacao
                                            , reg_item_fis.substituicao_icms
                                            , reg_item_fis.valor_substituicao_icms
                                            , reg_item_fis.base_substituicao_icms
                                            , reg_item_fis.inscricao_substituicao_icms
                                            , reg_item_fis.unidade_medida
                                            , reg_item_fis.valor_despesa_acessoria
                                            , reg_item_fis.valor_contabil
                                            , reg_item_fis.valor_frete
                                            , reg_item_fis.classificacao_cofins
                                            , reg_item_fis.valor_cofins
                                            , reg_item_fis.perc_cofins
                                            , reg_item_fis.perc_desconto
                                            , reg_item_fis.valor_diferenca_icms
                                            , reg_item_fis.perc_icms_zone_franca
                                            , reg_item_fis.valor_icms_zona_franca
                                            , reg_item_fis.cod_retencao_inss
                                            , reg_item_fis.perc_inss
                                            , reg_item_fis.perc_reducao_inss
                                            , reg_item_fis.valor_inss
                                            , reg_item_fis.valor_seguro
                                            , reg_item_fis.valor_ipi_a_recuperar
                                            , reg_item_fis.cod_retencao_irpj
                                            , reg_item_fis.valor_base_ir
                                            , reg_item_fis.perc_ir
                                            , reg_item_fis.perc_reducao_ir
                                            , reg_item_fis.valor_ir
                                            , reg_item_fis.valor_material
                                            , reg_item_fis.valor_frete_nota
                                            , reg_item_fis.texto_nota
                                            , reg_item_fis.classificacao_pis
                                            , reg_item_fis.perc_pis
                                            , reg_item_fis.valor_pis
                                            , reg_item_fis.valor_servico
                                            , reg_item_fis.preco_bruto_unitario
                                            , reg_item_fis.preco_unitario
                                            , reg_item_fis.valor_reter_ir
                                            , reg_item_fis.valor_inss_a_reter
                                            , reg_item_fis.valor_iss_a_reter
                                            , reg_item_fis.valor_custo_frete
                                            , reg_item_fis.valor_ipi_descolado
                                            , reg_item_fis.valor_pis_a_reter
                                            , reg_item_fis.valor_cofins_a_reter
                                            , reg_item_fis.valor_csll_a_reter
                                            , reg_item_fis.perc_pis_zona_franca
                                            , reg_item_fis.valor_pis_zona_franca
                                            , reg_item_fis.perc_cofins_zona_franca
                                            , reg_item_fis.valor_cofins_zona_franca
                                            , reg_item_fis.valor_retencao_icms
                                            , reg_item_fis.desc_cod_servico_iss
                                            , reg_item_fis.perc_retencao_pis
                                            , reg_item_fis.perc_retencao_cofins
                                            , reg_item_fis.perc_retencao_csll
                                            , reg_item_fis.cod_retencao_pis
                                            , reg_item_fis.linha_retencao_pis
                                            , reg_item_fis.cod_retencao_cofins
                                            , reg_item_fis.linha_retencao_cofins
                                            , reg_item_fis.valor_parcial_antecip_icms
                                            , reg_item_fis.perc_base_reducao_pis
                                            , reg_item_fis.perc_base_reducao_cofins
                                            , reg_item_fis.base_calculo_pis
                                            , reg_item_fis.base_calculo_cofins
                                            , reg_item_fis.cod_contribuicao_social
                                            , reg_item_fis.cod_tipo_credito
                                            , reg_item_fis.cod_base_credito
                                            , reg_item_fis.base_calculo_inss
                                            , reg_item_fis.observacao
                                            , reg_item_fis.cod_enquadramento
                                            , reg_item_fis.cst_ipi );
                                            
                                            
    COMMIT;
    
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
        GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_item_registro_fiscal_pkg.INSERIR');
        dbms_output.put_line(SQLERRM);
  
END INSERIR;


PROCEDURE ALTERAR(p_id_reg_fiscal efd_item_registro_fiscal_tab.id_reg_fiscal%TYPE
                , p_linha         efd_item_registro_fiscal_tab.no_linha%TYPE
                , reg_item_fis    efd_item_registro_fiscal_tab%ROWTYPE
                , p_count OUT     NUMBER  ) IS
                
       v_id_reg_fiscal efd_item_registro_fiscal_tab.id_reg_fiscal%TYPE;
       v_linha         efd_item_registro_fiscal_tab.no_linha%TYPE;
       
       BEGIN
         
         v_id_reg_fiscal := p_id_reg_fiscal;
         v_linha         := p_linha;
         p_count         := 0;
         
         UPDATE efd_item_registro_fiscal_tab
            SET numero_item_nf              = reg_item_fis.numero_item_nf   
              , cod_produto                 = reg_item_fis.cod_produto
              , descricao_produto           = reg_item_fis.descricao_produto
              , cfop                        = reg_item_fis.cfop
              , valor_desconto              = reg_item_fis.valor_desconto
              , cst_pis                     = reg_item_fis.cst_pis
              , cst_cofins                  = reg_item_fis.cst_cofins
              , cst_icms                    = reg_item_fis.cst_icms
              , perc_base_reducao_icms      = reg_item_fis.perc_base_reducao_icms
              , classificacao_icms          = reg_item_fis.classificacao_icms
              , base_insenta_icms           = reg_item_fis.base_insenta_icms
              , outras_bases_icms           = reg_item_fis.outras_bases_icms
              , perc_icms                   = reg_item_fis.perc_icms
              , base_calculo_icms           = reg_item_fis.base_calculo_icms
              , tipo_operacao_icms          = reg_item_fis.tipo_operacao_icms
              , cod_tributacao_icms         = reg_item_fis.cod_tributacao_icms
              , valor_icms                  = reg_item_fis.valor_icms
              , perc_base_reducao_ipi       = reg_item_fis.perc_base_reducao_ipi
              , classificacao_ipi           = reg_item_fis.classificacao_ipi
              , base_isenta_ipi             = reg_item_fis.base_isenta_ipi
              , outras_bases_ipi            = reg_item_fis.outras_bases_ipi
              , perc_ipi                    = reg_item_fis.perc_ipi
              , base_calculo_ipi            = reg_item_fis.base_calculo_ipi
              , tipo_operacao_ipi           = reg_item_fis.tipo_operacao_ipi
              , valor_ipi                   = reg_item_fis.valor_ipi
              , perc_recuperacao_ipi        = reg_item_fis.perc_recuperacao_ipi
              , perc_iss                    = reg_item_fis.perc_iss
              , base_isenta_iss             = reg_item_fis.base_isenta_iss
              , base_calculo_iss            = reg_item_fis.base_calculo_iss
              , valor_iss                   = reg_item_fis.valor_iss
              , perc_reducao_iss            = reg_item_fis.perc_reducao_iss
              , cod_servico_iss             = reg_item_fis.cod_servico_iss
              , quantidade_item             = reg_item_fis.quantidade_item
              , valor_total_item            = reg_item_fis.valor_total_item
              , id_natureza_operacao        = reg_item_fis.id_natureza_operacao
              , sub_item_natureza_operacao  = reg_item_fis.sub_item_natureza_operacao
              , substituicao_icms           = reg_item_fis.substituicao_icms
              , valor_substituicao_icms     = reg_item_fis.valor_substituicao_icms
              , base_substituicao_icms      = reg_item_fis.base_substituicao_icms
              , inscricao_substituicao_icms = reg_item_fis.inscricao_substituicao_icms
              , unidade_medida              = reg_item_fis.unidade_medida
              , valor_despesa_acessoria     = reg_item_fis.valor_despesa_acessoria
              , valor_contabil              = reg_item_fis.valor_contabil
              , valor_frete                 = reg_item_fis.valor_frete
              , classificacao_cofins        = reg_item_fis.classificacao_cofins
              , valor_cofins                = reg_item_fis.valor_cofins
              , perc_cofins                 = reg_item_fis.perc_cofins
              , perc_desconto               = reg_item_fis.perc_desconto
              , valor_diferenca_icms        = reg_item_fis.valor_diferenca_icms
              , perc_icms_zone_franca       = reg_item_fis.perc_icms_zone_franca
              , valor_icms_zona_franca      = reg_item_fis.valor_icms_zona_franca
              , cod_retencao_inss           = reg_item_fis.cod_retencao_inss
              , perc_inss                   = reg_item_fis.perc_inss
              , perc_reducao_inss           = reg_item_fis.perc_reducao_inss
              , valor_inss                  = reg_item_fis.valor_inss
              , valor_seguro                = reg_item_fis.valor_seguro
              , valor_ipi_a_recuperar       = reg_item_fis.valor_ipi_a_recuperar
              , cod_retencao_irpj           = reg_item_fis.cod_retencao_irpj
              , valor_base_ir               = reg_item_fis.valor_base_ir
              , perc_ir                     = reg_item_fis.perc_ir
              , perc_reducao_ir             = reg_item_fis.perc_reducao_ir
              , valor_ir                    = reg_item_fis.valor_ir
              , valor_material              = reg_item_fis.valor_material
              , valor_frete_nota            = reg_item_fis.valor_frete_nota
              , texto_nota                  = reg_item_fis.texto_nota
              , classificacao_pis           = reg_item_fis.classificacao_pis
              , perc_pis                    = reg_item_fis.perc_pis
              , valor_pis                   = reg_item_fis.valor_pis
              , valor_servico               = reg_item_fis.valor_servico
              , preco_bruto_unitario        = reg_item_fis.preco_bruto_unitario
              , preco_unitario              = reg_item_fis.preco_unitario
              , valor_reter_ir              = reg_item_fis.valor_reter_ir
              , valor_inss_a_reter          = reg_item_fis.valor_inss_a_reter
              , valor_iss_a_reter           = reg_item_fis.valor_iss_a_reter 
              , valor_custo_frete           = reg_item_fis.valor_custo_frete
              , valor_ipi_descolado         = reg_item_fis.valor_ipi_descolado
              , valor_pis_a_reter           = reg_item_fis.valor_pis_a_reter
              , valor_cofins_a_reter        = reg_item_fis.valor_cofins_a_reter
              , valor_csll_a_reter          = reg_item_fis.valor_csll_a_reter
              , perc_pis_zona_franca        = reg_item_fis.perc_pis_zona_franca
              , valor_pis_zona_franca       = reg_item_fis.valor_pis_zona_franca
              , perc_cofins_zona_franca     = reg_item_fis.perc_cofins_zona_franca
              , valor_cofins_zona_franca    = reg_item_fis.valor_cofins_zona_franca
              , valor_retencao_icms         = reg_item_fis.valor_retencao_icms
              , desc_cod_servico_iss        = reg_item_fis.desc_cod_servico_iss
              , perc_retencao_pis           = reg_item_fis.perc_retencao_pis
              , perc_retencao_cofins        = reg_item_fis.perc_retencao_cofins
              , perc_retencao_csll          = reg_item_fis.perc_retencao_csll
              , cod_retencao_pis            = reg_item_fis.cod_retencao_pis
              , linha_retencao_pis          = reg_item_fis.linha_retencao_pis
              , cod_retencao_cofins         = reg_item_fis.cod_retencao_cofins
              , linha_retencao_cofins       = reg_item_fis.linha_retencao_cofins
              , valor_parcial_antecip_icms  = reg_item_fis.valor_parcial_antecip_icms
              , perc_base_reducao_pis       = reg_item_fis.perc_base_reducao_pis
              , perc_base_reducao_cofins    = reg_item_fis.perc_base_reducao_cofins
              , base_calculo_pis            = reg_item_fis.base_calculo_pis
              , base_calculo_cofins         = reg_item_fis.base_calculo_cofins
              , cod_contribuicao_social     = reg_item_fis.cod_contribuicao_social
              , cod_tipo_credito            = reg_item_fis.cod_tipo_credito
              , cod_base_credito            = reg_item_fis.cod_base_credito
              , base_calculo_inss           = reg_item_fis.base_calculo_inss
              , observacao                  = reg_item_fis.observacao
              , cod_enquadramento           = reg_item_fis.cod_enquadramento
              , cst_ipi                     = reg_item_fis.cst_ipi
        WHERE id_reg_fiscal = v_id_reg_fiscal
          AND no_linha      = v_linha;
            
        
        p_count := SQL%ROWCOUNT;     
            
        COMMIT;  
        
        EXCEPTION
          WHEN OTHERS THEN
            ROLLBACK;
            GSF_LOG_PKG.Inserir('EO',SQLERRM,'efd_item_registro_fiscal_pkg.ALTERAR');
            dbms_output.put_line(SQLERRM);
                
END ALTERAR;


end efd_item_registro_fiscal_pkg;
/
