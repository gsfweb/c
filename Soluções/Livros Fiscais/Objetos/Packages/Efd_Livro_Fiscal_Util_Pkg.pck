CREATE OR REPLACE PACKAGE Efd_Livro_Fiscal_Util_Pkg
IS
PROCEDURE Gerar_Livro_Fiscal_P1 (p_id_empresa NUMBER, p_mes_ano_fiscal VARCHAR2, p_result OUT NUMBER);

PROCEDURE Gerar_Livro_Fiscal_P2 (p_id_empresa NUMBER, p_mes_ano_fiscal VARCHAR2, p_result OUT NUMBER);

END;
/
CREATE OR REPLACE PACKAGE BODY Efd_Livro_Fiscal_Util_Pkg
IS

PROCEDURE Gerar_Livro_Fiscal_P1 (p_id_empresa NUMBER, p_mes_ano_fiscal VARCHAR2, p_result OUT NUMBER)
IS
  v_res GSF_Livro_Fiscal_P1%ROWTYPE;
  tabela_regra  gsf_regra_liv_fiscal_p1_Pkg.t_regra;
  p_data_inicio DATE := '01/' || p_mes_ano_fiscal;
  p_data_fim DATE := last_day(p_data_inicio);
  v_result NUMBER := gsf_result_seq.nextval();
  v_linha NUMBER := 0;
BEGIN
  FOR reg IN  (
                SELECT r.especie_nf,
                       r.data_emissao,
                       r.serie || r.sub_serie serie_subserie,
                       r.no_nota_fiscal,
                       r.data_entrada,
                       r.uf_orig,
                       i.cod_produto,
                       i.cfop,
                       i.perc_icms,
                       i.cst_icms,
                       i.cst_ipi,
                       i.valor_total_item,
                       NVL(i.valor_ipi, 0) valor_ipi,
                       NVL(i.valor_icms, 0) valor_icms
                FROM   EFD_REGISTRO_FISCAL_TAB r INNER JOIN EFD_ITEM_REGISTRO_FISCAL_TAB i ON r.id_reg_fiscal = i.Id_Reg_Fiscal
                WHERE  r.id_empresa = p_id_empresa
                AND    r.tipo_entrada_saida = 'E'
                AND    r.data_entrada >= p_data_inicio AND r.data_entrada <= p_data_fim
                ORDER BY r.data_entrada,
                         i.cfop
                       
              ) LOOP


    v_res.id_resultado := v_result;

    v_linha := v_linha + 1;
    v_res.id_linha := v_linha;
    
    v_res.data_entrada := TO_DATE(reg.data_entrada, 'dd/mm/yyyy');
    v_res.especie := reg.especie_nf;
    v_res.serie_sub_serie := reg.serie_subserie;
    v_res.numero_nota_fiscal := reg.no_nota_fiscal;
    v_res.data_documento := TO_DATE(reg.data_emissao, 'dd/mm/yyyy');
    v_res.uf_origem := reg.uf_orig;

    --Obter regra ICMS/IPI
    tabela_regra := Gsf_Regra_Liv_Fiscal_p1_Pkg.Obtem_Dados_Regra(p_id_empresa, 'ICMS', reg.uf_orig, reg.cfop, reg.cst_icms);

    IF tabela_regra.COUNT = 0 THEN
      gsf_log_pkg.Inserir('LF', 'Regra n�o cadastrada: (' || p_id_empresa || '/ICMS/' || reg.uf_orig || '/'|| reg.cfop || '/' || reg.cst_icms || ')', 'Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_Icms_Pkg.Obtem_Dados_Regra');
      --dbms_output.put_line(p_id_empresa || '/P2' || '/NULL/' || reg.uf_origem || '/'|| reg.cod_fiscal_operacoes || '/' || reg.cod_enquadramento || '/' || reg.cst_icms || '/Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_Icms_Pkg.Obtem_Dados_Regra');
    ELSE
      v_res.cod_valor_fiscal_icms := tabela_regra(1).cod_valor_fiscal;
    END IF;
    
    v_res.valor_contabil := TO_NUMBER(reg.valor_total_item) + TO_NUMBER(reg.valor_ipi);
      
    tabela_regra := Gsf_Regra_Liv_Fiscal_P1_Pkg.Obtem_Dados_Regra(p_id_empresa, 'IPI', reg.uf_orig, reg.cfop, reg.cst_ipi);

    v_res.aliquota_icms := reg.perc_icms;
    v_res.imposto_creditado_icms := reg.valor_icms;

    IF tabela_regra.COUNT = 0 THEN
      gsf_log_pkg.Inserir('LF', 'Regra n�o cadastrada: (' || p_id_empresa || '/IPI/' || reg.uf_orig || '/'|| reg.cfop || '/' || reg.cst_ipi || ')', 'Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_IPI_Pkg.Obtem_Dados_Regra');
      --dbms_output.put_line(p_id_empresa || '/P2' || '/NULL/' || reg.uf_origem || '/'|| reg.cod_fiscal_operacoes || '/' || reg.cod_enquadramento || '/' || reg.cst_icms || '/Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_IPI_Pkg.Obtem_Dados_Regra');
    ELSE
      v_res.cod_valor_fiscal_ipi := tabela_regra(1).cod_valor_fiscal;
    END IF;

    v_res.base_calculo_ipi := reg.valor_total_item;
    v_res.imposto_creditado_ipi := reg.valor_ipi;

    INSERT INTO GSF_Livro_Fiscal_P1 VALUES v_res;
  END LOOP;
     
  p_result := v_result;
END;

PROCEDURE Gerar_Livro_Fiscal_P2 (p_id_empresa NUMBER, p_mes_ano_fiscal VARCHAR2, p_result OUT NUMBER)
IS
  v_res Gsf_Livro_Fiscal_P2%ROWTYPE;
  tabela_regra Gsf_Regra_Liv_Fiscal_P2_Pkg.t_regra;
  p_data_inicio DATE := '01/' || p_mes_ano_fiscal;
  p_data_fim DATE := last_day(p_data_inicio);
  v_result NUMBER;
  v_linha NUMBER := 0;

  CURSOR atr IS
    SELECT especie_nf,
           serie_subserie,
           no_nota_fiscal,
           dia,
           codigo_emitente,
           uf_origem,
           cod_fiscal_operacoes,
           cst_icms,
           cst_ipi,
           valor_contabil,
           valor_total_item,
           VALUE_TYPE tipo_imposto,
           VALUE valor_imposto
    FROM
    (
     (
     SELECT *
     FROM 
       (
       SELECT CAST(especie_nf AS VARCHAR2(5)) especie_nf,
              CAST(serie_sub_serie AS VARCHAR2(12)) serie_subserie,
              CAST(no_nota_fiscal AS VARCHAR2(12)) no_nota_fiscal,
              TO_CHAR(data_emissao, 'DD') dia,
              CAST(NULL AS VARCHAR2(100)) codigo_emitente,
              CAST(uf_orig AS VARCHAR2(2)) uf_origem,
              CAST(cfop AS VARCHAR2(5)) cod_fiscal_operacoes,
              CAST(perc_icms AS VARCHAR2(100)) perc_icms,
              CAST(cst_icms AS VARCHAR2(2)) cst_icms,
              CAST(cst_ipi AS VARCHAR2(2)) cst_ipi,
              CAST(valor_contabil AS VARCHAR2(100)) valor_contabil,
              CAST(valor_total_item AS VARCHAR2(100)) valor_total_item,
              CAST(valor_ipi AS VARCHAR2(100)) "IPI",
              CAST(valor_icms AS VARCHAR2(100)) "ICMS"
       FROM   (
              SELECT r.especie_nf,
                     r.data_emissao,
                     r.serie || r.sub_serie serie_sub_serie,
                     r.no_nota_fiscal,
                     r.uf_orig,
                     i.cfop,
                     i.perc_icms,
                     i.cst_icms,
                     i.cst_ipi,
                     SUM(i.valor_total_item + NVL(i.valor_ipi, 0)) valor_contabil,
                     SUM(i.valor_total_item) valor_total_item,
                     SUM(NVL(i.valor_ipi, 0)) valor_ipi,
                     SUM(NVL(i.valor_icms, 0)) valor_icms
              FROM   EFD_REGISTRO_FISCAL_TAB r INNER JOIN EFD_ITEM_REGISTRO_FISCAL_TAB i ON r.id_reg_fiscal = i.Id_Reg_Fiscal
              WHERE  r.id_empresa = p_id_empresa 
              AND    r.tipo_entrada_saida = 'S'
              AND    r.data_entrada >= p_data_inicio AND r.data_entrada <= p_data_fim
              GROUP BY r.especie_nf,
                       r.data_emissao,
                       r.serie || r.sub_serie,
                       r.no_nota_fiscal,
                       r.uf_orig,
                       i.cfop,
                       i.perc_icms,
                       i.cst_icms,
                       i.cst_ipi
              )
       )
     )
    UNPIVOT
    (
                      
            value
            FOR value_type IN
                (
               "ICMS",
               "IPI"         
                )
    )
    )
    ORDER BY dia, cod_fiscal_operacoes, especie_nf, serie_subserie, no_nota_fiscal, value_type;
  
  v_reg atr%ROWTYPE;
  reg atr%ROWTYPE;
  
  v_valor_contabil NUMBER := 0;
BEGIN
  OPEN atr;
  FETCH atr INTO reg;
  IF atr%NOTFOUND THEN
    CLOSE atr;
    RETURN;
  END IF;
  
  v_reg.especie_nf	                    := reg.especie_nf;
  v_reg.serie_subserie                  := reg.serie_subserie;
  v_reg.no_nota_fiscal                  := reg.no_nota_fiscal;
  v_reg.dia                             := reg.dia;
  v_reg.codigo_emitente                 := reg.codigo_emitente;
  v_reg.Uf_Origem                       := reg.uf_origem;
  v_reg.cod_fiscal_operacoes            := reg.cod_fiscal_operacoes;
  v_reg.cst_icms                        := reg.cst_icms;
  v_reg.cst_ipi                         := reg.cst_ipi;
  v_reg.tipo_imposto                    := reg.tipo_imposto;
  
  v_result := gsf_result_seq.nextval();
  v_res.id_resultado := v_result;

  LOOP
    IF reg.especie_nf 				  != v_reg.especie_nf OR
       reg.serie_subserie 			!= v_reg.serie_subserie OR
       reg.no_nota_fiscal 			!= v_reg.no_nota_fiscal OR
       reg.dia 					        != v_reg.dia OR
       reg.codigo_emitente 		  != v_reg.codigo_emitente OR
       reg.uf_origem 				    != v_reg.uf_origem OR
       reg.cod_fiscal_operacoes != v_reg.cod_fiscal_operacoes OR
       reg.tipo_imposto         != v_reg.tipo_imposto THEN

       v_linha := v_linha + 1;
       v_res.id_linha := v_linha;

       v_res.especie := reg.especie_nf;
       v_res.serie_sub_serie := reg.serie_subserie;
       v_res.numero_nota_fiscal := reg.no_nota_fiscal;
       v_res.dia := reg.dia;
       v_res.uf_destino := NULL;
       v_res.valor_contabil := v_valor_contabil;
       v_res.codif_contabil := NULL;
       v_res.codif_fiscal := reg.cod_fiscal_operacoes; -- CFOP
       v_res.icms_ipi := reg.tipo_imposto;

       INSERT INTO GSF_Livro_Fiscal_P2 VALUES v_res;

       v_reg.especie_nf	               := reg.especie_nf;
       v_reg.serie_subserie            := reg.serie_subserie;
       v_reg.no_nota_fiscal            := reg.no_nota_fiscal;
       v_reg.dia                       := reg.dia;
       v_reg.codigo_emitente           := reg.codigo_emitente;
       v_reg.Uf_Origem                 := reg.uf_origem;
       v_reg.cod_fiscal_operacoes      := reg.cod_fiscal_operacoes;
       v_reg.cst_icms                  := reg.cst_icms;
       v_reg.cst_ipi                   := reg.cst_ipi;
       v_reg.tipo_imposto              := reg.tipo_imposto;
       
       v_valor_contabil   := 0;
    END IF;

    v_valor_contabil := v_valor_contabil + reg.valor_contabil;
    
    --Obter regra ICMS/IPI
    IF reg.tipo_imposto = 'ICMS' THEN
      tabela_regra := Gsf_Regra_Liv_Fiscal_P2_Pkg.Obtem_Dados_Regra(p_id_empresa, 'ICMS',	reg.uf_origem, reg.cod_fiscal_operacoes, reg.cst_icms);

      IF tabela_regra.COUNT = 0 THEN
        gsf_log_pkg.Inserir('LF', 'Regra ICMS n�o cadastrada: (' || p_id_empresa || '/ICMS/' || reg.uf_origem || '/' || reg.cod_fiscal_operacoes || '/' || reg.cst_icms || ')', 'Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_Icms_Pkg.Obtem_Dados_Regra');
        --dbms_output.put_line(p_id_empresa || '/P2' || '/NULL/' || reg.uf_origem || '/'|| reg.cod_fiscal_operacoes || '/' || reg.cod_enquadramento || '/' || reg.cst_icms || '/Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_Icms_Pkg.Obtem_Dados_Regra');
      ELSE  
        IF tabela_regra(1).base_calculo = 'V' THEN
          v_res.base_calculo := reg.valor_total_item;
        ELSE
          v_res.base_calculo := NULL;
        END IF;

        IF tabela_regra(1).aliquota = 'V' THEN
          v_res.aliquota := NULL;
        ELSE
          v_res.aliquota := NULL;
        END IF;

        IF tabela_regra(1).imposto = 'V' THEN
          v_res.imposto_debitado := reg.valor_imposto;
        ELSE
          v_res.imposto_debitado := NULL;
        END IF;

        IF tabela_regra(1).isenta_nao_tributada = 'V' THEN
          v_res.isentas_nao_tributadas := reg.valor_total_item;
        ELSE
          v_res.isentas_nao_tributadas := NULL;
        END IF;

        IF tabela_regra(1).outras = 'V' THEN
          v_res.Outras := reg.valor_total_item;
        ELSE
          v_res.Outras := NULL;
        END IF;
      END IF;
    ELSIF reg.tipo_imposto = 'IPI' THEN
      v_res.valor_contabil := TO_NUMBER(reg.valor_total_item) + TO_NUMBER(reg.valor_imposto);

      tabela_regra := Gsf_Regra_Liv_Fiscal_P2_Pkg.Obtem_Dados_Regra(p_id_empresa, 'IPI', reg.uf_origem, reg.cod_fiscal_operacoes, reg.cst_ipi);

      IF tabela_regra.COUNT = 0 THEN
        gsf_log_pkg.Inserir('LF', 'Regra IPI n�o cadastrada: (' || p_id_empresa || '/IPI/' || reg.uf_origem || '/'|| reg.cod_fiscal_operacoes || '/' || reg.cst_ipi || ')', 'Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_IPI_Pkg.Obtem_Dados_Regra');
        --dbms_output.put_line(p_id_empresa || '/P2' || '/NULL/' || reg.uf_origem || '/'|| reg.cod_fiscal_operacoes || '/' || reg.cod_enquadramento || '/' || reg.cst_icms || '/Efd_Livro_Fiscal_Util_Pkg.Gerar_Livro_Fiscal/Regra_IPI_Pkg.Obtem_Dados_Regra');
      ELSE
        IF tabela_regra(1).base_calculo = 'V' THEN
          v_res.base_calculo := reg.valor_total_item;
        ELSE
          v_res.base_calculo := NULL;
        END IF;
          
        IF tabela_regra(1).aliquota = 'V' THEN
          v_res.aliquota := NULL;
        ELSE
          v_res.aliquota := NULL;
        END IF;
          
        IF tabela_regra(1).imposto = 'V' THEN
          v_res.imposto_debitado := reg.valor_imposto;
        ELSE
          v_res.imposto_debitado := NULL;
        END IF;
          
        IF tabela_regra(1).isenta_nao_tributada = 'V' THEN
          v_res.isentas_nao_tributadas := reg.valor_total_item;
        ELSE
          v_res.isentas_nao_tributadas := NULL;
        END IF;
          
        IF tabela_regra(1).outras = 'V' THEN
          v_res.Outras := reg.valor_total_item;
        ELSE
          v_res.Outras := NULL;
        END IF;
      END IF;
    END IF;
    
    FETCH atr INTO reg;
    EXIT WHEN atr%NOTFOUND;
  END LOOP;

---
  COMMIT;
  
  p_result := v_result;
END;



END;
/
