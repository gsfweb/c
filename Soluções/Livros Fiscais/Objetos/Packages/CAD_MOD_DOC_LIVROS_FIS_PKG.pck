create or replace package CAD_MOD_DOC_LIVROS_FIS_PKG is

procedure INSERIR( p_modelo                 cad_mod_doc_livros_fis_tab.MODELO%TYPE
                 , p_exige_cod_valor_fiscal cad_mod_doc_livros_fis_tab.EXIGE_COD_VALOR_FISCAL%TYPE DEFAULT 'F'
                 , p_descricao              cad_mod_doc_livros_fis_tab.DESCRICAO%TYPE );
                 
procedure ALTERAR( p_modelo                 cad_mod_doc_livros_fis_tab.MODELO%TYPE
                 , p_exige_cod_valor_fiscal cad_mod_doc_livros_fis_tab.EXIGE_COD_VALOR_FISCAL%TYPE
                 , p_descricao              cad_mod_doc_livros_fis_tab.DESCRICAO%TYPE );        
                 
procedure REMOVER( p_modelo cad_mod_doc_livros_fis_tab.MODELO%TYPE );   
                       

end CAD_MOD_DOC_LIVROS_FIS_PKG;
/
create or replace package body CAD_MOD_DOC_LIVROS_FIS_PKG is


v_sql_dynamic clob;

PROCEDURE INSERIR( p_modelo                 cad_mod_doc_livros_fis_tab.MODELO%TYPE
                 , p_exige_cod_valor_fiscal cad_mod_doc_livros_fis_tab.EXIGE_COD_VALOR_FISCAL%TYPE DEFAULT 'F'
                 , p_descricao              cad_mod_doc_livros_fis_tab.DESCRICAO%TYPE ) IS

       v_modelo                 cad_mod_doc_livros_fis_tab.modelo%TYPE;
       v_exige_cod_valor_fiscal cad_mod_doc_livros_fis_tab.exige_cod_valor_fiscal%TYPE; 
       v_descricao              cad_mod_doc_livros_fis_tab.descricao%TYPE;
       
       BEGIN
         
         v_modelo                 := p_modelo;
         v_exige_cod_valor_fiscal := p_exige_cod_valor_fiscal;
         v_descricao              := p_descricao;
         
         v_sql_dynamic := 'INSERT INTO CAD_MOD_DOC_LIVROS_FIS_TAB ( MODELO
                                                                  , EXIGE_COD_VALOR_FISCAL
                                                                  , DESCRICAO )
                                                           VALUES ( :modelo
                                                                  , :exige_cod_valor_fiscal
                                                                  , :descricao )';
                                                                  
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_modelo
             , v_exige_cod_valor_fiscal
             , v_descricao;
             
         COMMIT;
         
         EXCEPTION 
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'CAD_MOD_DOC_LIVROS_FIS_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);                                                                          
                
END INSERIR;



PROCEDURE ALTERAR( p_modelo                 cad_mod_doc_livros_fis_tab.MODELO%TYPE
                 , p_exige_cod_valor_fiscal cad_mod_doc_livros_fis_tab.EXIGE_COD_VALOR_FISCAL%TYPE
                 , p_descricao              cad_mod_doc_livros_fis_tab.DESCRICAO%TYPE ) IS

       v_modelo                 cad_mod_doc_livros_fis_tab.modelo%TYPE;
       v_exige_cod_valor_fiscal cad_mod_doc_livros_fis_tab.exige_cod_valor_fiscal%TYPE;
       v_descricao              cad_mod_doc_livros_fis_tab.descricao%TYPE;
       
       BEGIN
         
         v_modelo                 := p_modelo;
         v_exige_cod_valor_fiscal := p_exige_cod_valor_fiscal;
         v_descricao              := p_descricao;
         
         v_sql_dynamic := 'UPDATE CAD_MOD_DOC_LIVROS_FIS_TAB 
                              SET EXIGE_COD_VALOR_FISCAL = :exige_cod_valor_fiscal
                                , DESCRICAO              = :descricao
                            WHERE MODELO = :modelo';
                           
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_exige_cod_valor_fiscal
             , v_descricao
             , v_modelo;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'CAD_MOD_DOC_LIVROS_FIS_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);
                
     
END ALTERAR;


PROCEDURE REMOVER( p_modelo cad_mod_doc_livros_fis_tab.MODELO%TYPE ) IS
  
       v_modelo cad_mod_doc_livros_fis_tab.modelo%TYPE;
       
       BEGIN
         
         v_modelo := p_modelo;
         
         v_sql_dynamic := 'DELETE 
                             FROM CAD_MOD_DOC_LIVROS_FIS_TAB
                            WHERE MODELO = :modelo';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_modelo;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'CAD_MOD_DOC_LIVROS_FIS_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);
  
END REMOVER;

end CAD_MOD_DOC_LIVROS_FIS_PKG;
/
