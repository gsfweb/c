create or replace package COD_VALOR_FISCAL_PKG is

procedure INSERIR( p_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE
                 , p_descricao        cod_valor_fiscal_tab.DESCRICAO%TYPE ); 
                 
                 
procedure ALTERAR( p_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE
                 , p_descricao        cod_valor_fiscal_tab.DESCRICAO%TYPE );
                 
                 
procedure REMOVER( p_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE );                 

end COD_VALOR_FISCAL_PKG;
/
create or replace package body COD_VALOR_FISCAL_PKG is

v_sql_dynamic clob;

PROCEDURE INSERIR( p_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE
                 , p_descricao        cod_valor_fiscal_tab.DESCRICAO%TYPE ) IS


       v_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE;
       v_descricao        cod_valor_fiscal_tab.DESCRICAO%TYPE;
       
       BEGIN
         
         v_cod_valor_fiscal := p_cod_valor_fiscal;
         v_descricao        := p_descricao;
         
         v_sql_dynamic := 'INSERT INTO COD_VALOR_FISCAL_TAB ( COD_VALOR_FISCAL
                                                            , DESCRICAO )
                                                     VALUES ( :cod_valor_fiscal
                                                            , :descricao )';  
                                                            
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_valor_fiscal
             , v_descricao;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'COD_VALOR_FISCAL_PKG.INSERIR');
             dbms_output.put_line(SQLERRM); 
                                                                                                                             
                 
END INSERIR;   

PROCEDURE ALTERAR( p_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE
                 , p_descricao        cod_valor_fiscal_tab.DESCRICAO%TYPE ) IS
                 
       v_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE;
       v_descricao        cod_valor_fiscal_tab.DESCRICAO%TYPE;
       
       BEGIN
         
         v_cod_valor_fiscal := p_cod_valor_fiscal;
         v_descricao        := p_descricao;
         
         v_sql_dynamic := 'UPDATE COD_VALOR_FISCAL_TAB
                              SET DESCRICAO = :descricao
                            WHERE COD_VALOR_FISCAL = :cod_valor_fiscal';
                            
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_descricao
             , v_cod_valor_fiscal;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'COD_VALOR_FISCAL_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);                     

END ALTERAR; 

PROCEDURE REMOVER( p_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE ) IS
  
       v_cod_valor_fiscal cod_valor_fiscal_tab.COD_VALOR_FISCAL%TYPE;
       
       BEGIN
         
         v_cod_valor_fiscal := p_cod_valor_fiscal;
         
         v_sql_dynamic := 'DELETE 
                             FROM COD_VALOR_FISCAL_TAB
                            WHERE COD_VALOR_FISCAL = :cod_valor_fiscal';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_valor_fiscal;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'COD_VALOR_FISCAL_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);                    
  
END REMOVER;             

end COD_VALOR_FISCAL_PKG;
/
