create or replace package CAD_ENQ_LEGAL_IPI_PKG is

procedure INSERIR( p_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE
                 , p_grupo_cst         cad_enq_legal_ipi_tab.grupo_cst%TYPE
                 , p_descricao         cad_enq_legal_ipi_tab.descricao%TYPE );


procedure ALTERAR( p_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE
                 , p_grupo_cst         cad_enq_legal_ipi_tab.grupo_cst%TYPE
                 , p_descricao         cad_enq_legal_ipi_tab.descricao%TYPE );
                 
procedure REMOVER( p_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE );                 

end CAD_ENQ_LEGAL_IPI_PKG;
/
create or replace package body CAD_ENQ_LEGAL_IPI_PKG is

v_sql_dynamic clob;

PROCEDURE INSERIR( p_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE
                 , p_grupo_cst         cad_enq_legal_ipi_tab.grupo_cst%TYPE
                 , p_descricao         cad_enq_legal_ipi_tab.descricao%TYPE ) IS

       v_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE;
       v_grupo_cst         cad_enq_legal_ipi_tab.grupo_cst%TYPE;
       v_descricao         cad_enq_legal_ipi_tab.descricao%TYPE;
       
       BEGIN
         
         v_cod_enquadramento := p_cod_enquadramento;
         v_grupo_cst         := p_grupo_cst;
         v_descricao         := p_descricao;
         
         v_sql_dynamic := 'INSERT INTO CAD_ENQ_LEGAL_IPI_TAB ( COD_ENQUADRAMENTO
                                                             , GRUPO_CST
                                                             , DESCRICAO )
                                                      VALUES ( :cod_enquadramento
                                                             , :grupo_cst
                                                             , :descricao )';
         
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_enquadramento
             , v_grupo_cst
             , v_descricao;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'CAD_ENQ_LEGAL_IPI_PKG.INSERIR');
             dbms_output.put_line(SQLERRM); 
                                           
END INSERIR; 


PROCEDURE ALTERAR( p_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE
                 , p_grupo_cst         cad_enq_legal_ipi_tab.grupo_cst%TYPE
                 , p_descricao         cad_enq_legal_ipi_tab.descricao%TYPE ) IS

       v_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE;
       v_grupo_cst         cad_enq_legal_ipi_tab.grupo_cst%TYPE;
       v_descricao         cad_enq_legal_ipi_tab.descricao%TYPE;
       
       BEGIN
         
         v_cod_enquadramento := p_cod_enquadramento;
         v_grupo_cst         := p_grupo_cst;
         v_descricao         := p_descricao;
         
         v_sql_dynamic := 'UPDATE CAD_ENQ_LEGAL_IPI_TAB 
                              SET GRUPO_CST = :grupo_cst
                                , DESCRICAO = :descricao
                            WHERE COD_ENQUADRAMENTO = :cod_enquadramento';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_grupo_cst
             , v_descricao
             , v_cod_enquadramento;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'CAD_ENQ_LEGAL_IPI_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);            

END ALTERAR;


PROCEDURE REMOVER( p_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE ) IS
  
       v_cod_enquadramento cad_enq_legal_ipi_tab.cod_enquadramento%TYPE;
       
       BEGIN
         
         v_cod_enquadramento := p_cod_enquadramento;
         
         v_sql_dynamic := 'DELETE 
                             FROM CAD_ENQ_LEGAL_IPI_TAB
                            WHERE COD_ENQUADRAMENTO = :cod_enquadramento';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_enquadramento;
         
         COMMIT;
         
         EXCEPTION 
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'CAD_ENQ_LEGAL_IPI_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);                    
        
  
END REMOVER;                

end CAD_ENQ_LEGAL_IPI_PKG;
/
