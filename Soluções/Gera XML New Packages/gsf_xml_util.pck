create or replace package gsf_xml_util is

-- procedure respons�vel pela identificacao e leitura do xml
procedure gsf_leitura_arquivo_xml ( p_tipo   in varchar2
                                  , p_nome   in varchar2
                                  , p_modelo in varchar2
                                  , p_de_para in varchar2 default 'N'
                                  , p_destino in varchar2);

-- fun��o que retorna o valor da tag
function gsf_retorna_tag ( p_xml  in XMLType
                         , p_base_ini    in varchar2
                         , p_base_query  in varchar2
                         , p_base in varchar2
                         , p_link in varchar2) return varchar2;

-- funcao que retorna a base da tag
function gsf_retorna_base ( p_pai     in number
                          , p_campo   in varchar2) return varchar2;

-- procedure insert de_para
procedure gsf_insert_de_para ( p_arquivo  in number
                             , p_campo    in number
                             , p_conteudo in varchar2
                             , p_repete   in varchar2
                             , p_destino  in varchar2
                             , p_tipo     in varchar2) ;


-- procedure grava destino
procedure gsf_grava_destino (p_destino in varchar2);

-- procedure executa leitura
procedure gsf_executa_leitura;

end gsf_xml_util;


 
/
create or replace package body gsf_xml_util is

procedure gsf_leitura_arquivo_xml ( p_tipo    in varchar2
                                  , p_nome    in varchar2
                                  , p_modelo  in varchar2
                                  , p_de_para in varchar2 default 'N'
                                  , p_destino in varchar2) is

v_tag  varchar2(32000);
v_base varchar2(200);

begin

  -- cursor principal (busco o tipo de arquivo que dever� ser lido)
  for r_arquivo in (select arq.id
                         , arq.link
                         , arq.base_ini
                         , arq.base_completa
                         , xml.xml
                         , xml.id id_xml
                      from gsf_arquivo arq
                         , gsf_xml xml
                     where tipo = p_tipo
                       and nome = p_nome
                       and modelo = p_modelo
                       and arq.id = xml.id_arquivo
                       and xml.lido = 'N'
                      ) loop

     -- curso de_para
     for r_de_para in (select campo
                             , tipo
                             , tamanho
                             , tab_dest
                             , campo_dest
                             , campo_orig
                             , pai
                             , a.id
                             , a.id_arquivo
                             , a.repete
                             , a.ajusta_data
                          from gsf_arquivo_layout a
                             , gsf_layout_saida b
                          where a.id = b.campo_orig
                            and a.id_arquivo = b.id_arquivo
                            and a.id_arquivo = r_arquivo.id
                            and de_para = p_de_para
                            and tab_Dest = p_destino
                            and dado = 'S'
                          order by a.id) loop

        -- monta a base da tag
        v_base := gsf_retorna_base(r_de_para.pai, r_de_para.campo);

        -- chamo fun��o para retornar o valor da tag
        v_tag := gsf_retorna_tag ( XMLType(r_arquivo.xml)
                                 , r_arquivo.base_ini
                                 , r_arquivo.base_completa
                                 , v_base
                                 , r_arquivo.link);

        if r_de_para.tipo = 'DATE' and r_de_para.ajusta_Data = 'S' and v_tag is not null then
          -- corrigindo a data
          select to_date(substr(v_tag,9,2)|| '/' || substr(v_tag,6,2) || '/' || substr(v_tag,1,4) ,'dd/mm/rrrr')
            into v_tag
            from dual;
        end if;

        --dbms_output.put_line(v_tag);

        -- insert do de_para
        gsf_insert_de_para ( r_de_para.id_arquivo
                           , r_de_para.campo_orig
                           , v_tag
                           , r_de_para.repete
                           , p_destino
                           , r_de_para.tipo
                           );

    end loop; -- curso de_para


    -- gravo na tabela destino
    gsf_grava_destino (p_destino);

    -- atualizo xml lido
    update gsf_xml
       set lido = 'S'
     where id = r_arquivo.id_xml;

  end loop;-- cursor principal

end;

-- fun��o que retorna o valor da tag
function gsf_retorna_tag ( p_xml         in XMLType
                         , p_base_ini    in varchar2
                         , p_base_query  in varchar2
                         , p_base        in varchar2
                         , p_link in varchar2) return varchar2 is
begin

  -- extraindo valores para a capa do documento
  for r_xml in (
                  WITH t AS (SELECT p_xml xmlcol
                               from dual )
                    SELECT extractValue(value(x),p_base_query || p_base , p_link) tag
                      FROM t,TABLE(XMLSequence(extract(t.xmlcol,p_base_ini, p_link))) x ) loop


      return r_xml.tag;


    end loop;


end;

-- funcao que retorna a base da tag
function gsf_retorna_base ( p_pai     in number
                          , p_campo   in varchar2) return varchar2 is

v_base    varchar2(400);

begin

  -- loop para encontrar os niveis anteriores
  for r_tag in (select id
                     , pai
                     , campo
                     , dado
                  from gsf_arquivo_layout
                  start with pai = p_pai
                    and campo = p_campo
                  connect by prior  pai = id


                 order by 1) loop

      -- retorna a base
      v_base := v_base || '/' || r_Tag.Campo;

  end loop;

  return v_base;

end;

-- insert de_para
-- procedure insert de_para
procedure gsf_insert_de_para ( p_arquivo  in number
                             , p_campo    in number
                             , p_conteudo in varchar2
                             , p_repete   in varchar2
                             , p_destino  in varchar2
                             , p_tipo     in varchar2
                             ) is

begin

  insert into gsf_de_para
    ( id
     , id_Arquivo
      , id_campo
       , conteudo
        , repete
         , destino
          , tipo)
  values
    ( gsf_de_para_s.nextval
     , p_arquivo
      , p_campo
       , p_conteudo
        , p_repete
         , p_destino
          , p_tipo);

exception
  when others then
    raise_application_error(-20001,'Erro ao inserir GSF_DE_PARA: ' || sqlerrm);

end;

-- procedure grava destino
procedure gsf_grava_destino (p_destino in varchar2 ) is

v_tab     varchar2(32000);
v_insert  varchar2(32000);
v_execute long;
v_alter   varchar2(200) := 'alter session set nls_numeric_characters = '',.''';

begin

  execute immediate v_alter;

  for r_execute in ( select conteudo
                          , tab_dest
                          , campo_Dest
                          , a.id
                          , a.tipo
                       from gsf_de_para a
                          , gsf_layout_saida b
                      where a.id_arquivo = b.id_arquivo
                        and a.id_campo = b.campo_orig
                        and a.lido = 'N'
                        and tab_Dest = p_destino
                      order by tab_Dest
                          , campo_Dest) loop

   -- inicio da montagem do insert dinamico
   if v_insert is null then

     -- valido tipo do campo para formatar
     if r_execute.tipo = 'VARCHAR2' then
       v_tab := 'insert into ' || r_execute.tab_dest || ' (' || r_execute.campo_dest;
       v_insert := ' values (''' || r_execute.conteudo || '''';
     elsif r_execute.tipo = 'NUMBER' then
       v_tab := 'insert into ' || r_execute.tab_dest || ' (' || r_execute.campo_dest;
       v_insert := ' values (' || r_execute.conteudo;
     else
       v_tab := 'insert into ' || r_execute.tab_dest || ' (' || r_execute.campo_dest;
       v_insert := ' values (to_Date(''' || r_execute.conteudo || ''',''dd/mm/rrrr'')';
     end if;
   else

     -- valido tipo do campo para formatar
     if r_execute.tipo = 'VARCHAR2' then
       v_tab := v_tab || ' , ' || r_execute.campo_dest;
       v_insert := v_insert || ' , ''' || r_execute.conteudo || '''';
     elsif r_execute.tipo = 'NUMBER' then
       v_tab := v_tab || ' , ' || r_execute.campo_dest;
       v_insert := v_insert || ' , ' || r_execute.conteudo;
     else
       v_tab := v_tab || ' , ' || r_execute.campo_dest;
       v_insert := v_insert || ' , to_Date(''' || r_execute.conteudo || ''',''dd/mm/rrrr'')';
     end if;


   end if;


  end loop;

  -- concateno o total para corrigir a sintaxe do insert
  v_tab := v_tab || ')';
  v_insert := v_insert || ')';
  v_execute :=  v_tab || v_insert;

  execute immediate v_execute;

end;

-- procedure executa leitura
procedure gsf_executa_leitura is

begin

  for r_arquivo in (select distinct id_arquivo
                         , tab_dest
                         , b.nome
                         , b.modelo
                         , b.tipo
                     from gsf_layout_saida a
                        , gsf_arquivo  b
                     where a.id_arquivo = b.id) loop

    -- chamo procedure para leitura do xml
    gsf_leitura_arquivo_xml ( r_arquivo.tipo
                            , r_arquivo.nome
                            , r_arquivo.modelo
                            , 'S'
                            , r_arquivo.tab_dest);

  end loop;

end;


end gsf_xml_util;
/
