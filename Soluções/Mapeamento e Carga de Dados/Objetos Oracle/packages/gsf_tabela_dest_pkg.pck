CREATE OR REPLACE PACKAGE gsf_tabela_dest_pkg is

PROCEDURE Inserir( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE
                 , p_ds_tabela_dest  gsf_tabela_dest_tab.desc_tabela_dest%TYPE );
                                
PROCEDURE Alterar( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE
                 , p_ds_tabela_dest  gsf_tabela_dest_tab.desc_tabela_dest%TYPE );
                                 
PROCEDURE Remover( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE );

FUNCTION Obtem_Descricao ( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE ) RETURN VARCHAR2;

END gsf_tabela_dest_pkg;
/
CREATE OR REPLACE PACKAGE BODY gsf_tabela_dest_pkg is

--variaveis globais
v_sql_dynamic VARCHAR2(2000);

PROCEDURE Inserir( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE
                 , p_ds_tabela_dest  gsf_tabela_dest_tab.desc_tabela_dest%TYPE ) is

       --variaveis locais
       v_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE;
       v_ds_tabela_dest  gsf_tabela_dest_tab.desc_tabela_dest%TYPE;
       v_id_objeto       gsf_tabela_dest_tab.id_objeto%TYPE;
       
       BEGIN
         
         v_cod_tabela_dest := p_cod_tabela_dest;
         v_ds_tabela_dest  := p_ds_tabela_dest;
         
         v_sql_dynamic := 'insert into GSF_TABELA_DEST_TAB( COD_TABELA_DEST
                                                     , DESC_TABELA_DEST
                                                     , VERSAO_LINHA)
                                              values ( :cod_tabela_dest
                                                     , :ds_tabela_dest
                                                     , sysdate)';
          
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_tabela_dest
             , v_ds_tabela_dest;
             
         SELECT ROWID
           INTO v_id_objeto
           FROM GSF_TABELA_DEST_TAB
          WHERE COD_TABELA_DEST = v_cod_tabela_dest;  
             
         v_sql_dynamic := 'update GSF_TABELA_DEST_TAB 
                              set ID_OBJETO       = :id_objeto
                            where COD_TABELA_DEST = :cod_tabela_dest';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto
             , v_cod_tabela_dest;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TABELA_DEST_PKG.INSERIR');
             dbms_output.put_line(SQLERRM); 

END Inserir;

PROCEDURE Alterar( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE
                 , p_ds_tabela_dest  gsf_tabela_dest_tab.desc_tabela_dest%TYPE ) is
                                 
       --variaveis locais                                
       v_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE;
       v_ds_tabela_dest  gsf_tabela_dest_tab.desc_tabela_dest%TYPE;
       
       BEGIN
         
         v_cod_tabela_dest := p_cod_tabela_dest;
         v_ds_tabela_dest  := p_ds_tabela_dest;
         
         v_sql_dynamic := 'update GSF_TABELA_DEST_TAB
                              set DESC_TABELA_DEST = :ds_tabela_desc
                                , VERSAO_LINHA     = sysdate
                            where COD_TABELA_DEST  = :cod_tabela_desc';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_ds_tabela_dest
             , v_cod_tabela_dest;
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TABELA_DEST_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);    


END Alterar;

PROCEDURE Remover( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE ) is

       --variaveis locais         
       v_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE;
       
       BEGIN
         
         v_cod_tabela_dest := p_cod_tabela_dest;
         
         v_sql_dynamic := 'delete 
                             from GSF_TABELA_DEST_TAB
                            where COD_TABELA_DEST = :cod_tabela_dest';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_tabela_dest;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TABELA_DEST_PKG.REMOVER');
             dbms_output.put_line(SQLERRM); 

END Remover;

FUNCTION Obtem_Descricao ( p_cod_tabela_dest gsf_tabela_dest_tab.cod_tabela_dest%TYPE ) RETURN VARCHAR2
IS
  v_temp gsf_tabela_dest_tab.desc_tabela_dest%TYPE;
  
BEGIN
  SELECT desc_tabela_dest
  INTO   v_temp
  FROM   gsf_tabela_dest_tab
  WHERE  cod_tabela_dest = p_cod_tabela_dest;
  
  RETURN v_temp;
END;

END gsf_tabela_dest_pkg;
/
