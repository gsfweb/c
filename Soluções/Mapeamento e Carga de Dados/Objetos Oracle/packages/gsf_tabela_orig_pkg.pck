CREATE OR REPLACE PACKAGE gsf_tabela_orig_pkg is

PROCEDURE Inserir( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE
                 , p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE
                 , p_ds_tabela_orig  gsf_tabela_orig_tab.desc_tabela_orig%TYPE );
                               
PROCEDURE Alterar( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE
                 , p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE
                 , p_ds_tabela_orig  gsf_tabela_orig_tab.desc_tabela_orig%TYPE );  
                                 

PROCEDURE Remover( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE 
                 , p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE );                         


FUNCTION Obtem_Descricao ( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE, 
                           p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE ) RETURN VARCHAR2;
END gsf_tabela_orig_pkg;
/
CREATE OR REPLACE PACKAGE BODY gsf_tabela_orig_pkg is

--variaveis globais
v_sql_dynamic varchar2(2000);

PROCEDURE Inserir( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE
                 , p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE
                 , p_ds_tabela_orig  gsf_tabela_orig_tab.desc_tabela_orig%TYPE ) is
                               
       --variaveis locais      
       v_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE;
       v_ds_tabela_orig  gsf_tabela_orig_tab.desc_tabela_orig%TYPE;
       v_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE;
       v_id_objeto       gsf_tabela_orig_tab.id_objeto%TYPE;
       
       BEGIN
         
         v_cod_tabela_orig := p_cod_tabela_orig;
         v_ds_tabela_orig  := p_ds_tabela_orig;
         v_id_empresa      := p_id_empresa;
         
         v_sql_dynamic := 'insert into GSF_TABELA_ORIG_TAB( COD_TABELA_ORIG
                                                         , DESC_TABELA_ORIG
                                                         , ID_EMPRESA
                                                         , VERSAO_LINHA)
                                                  values ( :cod_tabela_orig
                                                         , :ds_tabela_orig
                                                         , :id_empresa
                                                         , sysdate )'; 
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_tabela_orig
             , v_ds_tabela_orig
             , v_id_empresa;
         
         SELECT ROWID
           INTO v_id_objeto 
           FROM GSF_TABELA_ORIG_TAB
          WHERE COD_TABELA_ORIG = v_cod_tabela_orig
            AND ID_EMPRESA      = v_id_empresa;
         
         --insere rowid
         v_sql_dynamic := 'update GSF_TABELA_ORIG_TAB
                              set ID_OBJETO       = :id_objeto
                            where COD_TABELA_ORIG = :cod_tabela_orig
                              and ID_EMPRESA      = :id_empresa';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto
             , v_cod_tabela_orig
             , v_id_empresa;
             
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TABELA_ORIG_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);      
             
END Inserir;      

PROCEDURE Alterar( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE
                 , p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE
                 , p_ds_tabela_orig  gsf_tabela_orig_tab.desc_tabela_orig%TYPE ) is

       --variaveis locais                                
       v_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE;
       v_ds_tabela_orig  gsf_tabela_orig_tab.desc_tabela_orig%TYPE;
       v_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE;
       
       BEGIN
         
         v_cod_tabela_orig := p_cod_tabela_orig;
         v_ds_tabela_orig  := p_ds_tabela_orig;
         v_id_empresa      := p_id_empresa;
         
         v_sql_dynamic := 'update GSF_TABELA_ORIG_TAB
                              set DESC_TABELA_ORIG = :ds_tabela_orig
                                , VERSAO_LINHA     = sysdate
                            where COD_TABELA_ORIG  = :cod_tabela_orig
                              and ID_EMPRESA       = :id_empresa';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_ds_tabela_orig
             , v_cod_tabela_orig
             , v_id_empresa;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TABELA_ORIG_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);     


END Alterar;

PROCEDURE Remover( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE 
                 , p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE ) is
                               
       --variaveis locais      
       v_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE;
       v_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE;
       
       BEGIN
         
         v_id_empresa      := p_id_empresa;
         v_cod_tabela_orig := p_cod_tabela_orig;
         
         v_sql_dynamic := 'delete 
                             from GSF_TABELA_ORIG_TAB
                            where COD_TABELA_ORIG = :cod_tabela_orig
                              and ID_EMPRESA      = :id_empresa';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_tabela_orig
             , v_id_empresa;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_TABELA_ORIG_PKG.REMOVER');
             dbms_output.put_line(SQLERRM); 
  
END Remover;


FUNCTION Obtem_Descricao ( p_id_empresa      gsf_tabela_orig_tab.id_empresa%TYPE, 
                           p_cod_tabela_orig gsf_tabela_orig_tab.cod_tabela_orig%TYPE ) RETURN VARCHAR2
IS
  v_temp gsf_tabela_orig_tab.desc_tabela_orig%TYPE;
BEGIN
  SELECT desc_tabela_orig
  INTO   v_temp
  FROM   gsf_tabela_orig_tab
  WHERE  id_empresa = p_id_empresa
  AND    cod_tabela_orig = p_cod_tabela_orig;
    
  RETURN v_temp;
END;
END gsf_tabela_orig_pkg;
/
