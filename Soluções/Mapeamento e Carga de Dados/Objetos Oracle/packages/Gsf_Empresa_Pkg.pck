CREATE OR REPLACE PACKAGE Gsf_Empresa_Pkg is

PROCEDURE Inserir( p_nome_empresa   gsf_empresa.nome_empresa%TYPE
                 , p_id_empresa_pai gsf_empresa.id_empresa_pai%TYPE
                 , p_usu_inclusao   gsf_empresa.usu_inclusao%TYPE
                 , p_cnpj           gsf_empresa.cnpj%TYPE
                 , p_sigla_la       gsf_empresa.sigla_la%TYPE );

PROCEDURE Alterar ( p_id_empresa        gsf_empresa.id%TYPE
                  , p_nome_empresa      gsf_empresa.nome_empresa%TYPE
                  , p_id_empresa_pai    gsf_empresa.id_empresa_pai%TYPE
                  , p_usu_alt_alteracao gsf_empresa.usu_ult_alteracao%TYPE
                  , p_cnpj              gsf_empresa.cnpj%TYPE
                  , p_sigla_la          gsf_empresa.sigla_la%TYPE );


PROCEDURE Remover ( p_id_empresa gsf_empresa.id%TYPE );


FUNCTION Obtem_Nome_Empresa( p_id_empresa gsf_empresa.id%TYPE )return varchar2;


END Gsf_Empresa_Pkg;
/
CREATE OR REPLACE PACKAGE BODY Gsf_Empresa_Pkg is

--variaveis globais
v_sql_dynamic VARCHAR2(2000);

PROCEDURE Inserir( p_nome_empresa   gsf_empresa.nome_empresa%TYPE
                 , p_id_empresa_pai gsf_empresa.id_empresa_pai%TYPE
                 , p_usu_inclusao   gsf_empresa.usu_inclusao%TYPE
                 , p_cnpj           gsf_empresa.cnpj%TYPE
                 , p_sigla_la       gsf_empresa.sigla_la%TYPE ) is

       v_nome_empresa   gsf_empresa.nome_empresa%TYPE;
       v_id_empresa_pai gsf_empresa.id_empresa_pai%TYPE;
       v_usu_inclusao   gsf_empresa.usu_inclusao%TYPE;
       v_cnpj           gsf_empresa.cnpj%TYPE;
       v_sigla_la       gsf_empresa.sigla_la%TYPE;
       v_id             gsf_empresa.id%TYPE;
       v_data           gsf_empresa.dt_inclusao%TYPE;
       
       begin
         
         v_nome_empresa   := p_nome_empresa;
         v_id_empresa_pai := p_id_empresa_pai;
         v_usu_inclusao   := p_usu_inclusao;
         v_cnpj           := p_cnpj;
         v_sigla_la       := p_sigla_la;
         
         select MAX(ID)+1
              , to_char(sysdate,'dd/mm/yyyy') 
           INTO v_id
              , v_data
           from GSF_EMPRESA;
         
         v_sql_dynamic := 'insert into GSF_EMPRESA ( ID
                                                   , NOME_EMPRESA
                                                   , ID_EMPRESA_PAI
                                                   , DT_INCLUSAO
                                                   , USU_INCLUSAO
                                                   , DT_ULT_ALTERACAO
                                                   , USU_ULT_ALTERACAO
                                                   , CNPJ
                                                   , SIGLA_LA )
                                            values ( :id
                                                   , :nome_empresa
                                                   , :id_empresa_pai
                                                   , :dt_inclusao
                                                   , :usu_inclusao
                                                   , :dt_ult_alteracao
                                                   , :usu_ult_alteracao
                                                   , :cnpj
                                                   , :sigla_la )';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id
             , v_nome_empresa
             , v_id_empresa_pai
             , v_data
             , v_usu_inclusao
             , v_data
             , v_usu_inclusao  
             , v_cnpj
             , v_sigla_la;
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'Gsf_Empresa_Pkg.Inserir');
             dbms_output.put_line(SQLERRM);
                 

             
          
END Inserir;


PROCEDURE Alterar( p_id_empresa        gsf_empresa.id%TYPE
                 , p_nome_empresa      gsf_empresa.nome_empresa%TYPE
                 , p_id_empresa_pai    gsf_empresa.id_empresa_pai%TYPE
                 , p_usu_alt_alteracao gsf_empresa.usu_ult_alteracao%TYPE
                 , p_cnpj              gsf_empresa.cnpj%TYPE
                 , p_sigla_la          gsf_empresa.sigla_la%TYPE ) is
                 
       v_id_empresa        gsf_empresa.id%TYPE;
       v_nome_empresa      gsf_empresa.nome_empresa%TYPE;
       v_id_empresa_pai    gsf_empresa.id_empresa_pai%TYPE;
       v_usu_alt_alteracao gsf_empresa.usu_ult_alteracao%TYPE;
       v_cnpj              gsf_empresa.cnpj%TYPE;
       v_sigla_la          gsf_empresa.sigla_la%TYPE;
       v_dt_alteracao      gsf_empresa.dt_ult_alteracao%TYPE;
       
       begin
         
         v_id_empresa      	 := p_id_empresa;
         v_nome_empresa      := p_nome_empresa;
         v_id_empresa_pai    := p_id_empresa_pai;
         v_usu_alt_alteracao := p_usu_alt_alteracao;                
         v_cnpj              := p_cnpj;
         v_sigla_la          := p_sigla_la;
         
         select to_char(sysdate,'dd/mm/yyyy') 
           INTO v_dt_alteracao
           from dual;
         
         v_sql_dynamic := 'update GSF_EMPRESA 
                              set NOME_EMPRESA      = :nome_empresa
                                , ID_EMPRESA_PAI    = :id_empresa_pai
                                , USU_ULT_ALTERACAO = :usu_ult_alteracao
                                , DT_ULT_ALTERACAO  = :dt_ult_altercao
                                , CNPJ              = :cnpj
                                , SIGLA_LA          = :sigla_la
                           where ID = :id';
                 
	       EXECUTE IMMEDIATE v_sql_dynamic
         USING v_nome_empresa
             , v_id_empresa_pai
             , v_usu_alt_alteracao
             , v_dt_alteracao
             , v_cnpj
             , v_sigla_la
             , v_id_empresa;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'Gsf_Empresa_Pkg.Alterar');
             dbms_output.put_line(SQLERRM);
 

END Alterar;  


PROCEDURE Remover( p_id_empresa gsf_empresa.id%TYPE ) is
  
       v_id_empresa gsf_empresa.id%TYPE;
       
       begin 
         
         v_id_empresa := p_id_empresa;
         
         v_sql_dynamic := 'delete 
                             from GSF_EMPRESA
                            where ID = :id';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_empresa;
         
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'Gsf_Empresa_Pkg.Remover');
             dbms_output.put_line(SQLERRM);
             ROLLBACK; 

END Remover;  

FUNCTION Obtem_Nome_Empresa( p_id_empresa gsf_empresa.id%TYPE ) return varchar2 is
  
       v_id_empresa   gsf_empresa.id%TYPE;
       v_nome_empresa gsf_empresa.nome_empresa%TYPE;
       
       begin
         
         v_id_empresa := p_id_empresa;
         
         select nome_empresa
           INTO v_nome_empresa  
           from gsf_empresa
          where id = v_id_empresa;    
         
         return v_nome_empresa;

END Obtem_Nome_Empresa;

END Gsf_Empresa_Pkg;
/
