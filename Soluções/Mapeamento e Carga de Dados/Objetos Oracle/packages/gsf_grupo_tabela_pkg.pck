create or replace package gsf_grupo_tabela_pkg is

procedure Inserir( p_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE
                 , p_ds_modulo  gsf_grupo_tabela_tab.desc_modulo%TYPE );

procedure Alterar( p_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE
                 , p_ds_modulo  gsf_grupo_tabela_tab.desc_modulo%TYPE );
                                 
                                 
procedure Remover( p_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE );                                 


end gsf_grupo_tabela_pkg;
/
CREATE OR REPLACE PACKAGE BODY gsf_grupo_tabela_pkg is

--variaveis globais
v_sql_dynamic VARCHAR2(2000);

PROCEDURE Inserir( p_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE
                 , p_ds_modulo  gsf_grupo_tabela_tab.desc_modulo%TYPE ) is

       --variaveis locais                        
       v_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE;
       v_ds_modulo  gsf_grupo_tabela_tab.desc_modulo%TYPE;
       v_id_objeto  gsf_grupo_tabela_tab.id_objeto%TYPE;
       
       BEGIN
         
         v_cod_modulo := p_cod_modulo;
         v_ds_modulo  := p_ds_modulo;
         
         
         --insere registro
         v_sql_dynamic := 'insert into gsf_grupo_tabela_tab( COD_MODULO
                                                    , DESC_MODULO
                                                    , VERSAO_LINHA )
                                             values ( :cod_modulo
                                                    , :ds_modulo
                                                    , sysdate )';
                                                         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_modulo
             , v_ds_modulo;
         
         --pega rowid
         SELECT ROWID
           INTO v_id_objeto
           FROM gsf_grupo_tabela_tab 
          WHERE cod_modulo = v_cod_modulo;
         
         --insere rowid
         v_sql_dynamic := 'update gsf_grupo_tabela_tab
                              set ID_OBJETO  = :id_objeto
                            where cod_modulo = :cod_modulo';
                                                          
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto
             , v_cod_modulo;
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_MODULO_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);    
         
         
END Inserir;                               


PROCEDURE Alterar( p_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE
                 , p_ds_modulo  gsf_grupo_tabela_tab.desc_modulo%TYPE ) is
                                 
       --variaveis locais
       v_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE;
       v_ds_modulo  gsf_grupo_tabela_tab.desc_modulo%TYPE;
       
       BEGIN
         
         v_cod_modulo := p_cod_modulo;
         v_ds_modulo  := p_ds_modulo;
         
         v_sql_dynamic := 'update gsf_grupo_tabela_tab 
                              set DESC_MODULO  = :ds_modulo
                                , VERSAO_LINHA = sysdate
                            where COD_MODULO   = :cod_modulo';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_ds_modulo
             , v_cod_modulo;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_MODULO_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);    

END Alterar;

PROCEDURE Remover( p_cod_modulo gsf_grupo_tabela_tab.cod_modulo%TYPE ) is
       
       --variaveis locais
       v_cod_modulo_tab gsf_grupo_tabela_tab.cod_modulo %TYPE;
       
       BEGIN
         
         v_cod_modulo_tab := p_cod_modulo;
         
         v_sql_dynamic := 'delete 
                             from gsf_grupo_tabela_tab
                            where COD_MODULO = :cod_modulo';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_cod_modulo_tab;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_MODULO_PKG.REMOVER');
             dbms_output.put_line(SQLERRM);                    
  
END Remover;

END gsf_grupo_tabela_pkg;
/
