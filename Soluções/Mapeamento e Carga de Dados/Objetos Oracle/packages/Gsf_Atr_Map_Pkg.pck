CREATE OR REPLACE PACKAGE gsf_map_pkg IS

PROCEDURE Inserir( p_id_empresa      gsf_map_tab.id_empresa%TYPE
                 , p_id_db_link      gsf_map_tab.id_db_link%TYPE
                 , p_id_map          OUT gsf_map_tab.id_map%TYPE
                 , p_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE
                 , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                 , p_cod_modulo      gsf_map_tab.cod_modulo%TYPE DEFAULT NULL );
                       
PROCEDURE Alterar( p_id_map          gsf_map_tab.id_map%TYPE 
                 , p_id_empresa      gsf_map_tab.id_empresa%TYPE
                 , p_id_db_link      gsf_map_tab.id_db_link%TYPE
                 , p_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE
                 , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                 , p_cod_modulo      gsf_map_tab.cod_modulo%TYPE DEFAULT NULL );           
                          
PROCEDURE Remover( p_id_map gsf_map_tab.id_map%TYPE ); 

function obtem_id_empresa( p_id_map gsf_map_tab.id_map%TYPE ) return number; 

function obtem_id_db_link( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2;

function obtem_cod_modulo( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2;  

function obtem_cod_tabela_orig( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2;               
                                      
function obtem_cod_tabela_dest( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2;  

END gsf_map_pkg;
/
CREATE OR REPLACE PACKAGE BODY gsf_map_pkg IS

--variaveis globais
v_sql_dynamic varchar2(2000);

PROCEDURE Inserir( p_id_empresa      gsf_map_tab.id_empresa%TYPE
                 , p_id_db_link      gsf_map_tab.id_db_link%TYPE
                 , p_id_map          OUT gsf_map_tab.id_map%TYPE
                 , p_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE
                 , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                 , p_cod_modulo      gsf_map_tab.cod_modulo%TYPE DEFAULT NULL ) is

       --variaveis locais                      
       v_id_empresa      gsf_map_tab.cod_modulo%TYPE;
       v_id_db_link      gsf_map_tab.id_db_link%TYPE;
       v_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE;
       v_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE;
       v_cod_modulo      gsf_map_tab.cod_modulo%TYPE;        
       v_id_objeto       gsf_map_tab.id_objeto%TYPE;
       v_id_map          gsf_map_tab.id_map%TYPE;
       
       BEGIN
         
         v_id_empresa      := p_id_empresa;
         v_id_db_link      := p_id_db_link;
         v_cod_tabela_orig := p_cod_tabela_orig;
         v_cod_tabela_dest := p_cod_tabela_dest;
         v_cod_modulo      := p_cod_modulo;
         
         SELECT gsf_map_seq.nextval 
           INTO v_id_map 
           FROM dual;
           
         p_id_map := v_id_map;
         
         v_sql_dynamic := 'insert into GSF_MAP_TAB( ID_MAP
                                                 , ID_EMPRESA
                                                 , ID_DB_LINK
                                                 , COD_TABELA_ORIG
                                                 , COD_TABELA_DEST
                                                 , COD_MODULO
                                                 , VERSAO_LINHA
                                                 , DATA_MAP )
                                          values ( :id_map
                                                 , :id_empresa
                                                 , :id_db_link
                                                 , :cod_tabela_orig
                                                 , :cod_tabela_dest
                                                 , :cod_modulo
                                                 , sysdate
                                                 , sysdate )';
             
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_map
             , v_id_empresa
             , v_id_db_link
             , v_cod_tabela_orig
             , v_cod_tabela_dest
             , v_cod_modulo;
             
         SELECT ROWID
           INTO v_id_objeto
           FROM GSF_MAP_TAB 
          WHERE ID_MAP = v_id_map;   
          
         v_sql_dynamic := 'update GSF_MAP_TAB
                              set ID_OBJETO = :id_objeto
                            where ID_MAP    = :id_map';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_objeto
             , v_id_map;
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_MAP_PKG.INSERIR');
             dbms_output.put_line(SQLERRM);
             

END Inserir;         

PROCEDURE Alterar( p_id_map          gsf_map_tab.id_map%TYPE 
                 , p_id_empresa      gsf_map_tab.id_empresa%TYPE
                 , p_id_db_link      gsf_map_tab.id_db_link%TYPE
                 , p_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE
                 , p_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE
                 , p_cod_modulo      gsf_map_tab.cod_modulo%TYPE DEFAULT NULL ) is
                         
       --variaveis locais                         
       v_id_empresa      gsf_map_tab.id_empresa%TYPE;
       v_id_db_link      gsf_map_tab.id_db_link%TYPE;
       v_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE;
       v_cod_tabela_dest gsf_map_tab.cod_tabela_dest%TYPE;
       v_cod_modulo      gsf_map_tab.cod_modulo%TYPE;             
       v_id_map          gsf_map_tab.id_map%TYPE;
       
       BEGIN
         
         v_id_empresa      := p_id_empresa;
         v_id_db_link      := p_id_db_link;
         v_cod_tabela_orig := p_cod_tabela_orig;
         v_cod_tabela_dest := p_cod_tabela_dest;
         v_cod_modulo      := p_cod_modulo;
         v_id_map          := p_id_map;
         
         v_sql_dynamic := 'update GSF_MAP_TAB
                              set ID_EMPRESA      = :id_empresa
                                , ID_DB_LINK      = :id_db_link
                                , COD_TABELA_ORIG = :cod_tabela_orig
                                , COD_TABELA_DEST = :cod_tabela_dest
                                , COD_MODULO      = :cod_modulo
                                , VERSAO_LINHA    = sysdate
                            where ID_MAP          = :id_map';
         
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_empresa
             , v_id_db_link
             , v_cod_tabela_orig
             , v_cod_tabela_dest
             , v_cod_modulo
             , v_id_map;
             
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_MAP_PKG.ALTERAR');
             dbms_output.put_line(SQLERRM);           

END Alterar;    

PROCEDURE Remover( p_id_map gsf_map_tab.id_map%TYPE ) is    
   
       --variaveis locais        
       v_id_map gsf_map_tab.id_map%TYPE;
       
       BEGIN
         
         v_id_map := p_id_map;
         
         v_sql_dynamic := 'delete 
                             from GSF_MAP_TAB
                            where ID_MAP = :id_map';
                            
         EXECUTE IMMEDIATE v_sql_dynamic
         USING v_id_map;
         
         COMMIT;
         
         EXCEPTION
           WHEN others THEN
             ROLLBACK; 
             GSF_LOG_PKG.Inserir('EO',SQLERRM,'GSF_MAP_PKG.REMOVER');
             dbms_output.put_line(SQLERRM); 
         
  
END Remover;        

function obtem_id_empresa( p_id_map gsf_map_tab.id_map%TYPE ) return number is
  
       v_id_map     gsf_map_tab.id_map%TYPE;
       v_id_empresa gsf_map_tab.id_empresa%TYPE;
       
       begin
         
         v_id_map := p_id_map;
         
         SELECT ID_EMPRESA
           INTO v_id_empresa
           FROM GSF_MAP_TAB
          WHERE ID_MAP = v_id_map;
          
         return v_id_empresa;  
       
end obtem_id_empresa;

function obtem_id_db_link( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2 is

       v_id_map     gsf_map_tab.id_map%TYPE;
       v_id_db_link gsf_map_tab.id_db_link%TYPE;
       
       begin
         
         v_id_map := p_id_map;
         
         SELECT ID_DB_LINK
           INTO v_id_db_link
           FROM GSF_MAP_TAB
          WHERE ID_MAP = v_id_map;
          
         return v_id_db_link;    

end obtem_id_db_link;  

function obtem_cod_modulo( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2 is

       v_id_map     gsf_map_tab.id_map%TYPE;
       v_cod_modulo gsf_map_tab.cod_modulo%TYPE;
       
       begin
         
         v_id_map := p_id_map;
         
         SELECT COD_MODULO
           INTO v_cod_modulo
           FROM GSF_MAP_TAB
          WHERE ID_MAP = v_id_map;
          
         return v_cod_modulo;   

end obtem_cod_modulo; 
 

function obtem_cod_tabela_orig( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2 is
  
       v_id_map          gsf_map_tab.id_map%TYPE;
       v_cod_tabela_orig gsf_map_tab.cod_tabela_orig%TYPE;
       
       begin
         
         v_id_map := p_id_map;
         
         SELECT COD_TABELA_ORIG
           INTO v_cod_tabela_orig
           FROM GSF_MAP_TAB
          WHERE ID_MAP = v_id_map;
          
         return v_cod_tabela_orig; 
  
end obtem_cod_tabela_orig;

function obtem_cod_tabela_dest( p_id_map gsf_map_tab.id_map%TYPE ) return varchar2 is
  
       v_id_map          gsf_map_tab.id_map%TYPE;
       v_cod_tabela_dest gsf_map_tab.cod_tabela_orig%TYPE;
       
       begin
         
         v_id_map := p_id_map;
         
         SELECT COD_TABELA_DEST
           INTO v_cod_tabela_dest
           FROM GSF_MAP_TAB
          WHERE ID_MAP = v_id_map;
          
         return v_cod_tabela_dest; 
  
end obtem_cod_tabela_dest;                                                                       

END gsf_map_pkg;
/
