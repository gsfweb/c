create or replace package gsf_importacao_pkg is

procedure Carga_Dados_Map ( p_id_map gsf_atr_map_tab.id_map%TYPE );

procedure Remover_Carga_Dados_Map ( p_id_tarefa gsf_atr_map_tab.id_map%TYPE );

end gsf_importacao_pkg;
/
create or replace package body gsf_importacao_pkg is

PROCEDURE Carga_Dados_Map ( p_id_map gsf_atr_map_tab.id_map%TYPE ) IS
  
  v_sql_stmt      CLOB;
  v_db_link       gsf_map_tab.id_db_link%TYPE;
  v_tabela_orig   gsf_tabela_orig_tab.cod_tabela_orig%TYPE;
  v_tabela_dest   gsf_tabela_dest_tab.cod_tabela_dest%TYPE;  
  v_atr_orig      VARCHAR2(32000);
  v_atr_dest      VARCHAR2(32000);
  v_temp          VARCHAR2(1500);  
  v_id_map        gsf_atr_map_tab.id_map%TYPE;
  v_id_tarefa     number;
  v_qt_crit_selec number;
  v_clausula_orig varchar2(2000);
  v_data_type     User_Tab_Columns.DATA_TYPE%TYPE;
  
  
  CURSOR c_clausula IS
  SELECT COD_ATR_ORIG 
       , OPER_RELACIONAL
       , CONDICAO
       , OPER_LOGICO
    FROM GSF_CRIT_SELECAO_MAP_TAB
   WHERE ID_MAP = v_id_map
   ORDER BY ID_ORDEM; 
  
  CURSOR get_map IS
  SELECT id_db_link
       , cod_tabela_orig
       , cod_tabela_dest
    FROM gsf_map map
       , gsf_atr_map amap
   WHERE map.id_map = amap.id_map
     and map.id_map = v_id_map;
     
  CURSOR get_atr IS
     SELECT atr_sql_orig
          , cod_atr_dest 
       FROM  gsf_atr_map
      WHERE  id_map = v_id_map;
      
  TYPE t_clausula IS TABLE OF c_clausula%ROWTYPE 
                  INDEX BY PLS_INTEGER;
            
  l_clausula t_clausula;      
      
BEGIN
  
  v_id_map := p_id_map;

  v_clausula_orig := '';
  
  gsf_tarefa_pkg.inserir(v_id_map
                      , sysdate
                      , 'Carga de Dados'
                      , 'Processando'
                      ,  v_id_tarefa );
                      
 
  
  -- Obt�m dados do mapeamento
  OPEN get_map;
  FETCH get_map INTO v_db_link
                   , v_tabela_orig
                   , v_tabela_dest;
  CLOSE get_map;
  
  -- Obt�m atributos da tabela mapeada de origem e destino
  FOR rec_ IN get_atr LOOP
    v_atr_orig := v_atr_orig || rec_.atr_sql_orig || ',';
    v_atr_dest := v_atr_dest || rec_.cod_atr_dest || ',';
  END LOOP;
  
  v_atr_dest := v_atr_dest || 'id_tarefa, id_map';
  
  DBMS_OUTPUT.PUT_LINE(v_temp);

  
  --monta claudulas dinamicas da tabela gf_cret_selecao_map
  
  SELECT nvl(count(*),0)
    INTO v_qt_crit_selec
    FROM gsf_crit_selecao_map_tab 
   WHERE id_map = v_id_map;
   
  IF v_qt_crit_selec > 0 THEN
    
    OPEN c_clausula;

    FETCH c_clausula

    BULK COLLECT INTO l_clausula;

    CLOSE c_clausula;
    
    v_clausula_orig := 'WHERE ';
         
    FOR indx IN l_clausula.first .. l_clausula.last LOOP
      
      --verifica se campo � do tipo string e trata
      SELECT nvl(CHARACTER_SET_NAME,0)
        INTO v_data_type
        FROM User_Tab_Columns 
       WHERE TABLE_NAME  = v_tabela_orig
         AND COLUMN_NAME = l_clausula(indx).COD_ATR_ORIG;
      
      --� string   
      IF(v_data_type = 'CHAR_CS') THEN
        
        l_clausula(indx).CONDICAO := ''''||l_clausula(indx).CONDICAO||''''; 
        
      END IF;
      
      v_clausula_orig := v_clausula_orig || l_clausula(indx).COD_ATR_ORIG || ' ' ||l_clausula(indx).OPER_RELACIONAL||' '||l_clausula(indx).CONDICAO|| ' '||l_clausula(indx).OPER_LOGICO||' ';

    END LOOP;
    
  END IF; 
  
  
  v_sql_stmt :=  
      'INSERT INTO ' || v_tabela_dest || ' (' || v_atr_dest || ') 
      (SELECT ' || v_atr_orig || v_id_tarefa || ',' ||  v_id_map || ' FROM ' || v_tabela_orig || '@' || v_db_link || ' '||v_clausula_orig||  ' )';

  dbms_output.put_line(v_sql_stmt);
  
  gsf_tarefa_pkg.alterar(v_id_tarefa,sysdate,'Processado');
  
  EXECUTE IMMEDIATE v_sql_stmt;
  
  COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      GSF_LOG_PKG.Inserir('EO', SQLERRM, 'gsf_importacao_pkg.carga_Tabela_Map');
      dbms_output.put_line(SQLERRM);
  
END Carga_Dados_Map;

PROCEDURE Remover_Carga_Dados_Map ( p_id_tarefa gsf_atr_map_tab.id_map%TYPE ) IS
      
       v_id_tarefa      gsf_tarefa_tab.id_tarefa%TYPE;
       v_id_map         gsf_atr_map_tab.id_map%TYPE;
       v_tabela_destino gsf_map_tab.cod_tabela_dest%TYPE;
       v_sql_dynamic    varchar2(2000);
       
       BEGIN
         
         v_id_tarefa := p_id_tarefa;
         
         --pega id_map
         v_id_map := gsf_tarefa_pkg.obtem_id_map(v_id_tarefa);
         
         --pega tabela de destino
         v_tabela_destino := gsf_map_pkg.obtem_cod_tabela_dest(v_id_map);
          
         v_sql_dynamic := 'delete from ' || v_tabela_destino ||' where id_map = '|| v_id_map;
         
         EXECUTE IMMEDIATE v_sql_dynamic;
         
         --atualiza tarefa para exclu�do
         gsf_tarefa_pkg.alterar( v_id_tarefa
                              , sysdate
                              , 'Excluido' );
             
         COMMIT;
         
         EXCEPTION
           WHEN OTHERS THEN
             ROLLBACK;
             GSF_LOG_PKG.Inserir('EO', SQLERRM, 'gsf_importacao_pkg.remover_carga_dados_map');
             dbms_output.put_line(SQLERRM);      
       
  
END Remover_Carga_Dados_Map;

end gsf_importacao_pkg;
/
