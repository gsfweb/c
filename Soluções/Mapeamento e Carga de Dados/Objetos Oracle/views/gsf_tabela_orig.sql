create or replace view gsf_tabela_orig as
select id_empresa
     , Gsf_Empresa_Pkg.Obtem_Nome_Empresa(id_empresa) nome_empresa
     , COD_TABELA_ORIG
     , DESC_TABELA_ORIG
     , VERSAO_LINHA
     , ID_OBJETO
 from GSF_TABELA_ORIG_TAB
WITH READ ONLY;
