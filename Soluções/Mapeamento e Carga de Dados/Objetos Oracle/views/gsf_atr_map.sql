create or replace view gsf_atr_map as
select ID_MAP
     , ID_ORDEM
     , ATR_SQL_ORIG
     , COD_ATR_DEST
     , REGRA_VALIDACAO
     , VERSAO_LINHA
     , ID_OBJETO
 from GSF_ATR_MAP_TAB
WITH READ ONLY;
