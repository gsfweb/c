﻿namespace MapWinApp
{
    partial class FormMapeamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMapeamento));
            this.groupBoxCriteriosSelecaoOrigem = new System.Windows.Forms.GroupBox();
            this.buttonRemoverCriterioSelecaoOrigem = new System.Windows.Forms.Button();
            this.buttonNovoCriterioSelecaoOrigem = new System.Windows.Forms.Button();
            this.listViewCriteriosSelecaoOrigem = new System.Windows.Forms.ListView();
            this.columnHeaderAtributo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderOperadorRelacion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderValorSelecao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderOperadorLógico = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxMapeamentoAtributos = new System.Windows.Forms.GroupBox();
            this.labelAtibutosMapeados = new System.Windows.Forms.Label();
            this.buttonOutraOrigem = new System.Windows.Forms.Button();
            this.buttonRemoverMapeamento = new System.Windows.Forms.Button();
            this.buttonIncluirMapeamento = new System.Windows.Forms.Button();
            this.listBoxAtributosMapeados = new System.Windows.Forms.ListBox();
            this.listBoxAtributosDestino = new System.Windows.Forms.ListBox();
            this.comboBoxTabelasDestino = new System.Windows.Forms.ComboBox();
            this.labelTabelasDestino = new System.Windows.Forms.Label();
            this.listBoxAtributosOrigem = new System.Windows.Forms.ListBox();
            this.comboBoxTabelaOrigem = new System.Windows.Forms.ComboBox();
            this.labelTabelaOrigem = new System.Windows.Forms.Label();
            this.comboBoxEmpresa = new System.Windows.Forms.ComboBox();
            this.labelEmpresa = new System.Windows.Forms.Label();
            this.comboBoxDBLink = new System.Windows.Forms.ComboBox();
            this.labelDBLink = new System.Windows.Forms.Label();
            this.labelIdMap = new System.Windows.Forms.Label();
            this.textBoxIdMapeamento = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBoxCriteriosSelecaoOrigem.SuspendLayout();
            this.groupBoxMapeamentoAtributos.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxCriteriosSelecaoOrigem
            // 
            this.groupBoxCriteriosSelecaoOrigem.Controls.Add(this.buttonRemoverCriterioSelecaoOrigem);
            this.groupBoxCriteriosSelecaoOrigem.Controls.Add(this.buttonNovoCriterioSelecaoOrigem);
            this.groupBoxCriteriosSelecaoOrigem.Controls.Add(this.listViewCriteriosSelecaoOrigem);
            this.groupBoxCriteriosSelecaoOrigem.Location = new System.Drawing.Point(12, 454);
            this.groupBoxCriteriosSelecaoOrigem.Name = "groupBoxCriteriosSelecaoOrigem";
            this.groupBoxCriteriosSelecaoOrigem.Size = new System.Drawing.Size(908, 158);
            this.groupBoxCriteriosSelecaoOrigem.TabIndex = 11;
            this.groupBoxCriteriosSelecaoOrigem.TabStop = false;
            this.groupBoxCriteriosSelecaoOrigem.Text = "Critérios de Seleção da Origem";
            // 
            // buttonRemoverCriterioSelecaoOrigem
            // 
            this.buttonRemoverCriterioSelecaoOrigem.Enabled = false;
            this.buttonRemoverCriterioSelecaoOrigem.Location = new System.Drawing.Point(828, 73);
            this.buttonRemoverCriterioSelecaoOrigem.Name = "buttonRemoverCriterioSelecaoOrigem";
            this.buttonRemoverCriterioSelecaoOrigem.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoverCriterioSelecaoOrigem.TabIndex = 16;
            this.buttonRemoverCriterioSelecaoOrigem.Text = "&Remover";
            this.buttonRemoverCriterioSelecaoOrigem.UseVisualStyleBackColor = true;
            this.buttonRemoverCriterioSelecaoOrigem.Click += new System.EventHandler(this.buttonRemoverCriterioSelecaoOrigem_Click);
            // 
            // buttonNovoCriterioSelecaoOrigem
            // 
            this.buttonNovoCriterioSelecaoOrigem.Enabled = false;
            this.buttonNovoCriterioSelecaoOrigem.Location = new System.Drawing.Point(828, 43);
            this.buttonNovoCriterioSelecaoOrigem.Name = "buttonNovoCriterioSelecaoOrigem";
            this.buttonNovoCriterioSelecaoOrigem.Size = new System.Drawing.Size(75, 23);
            this.buttonNovoCriterioSelecaoOrigem.TabIndex = 14;
            this.buttonNovoCriterioSelecaoOrigem.Text = "&Novo...";
            this.buttonNovoCriterioSelecaoOrigem.UseVisualStyleBackColor = true;
            this.buttonNovoCriterioSelecaoOrigem.Click += new System.EventHandler(this.buttonNovoCriterioSelecaoOrigem_Click);
            // 
            // listViewCriteriosSelecaoOrigem
            // 
            this.listViewCriteriosSelecaoOrigem.AllowColumnReorder = true;
            this.listViewCriteriosSelecaoOrigem.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderAtributo,
            this.columnHeaderOperadorRelacion,
            this.columnHeaderValorSelecao,
            this.columnHeaderOperadorLógico});
            this.listViewCriteriosSelecaoOrigem.FullRowSelect = true;
            this.listViewCriteriosSelecaoOrigem.GridLines = true;
            this.listViewCriteriosSelecaoOrigem.HideSelection = false;
            this.listViewCriteriosSelecaoOrigem.Location = new System.Drawing.Point(13, 19);
            this.listViewCriteriosSelecaoOrigem.Name = "listViewCriteriosSelecaoOrigem";
            this.listViewCriteriosSelecaoOrigem.Size = new System.Drawing.Size(809, 128);
            this.listViewCriteriosSelecaoOrigem.TabIndex = 15;
            this.listViewCriteriosSelecaoOrigem.UseCompatibleStateImageBehavior = false;
            this.listViewCriteriosSelecaoOrigem.View = System.Windows.Forms.View.Details;
            this.listViewCriteriosSelecaoOrigem.SelectedIndexChanged += new System.EventHandler(this.listViewCriteriosSelecao_SelectedIndexChanged);
            // 
            // columnHeaderAtributo
            // 
            this.columnHeaderAtributo.Text = "Atributo";
            this.columnHeaderAtributo.Width = 440;
            // 
            // columnHeaderOperadorRelacion
            // 
            this.columnHeaderOperadorRelacion.Text = "Operador Relacional";
            this.columnHeaderOperadorRelacion.Width = 110;
            // 
            // columnHeaderValorSelecao
            // 
            this.columnHeaderValorSelecao.Text = "Valor de Seleção";
            this.columnHeaderValorSelecao.Width = 120;
            // 
            // columnHeaderOperadorLógico
            // 
            this.columnHeaderOperadorLógico.Text = "Operador Lógico";
            this.columnHeaderOperadorLógico.Width = 110;
            // 
            // groupBoxMapeamentoAtributos
            // 
            this.groupBoxMapeamentoAtributos.Controls.Add(this.labelAtibutosMapeados);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.buttonOutraOrigem);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.buttonRemoverMapeamento);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.buttonIncluirMapeamento);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.listBoxAtributosMapeados);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.listBoxAtributosDestino);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.comboBoxTabelasDestino);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.labelTabelasDestino);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.listBoxAtributosOrigem);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.comboBoxTabelaOrigem);
            this.groupBoxMapeamentoAtributos.Controls.Add(this.labelTabelaOrigem);
            this.groupBoxMapeamentoAtributos.Location = new System.Drawing.Point(11, 150);
            this.groupBoxMapeamentoAtributos.Name = "groupBoxMapeamentoAtributos";
            this.groupBoxMapeamentoAtributos.Size = new System.Drawing.Size(909, 298);
            this.groupBoxMapeamentoAtributos.TabIndex = 10;
            this.groupBoxMapeamentoAtributos.TabStop = false;
            this.groupBoxMapeamentoAtributos.Text = "Mapeamento dos Atributos";
            // 
            // labelAtibutosMapeados
            // 
            this.labelAtibutosMapeados.AutoSize = true;
            this.labelAtibutosMapeados.Location = new System.Drawing.Point(462, 25);
            this.labelAtibutosMapeados.Name = "labelAtibutosMapeados";
            this.labelAtibutosMapeados.Size = new System.Drawing.Size(104, 13);
            this.labelAtibutosMapeados.TabIndex = 14;
            this.labelAtibutosMapeados.Text = "Atributos Mapeados:";
            // 
            // buttonOutraOrigem
            // 
            this.buttonOutraOrigem.Location = new System.Drawing.Point(96, 257);
            this.buttonOutraOrigem.Name = "buttonOutraOrigem";
            this.buttonOutraOrigem.Size = new System.Drawing.Size(90, 23);
            this.buttonOutraOrigem.TabIndex = 13;
            this.buttonOutraOrigem.Text = "Outra origem...";
            this.buttonOutraOrigem.UseVisualStyleBackColor = true;
            this.buttonOutraOrigem.Click += new System.EventHandler(this.buttonOutraOrigem_Click);
            // 
            // buttonRemoverMapeamento
            // 
            this.buttonRemoverMapeamento.Enabled = false;
            this.buttonRemoverMapeamento.Location = new System.Drawing.Point(411, 126);
            this.buttonRemoverMapeamento.Name = "buttonRemoverMapeamento";
            this.buttonRemoverMapeamento.Size = new System.Drawing.Size(27, 23);
            this.buttonRemoverMapeamento.TabIndex = 12;
            this.buttonRemoverMapeamento.Text = "<";
            this.buttonRemoverMapeamento.UseVisualStyleBackColor = true;
            this.buttonRemoverMapeamento.Click += new System.EventHandler(this.buttonRemoverMapeamento_Click);
            // 
            // buttonIncluirMapeamento
            // 
            this.buttonIncluirMapeamento.Enabled = false;
            this.buttonIncluirMapeamento.Location = new System.Drawing.Point(411, 101);
            this.buttonIncluirMapeamento.Name = "buttonIncluirMapeamento";
            this.buttonIncluirMapeamento.Size = new System.Drawing.Size(27, 23);
            this.buttonIncluirMapeamento.TabIndex = 10;
            this.buttonIncluirMapeamento.Text = ">";
            this.buttonIncluirMapeamento.UseVisualStyleBackColor = true;
            this.buttonIncluirMapeamento.Click += new System.EventHandler(this.buttonIncluirMapeamento_Click);
            // 
            // listBoxAtributosMapeados
            // 
            this.listBoxAtributosMapeados.AllowDrop = true;
            this.listBoxAtributosMapeados.FormattingEnabled = true;
            this.listBoxAtributosMapeados.HorizontalScrollbar = true;
            this.listBoxAtributosMapeados.Location = new System.Drawing.Point(465, 42);
            this.listBoxAtributosMapeados.Name = "listBoxAtributosMapeados";
            this.listBoxAtributosMapeados.Size = new System.Drawing.Size(429, 238);
            this.listBoxAtributosMapeados.TabIndex = 11;
            this.listBoxAtributosMapeados.SelectedIndexChanged += new System.EventHandler(this.listBoxAtributosMapeados_SelectedIndexChanged);
            // 
            // listBoxAtributosDestino
            // 
            this.listBoxAtributosDestino.FormattingEnabled = true;
            this.listBoxAtributosDestino.Location = new System.Drawing.Point(215, 68);
            this.listBoxAtributosDestino.Name = "listBoxAtributosDestino";
            this.listBoxAtributosDestino.Size = new System.Drawing.Size(168, 212);
            this.listBoxAtributosDestino.TabIndex = 9;
            this.listBoxAtributosDestino.SelectedValueChanged += new System.EventHandler(this.VerificaItensListBox);
            // 
            // comboBoxTabelasDestino
            // 
            this.comboBoxTabelasDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTabelasDestino.FormattingEnabled = true;
            this.comboBoxTabelasDestino.Location = new System.Drawing.Point(215, 41);
            this.comboBoxTabelasDestino.Name = "comboBoxTabelasDestino";
            this.comboBoxTabelasDestino.Size = new System.Drawing.Size(168, 21);
            this.comboBoxTabelasDestino.TabIndex = 7;
            this.comboBoxTabelasDestino.DropDown += new System.EventHandler(this.comboBoxTabelasDestino_DropDown);
            this.comboBoxTabelasDestino.SelectedIndexChanged += new System.EventHandler(this.comboBoxTabelasDestino_SelectedIndexChanged);
            this.comboBoxTabelasDestino.SelectedValueChanged += new System.EventHandler(this.comboBoxTabelasDestino_SelectedValueChanged);
            this.comboBoxTabelasDestino.TextChanged += new System.EventHandler(this.comboBoxTabelasDestino_TextChanged);
            // 
            // labelTabelasDestino
            // 
            this.labelTabelasDestino.AutoSize = true;
            this.labelTabelasDestino.Location = new System.Drawing.Point(212, 25);
            this.labelTabelasDestino.Name = "labelTabelasDestino";
            this.labelTabelasDestino.Size = new System.Drawing.Size(46, 13);
            this.labelTabelasDestino.TabIndex = 5;
            this.labelTabelasDestino.Text = "Destino:";
            // 
            // listBoxAtributosOrigem
            // 
            this.listBoxAtributosOrigem.FormattingEnabled = true;
            this.listBoxAtributosOrigem.Location = new System.Drawing.Point(18, 68);
            this.listBoxAtributosOrigem.Name = "listBoxAtributosOrigem";
            this.listBoxAtributosOrigem.Size = new System.Drawing.Size(168, 186);
            this.listBoxAtributosOrigem.TabIndex = 8;
            this.listBoxAtributosOrigem.SelectedValueChanged += new System.EventHandler(this.VerificaItensListBox);
            // 
            // comboBoxTabelaOrigem
            // 
            this.comboBoxTabelaOrigem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTabelaOrigem.FormattingEnabled = true;
            this.comboBoxTabelaOrigem.Location = new System.Drawing.Point(18, 41);
            this.comboBoxTabelaOrigem.Name = "comboBoxTabelaOrigem";
            this.comboBoxTabelaOrigem.Size = new System.Drawing.Size(168, 21);
            this.comboBoxTabelaOrigem.TabIndex = 6;
            this.comboBoxTabelaOrigem.DropDown += new System.EventHandler(this.comboBoxTabelaOrigem_DropDown);
            this.comboBoxTabelaOrigem.SelectedValueChanged += new System.EventHandler(this.comboBoxTabelaOrigem_SelectedValueChanged);
            this.comboBoxTabelaOrigem.TextChanged += new System.EventHandler(this.comboBoxTabelaOrigem_TextChanged);
            // 
            // labelTabelaOrigem
            // 
            this.labelTabelaOrigem.AutoSize = true;
            this.labelTabelaOrigem.Location = new System.Drawing.Point(15, 25);
            this.labelTabelaOrigem.Name = "labelTabelaOrigem";
            this.labelTabelaOrigem.Size = new System.Drawing.Size(43, 13);
            this.labelTabelaOrigem.TabIndex = 2;
            this.labelTabelaOrigem.Text = "Origem:";
            // 
            // comboBoxEmpresa
            // 
            this.comboBoxEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxEmpresa.FormattingEnabled = true;
            this.comboBoxEmpresa.Location = new System.Drawing.Point(11, 115);
            this.comboBoxEmpresa.Name = "comboBoxEmpresa";
            this.comboBoxEmpresa.Size = new System.Drawing.Size(251, 21);
            this.comboBoxEmpresa.TabIndex = 5;
            this.comboBoxEmpresa.DropDown += new System.EventHandler(this.comboBoxEmpresa_DropDown);
            this.comboBoxEmpresa.SelectedIndexChanged += new System.EventHandler(this.comboBoxEmpresa_SelectedIndexChanged);
            // 
            // labelEmpresa
            // 
            this.labelEmpresa.AutoSize = true;
            this.labelEmpresa.Location = new System.Drawing.Point(8, 99);
            this.labelEmpresa.Name = "labelEmpresa";
            this.labelEmpresa.Size = new System.Drawing.Size(51, 13);
            this.labelEmpresa.TabIndex = 4;
            this.labelEmpresa.Text = "Empresa:";
            this.labelEmpresa.Click += new System.EventHandler(this.labelEmpresa_Click);
            // 
            // comboBoxDBLink
            // 
            this.comboBoxDBLink.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDBLink.FormattingEnabled = true;
            this.comboBoxDBLink.Location = new System.Drawing.Point(11, 70);
            this.comboBoxDBLink.Name = "comboBoxDBLink";
            this.comboBoxDBLink.Size = new System.Drawing.Size(179, 21);
            this.comboBoxDBLink.TabIndex = 3;
            this.comboBoxDBLink.DropDown += new System.EventHandler(this.comboBoxDBLink_DropDown);
            this.comboBoxDBLink.SelectedIndexChanged += new System.EventHandler(this.comboBoxDBLink_SelectedIndexChanged);
            // 
            // labelDBLink
            // 
            this.labelDBLink.AutoSize = true;
            this.labelDBLink.Location = new System.Drawing.Point(8, 54);
            this.labelDBLink.Name = "labelDBLink";
            this.labelDBLink.Size = new System.Drawing.Size(48, 13);
            this.labelDBLink.TabIndex = 2;
            this.labelDBLink.Text = "DB Link:";
            // 
            // labelIdMap
            // 
            this.labelIdMap.AutoSize = true;
            this.labelIdMap.Location = new System.Drawing.Point(8, 9);
            this.labelIdMap.Name = "labelIdMap";
            this.labelIdMap.Size = new System.Drawing.Size(87, 13);
            this.labelIdMap.TabIndex = 0;
            this.labelIdMap.Text = "Id. Mapeamento:";
            // 
            // textBoxIdMapeamento
            // 
            this.textBoxIdMapeamento.Enabled = false;
            this.textBoxIdMapeamento.Location = new System.Drawing.Point(11, 26);
            this.textBoxIdMapeamento.Name = "textBoxIdMapeamento";
            this.textBoxIdMapeamento.ReadOnly = true;
            this.textBoxIdMapeamento.Size = new System.Drawing.Size(100, 20);
            this.textBoxIdMapeamento.TabIndex = 1;
            this.textBoxIdMapeamento.TabStop = false;
            this.textBoxIdMapeamento.TextChanged += new System.EventHandler(this.textBoxIdMapeamento_TextChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(845, 618);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 17;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // FormMapeamento
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(930, 649);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxIdMapeamento);
            this.Controls.Add(this.labelIdMap);
            this.Controls.Add(this.groupBoxCriteriosSelecaoOrigem);
            this.Controls.Add(this.groupBoxMapeamentoAtributos);
            this.Controls.Add(this.comboBoxEmpresa);
            this.Controls.Add(this.labelEmpresa);
            this.Controls.Add(this.comboBoxDBLink);
            this.Controls.Add(this.labelDBLink);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMapeamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mapeamento";
            this.Activated += new System.EventHandler(this.FormMapeamento_Activated);
            this.Load += new System.EventHandler(this.FormMapeamento_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormMapeamento_KeyDown);
            this.groupBoxCriteriosSelecaoOrigem.ResumeLayout(false);
            this.groupBoxMapeamentoAtributos.ResumeLayout(false);
            this.groupBoxMapeamentoAtributos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBoxCriteriosSelecaoOrigem;
        public System.Windows.Forms.Button buttonRemoverCriterioSelecaoOrigem;
        public System.Windows.Forms.Button buttonNovoCriterioSelecaoOrigem;
        public System.Windows.Forms.ListView listViewCriteriosSelecaoOrigem;
        private System.Windows.Forms.ColumnHeader columnHeaderAtributo;
        private System.Windows.Forms.ColumnHeader columnHeaderOperadorRelacion;
        private System.Windows.Forms.ColumnHeader columnHeaderValorSelecao;
        private System.Windows.Forms.ColumnHeader columnHeaderOperadorLógico;
        private System.Windows.Forms.GroupBox groupBoxMapeamentoAtributos;
        private System.Windows.Forms.Label labelAtibutosMapeados;
        public System.Windows.Forms.Button buttonOutraOrigem;
        public System.Windows.Forms.Button buttonRemoverMapeamento;
        public System.Windows.Forms.Button buttonIncluirMapeamento;
        public System.Windows.Forms.ListBox listBoxAtributosMapeados;
        public System.Windows.Forms.ListBox listBoxAtributosDestino;
        public System.Windows.Forms.ComboBox comboBoxTabelasDestino;
        private System.Windows.Forms.Label labelTabelasDestino;
        public System.Windows.Forms.ListBox listBoxAtributosOrigem;
        public System.Windows.Forms.ComboBox comboBoxTabelaOrigem;
        private System.Windows.Forms.Label labelTabelaOrigem;
        public System.Windows.Forms.ComboBox comboBoxEmpresa;
        private System.Windows.Forms.Label labelEmpresa;
        public System.Windows.Forms.ComboBox comboBoxDBLink;
        private System.Windows.Forms.Label labelDBLink;
        private System.Windows.Forms.Label labelIdMap;
        public System.Windows.Forms.TextBox textBoxIdMapeamento;
        private System.Windows.Forms.Button buttonOK;
    }
}