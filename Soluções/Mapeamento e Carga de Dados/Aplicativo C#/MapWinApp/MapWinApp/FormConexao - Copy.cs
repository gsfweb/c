﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace MapWinApp
{
    public partial class FormConexao : Form
    {
        public FormConexao()
        {
            InitializeComponent();
        }

        private TNSNamesReader tnsNamesReader = new TNSNamesReader();
        private int index;

        private void FormConexao_Load(object sender, EventArgs e)
        {
            this.Tag = string.Empty;

            this.comboBoxOracleHomes.DataSource = tnsNamesReader.GetOracleHomes();

            if (MapWinApp.Properties.Settings.Default["OracleHome"].ToString() != string.Empty)
            {
                for (index = 0;index < this.comboBoxOracleHomes.Items.Count; index++)
                {
                    if (this.comboBoxOracleHomes.Items[index].ToString() == Properties.Settings.Default["OracleHome"].ToString())
                    {
                        this.comboBoxOracleHomes.SelectedIndex = index;
                        break;
                    }
                }
            }

            if (MapWinApp.Properties.Settings.Default["OracleTNSName"].ToString() != string.Empty)
            {
                this.comboBoxTNSNames.Text = MapWinApp.Properties.Settings.Default["OracleTNSName"].ToString();
            }

            if (MapWinApp.Properties.Settings.Default["OracleUsername"].ToString() != string.Empty)
            {
                this.textBoxUserName.Text = MapWinApp.Properties.Settings.Default["OracleUsername"].ToString();
                this.textBoxPassword.Focus();
            }
        }

        private void comboBoxOracleHomes_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> tnsNames = this.tnsNamesReader.LoadTNSNames((string)this.comboBoxOracleHomes.SelectedValue);

            this.comboBoxTNSNames.DataSource = tnsNames;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.comboBoxTNSNames.Text.Trim() != null && this.textBoxUserName.Text.Trim() != null && this.textBoxPassword.Text.Trim() != null)
            {
                this.Cursor = Cursors.WaitCursor;

                Database.connection.ConnectionString = "Data Source=" + this.comboBoxTNSNames.Text.Trim() + ";User Id=" + this.textBoxUserName.Text.Trim() + ";Password=" + this.textBoxPassword.Text.Trim() + ";";

                try
                {
                    Database.connection.Open();

                    if (Database.connection.State == ConnectionState.Open)
                    {
                        this.Tag = "Connected";
                        Console.WriteLine("Connected to Oracle" + Database.connection.ServerVersion);

                        MapWinApp.Properties.Settings.Default.OracleHome = this.comboBoxOracleHomes.Text;
                        MapWinApp.Properties.Settings.Default.OracleTNSName = this.comboBoxTNSNames.Text;
                        MapWinApp.Properties.Settings.Default.OracleUsername = this.textBoxUserName.Text;

                        MapWinApp.Properties.Settings.Default.Save();

                        this.Cursor = Cursors.Default;
                        this.Close();
                    }
                    else
                    {
                        this.Cursor = Cursors.Default;
                    }
                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    MessageBox.Show("Ocorreu erro ao se conectar ao banco de dados (" + Environment.NewLine + ex.Message + ")", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }

        private void FormConexao_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.Tag.ToString() != "Connected")
            {
                Application.Exit();
            }
        }

        private void comboBoxTNSNames_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void labelUserName_Click(object sender, EventArgs e)
        {

        }

        private void labelPassword_Click(object sender, EventArgs e)
        {

        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelOracleHomes_Click(object sender, EventArgs e)
        {

        }

        private void FormConexao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Application.Exit();
            }
        }

    }
}
